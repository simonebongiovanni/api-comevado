
var settings = global.settings;
var codes = {};

codes.invalidParam = 400;
codes.invalidCredentials = 401;
codes.insufficientPermissions = 403;
codes.notFound = 404;
codes.quotaExceeded = 405;
codes.internalError = 500;
codes.externalError = 501;

exports.errorHandler = function(err, req, res, next)
{
    console.error(err.stack);
    
    if(settings.debug)
        exports.sendError(res, codes.internalError, err.stack);
    else
        exports.sendError(res, codes.internalError);
}

exports.notFoundHandler = function(req, res, next)
{
    exports.sendError(res, codes.notFound);
}

exports.sendError = function(res, code, data)
{
    var msg = "";
    var http_error = 500;
    switch(code) {
        case codes.invalidParam:
            msg = "Invalid Parameters";
            http_error = 400;
            break;
        case codes.invalidCredentials:            
            msg = "Invalid Credentials";
            http_error = 401;
            break;
        case codes.insufficientPermissions:            
            msg = "Insufficient Permissions";
            http_error = 401;
            break;
        case codes.notFound:            
            msg = "Not Found";
            http_error = 404;
            break;
        case codes.quotaExceeded:            
            msg = "Quota Exceeded";
            http_error = 500;
            break;
        case codes.internalError:            
            msg = "Internal Server Error";
            http_error = 500;
            break;
        default:
            http_error = 500;
            msg = "Internal Server Error";
    } 
    
    res.status(http_error).send({
        "status" : code,
        "message" : msg,
        "data" : data || []
    });    
}

exports.codes = codes;