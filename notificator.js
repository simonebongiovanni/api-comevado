var db = global.db;
var utilities = global.utilities;
var errors = global.errors;
var settings = global.settings.notification;
var request = require("request");

function dispatchNotification(notification, everybody, callback){
    /*
     * sender: sender user_id 
     * type: notification type       
     * content: content of notification
     * link: url link to show notification details (optional) 
     */
        
    if(!everybody)
    {
        request.post({url: settings.host+"/notify", form: notification}, function(err,httpResponse,body){ 
            if(callback)
            {
                if(err)
                    callback(false, err);
                else
                    callback(true, notification);
            }
        
        })
    }
    else
    {
        request.post({url: settings.host+"/notify/everybody", form: notification}, function(err,httpResponse,body){ 
            if(callback)
            {
                if(err)
                    callback(false, err);
                else
                    callback(true, notification);
            } 
        
        })
    }
    
}


//events
exports.notify = function(sender, content, type, restriction, data, callback){
    
    dispatchNotification({
        sender: sender,
        type: type || 1, // general
        content: content,
        data: data,
        restriction: restriction
    }, false, callback) 
}

exports.notifyAll = function(sender, content, type, data, callback){
    
    dispatchNotification({
        sender: sender,
        type: type || 1, // general
        content: content,
        data: data
    }, true, callback)
    
}


