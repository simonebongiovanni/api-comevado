/**
 * Utilities
**/

var http = require('http');
var https = require('https');
var zlib = require("zlib");
var querystring = require('querystring');
var polyline = require('polyline');
var bcrypt = require('bcrypt');
var moment = require('moment');
var md5 = require('MD5');
var fs = require('fs');

var pdfmaker = require('./pdfmaker');

var request = require('request');
var settings = global.settings;

exports.bcrypt = {};
exports.bcrypt.generate = function(password)
{
    return bcrypt.hashSync(password, 10);
}
exports.bcrypt.check = function(hash, password)
{
    return bcrypt.compareSync(hash, password);
}

exports.md5 = md5;

exports.randomString = function(len, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
    	var randomPoz = Math.floor(Math.random() * charSet.length);
    	randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
}

exports.randomInt = function(max, min){
    return parseInt(Math.random() * (max - min) + min);
}

exports.downloadImage = function(user, url, format, callback){
        
    var name = exports.randomString(5);    
    var filename = "/"+name+format;
    
    var img_host = url.substring(8, url.indexOf("/", 8));
    var img_path = url.substring(url.indexOf("/", 8));
        
    https.get({
      host: img_host,
      path: img_path
    }, function(res) {

        res.setEncoding('binary');
        var imagedata = '';
        
        res.on('data', function(chunk){
            imagedata+= chunk; 
        });
        res.on('end', function(){
            fs.writeFile(settings.upload_dir +filename, imagedata, 'binary', function(){  
                                
                global.aws.s3.upload(settings.upload_dir +filename, "user_profile"+format, user, function(status, data){
                
                    fs.unlink(settings.upload_dir + filename, function(){

                        if(status)
                            callback("user_profile"+format);
                        else
                            console.log("upload error");

                    })
                })
            });
        });
    }).on('error', function(e) {
        console.log("downlaod" + e.message);
        callback(null);
    });
    
}

/**
 * http.get - do a get request
 * @param {string} - host - hostname
 * @param {string} - path - path of the call
 * @param {function} - callback - return the data or the error
 */
exports.http = {};
exports.http.get = function(host, path, callback, header) {
	var response = '';

	var options = {
	    host: host,
	    path: path,
	    headers: header || {}
	};
	http.get(options, function(httpRes) {
	/**
	 * http.Res.on('data') -> Getting the data stream
	 */
	    httpRes.on('data', function (chunk) {
	        response += chunk;
	     
	    });
	/**
	 * http.Res.on('end') -> After the GET, we have the data in response
	 */
	    httpRes.on('end', function() {
	    	callback(true, response);
	    });
	/**
	 * http.Res.on('error') -> return the error
	 */
		httpRes.on('error', function(e) {
		  	callback(false, e);
		});
	});
}

/**
 * http.post - do a post request
 * @param {string} - host - hostname
 * @param {string} - path - path of the call
 * @param {obj} - obj - An object with key -> value
 * @param {function} - callback - return the data or the error
 */
exports.http.post = function(host, path, obj, callback, header) {
	var postData = '';
	var response = '';
	postData = querystring.stringify(obj);
        
    	
	var options = {
		hostname: host,
		path: path,
	    method: 'POST',
	    headers:  header || {'Content-Type': 'application/x-www-form-urlencoded'}
	}
    	
	var req = http.request(options, function(httpRes){
				  	httpRes.on('data', function (chunk) {
				  		response += chunk;
					});
				});		
    
	req.on('error', function(e) {
		callback(false, e);
	});
	
	req.on('end', function(response){
		callback(true, data)
	})
	req.write(postData);
	req.end();
}


/**
 * https.get - do a get secure request
 * @param {string} - host - hostname
 * @param {string} - path - path of the call
 * @param {function} - callback - return the data or the error
 */
exports.https = {};
exports.https.get = function(host, path, callback, header) {
	var response ='';
	var options = {
	    host: host,
	    path: path,
	    headers: header || {}
	};
	https.get(options, function(httpsRes) {
	/**
	 * http.Res.on('data') -> Getting the data stream
	 */
	    httpsRes.on('data', function (chunk) {
	        response += chunk;
	    });
	/**
	 * http.Res.on('end') -> After the GET, we have the data in response
	 */
	    httpsRes.on('end', function() {
	    	callback(true, response);
	    });
	/**
	 * http.Res.on('error') -> return the error
	 */
		httpsRes.on('error', function(e) {
		  	callback(false, e);
		});
	});
}

/**
 * https.post - do a secure post request
 * @param {string} - host - hostname
 * @param {string} - path - path of the call
 * @param {obj} - obj - An object with key -> value
 * @param {function} - callback - return the data or the error
 */
exports.https.post = function(host, path, obj, callback, header) {
	var postData = '';
	var response = '';
	postData = querystring.stringify(obj);
		
	var options = {
	  hostname: host,
	  path: path,
	  method: 'POST',
	  headers: header || {'Content-Type': 'application/x-www-form-urlencoded'}
	};
	
	var req = https.request(options, function(httpsRes){
				  	httpsRes.on('data', function (chunk) {
				  		response += chunk;
				  		
					});
				});			
	
	req.on('error', function(e) {
		console.log(e)
		callback(false, e);
	});
	
	req.on('end', function(response){
		console.log(response)
		callback(true, data)
	})
	
	req.write(postData);
	req.end();
}



/**
 * isJson - Check the given value. Return true if is a JSON, false if not
 * @param - value - content
 */
exports.isJSON = function(value) {
    try {
        JSON.parse(value);
    } catch (e) {
        return false;
    }
    return true;
}

/**
 * getDistanceByHaversine - Return the distance between two points
 * @param {float} - latOne
 * @param {float} - lngOne
 * @param {float} - latTwo
 * @param {float} - lngTwo
 */
exports.getDistanceByHaversine = function(latOne, lngOne, latTwo, lngTwo) {
	var R = 6371; // km  
	var dLat = (latTwo-latOne) * Math.PI/180;  
	var dLon = (lngTwo-lngOne) * Math.PI/180;   
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +  
	        Math.cos(latOne * Math.PI/180) * Math.cos(latTwo * Math.PI/180) *   
	        Math.sin(dLon/2) * Math.sin(dLon/2);   
	var c = 2 * Math.asin(Math.sqrt(a));   
	var d = R * c;
	return d;
}

/**
 * decodePolyline - decrypt a polyline and return an array of coordinates
 * @param {string} - encodedPolyline - The polyline that must be decrypted
 */
exports.decodePolyline = function(encodedPolyline) {
	return polyline.decode(encodedPolyline);
}

exports.moment = {};
exports.moment.datediff = function(data1, data2){
	data1 = moment(data1);
	data2 = moment(data2);
	
	return data1.diff(data2, 'minutes');
};

exports.moment.fromnow = function(data){
	data = moment(data).fromNow();
	
	return data;
}

exports.checkString = {}
exports.checkString.empty = function(words){
	if(words == '') 
		return true;
	else 
		return false;
}
	
exports.checkString.checkInvalidChar = function(words){
	if((/</ig.test(words) == true) || (/\\/ig.test(words) == true) || (/&/ig.test(words) == true) || (/'/g.test(words))) {
		return false;
	}
	
	return true;
};
	
exports.checkString.checkSpace = function(words){
	
	return /\s+/ig.test(words);
};
	
exports.checkString.checkSnail = function(words){
			
	return /@/ig.test(words);
};
	
exports.checkString.checkPoint = function(words) {
	if(words.indexOf(".") > 0)
		return true;
	else
		return false;
};

exports.checkString.checkUpper = function(words){
	return /([A-Z])+/g.test(words);
};
	
exports.checkString.checkNumber = function(words){
	return /([0-9])+/ig.test(words);
};
	
exports.checkString.replaceInvalidChar = function(words){
	return words.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;').replace(/'/g, "/\'\/g");
};
	


exports.elaboratePdf = function(data, callback){
    
    var name = exports.randomString(5);  
    var path = global.settings.upload_dir+name+".pdf";   

    pdfmaker.elaborate(data, path, callback);
}