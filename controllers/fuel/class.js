var db = global.db;
var errors = global.errors;

/**
*
*	Callbacks
*
*/
module.exports.callbackFuel = function(req, res) {
	db.getFuelCost(req.params.type, req, res);
}


/**
*
*	Check parameter input
*
*/
module.exports.checkParam = function(req, res, next) {
	var allowFuel = ['gpl', 'gasoline', 'unleaded'];
	var request = req.params.type;
	
	if(request === undefined || request == '' || allowFuel.indexOf(request) == -1 ) 
		errors.sendError(res, 400);
	else 
		next();
	
}