var controller = require("./class.js");
var updater = require("./updater.js");

module.exports = function(app) {
	
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {post} /fuel/:type Fuel cost 
	*
	*   @apiName GetFuelCost
	*	@apiGroup Fuel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive fuel cost. The fuel type may be gpl/gasoline/unleaded
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/fuel/gpl
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} type The fuel type
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*
	*/
	app.post("/fuel/:type", controller.checkParam, controller.callbackFuel);
}