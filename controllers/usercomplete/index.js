var extend = require('./class.js');
var filters = global.filters;

module.exports = function(app) {
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {post} /usercomplete Find user by name
	*
	*   @apiName FindUser
	*	@apiGroup User Autocomplete
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method it's the user autocomplete
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/usercomplete
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {String} name The name of the user to search
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*
	*		TO DO
	*/	
	app.post('/usercomplete', filters.auth, extend.checkParam, extend.getAll);
}