/**
 * checkParam
 * This method will check the params if they are correct or not
 * @param  {Object} req
 */
exports.checkParam = function(req, res, next) {
	if(req.body.name.length == 0 || req.body.name === undefined) {
		res.status(400).send('Invalid Params');
	}
	else{
		next();
	}		
};

/**
 * Usercomplete Controller.
 * Give the UserComplete Query results
 * @param {string} req.body.name - Name
 */
exports.getAll = function(req, res) {
		global.db_usercomplete.getUsercomplete(req, function(usercomplete){
			
		data = usercomplete;
		ar = [];
		for(i = 0; i < usercomplete.length; i++){
			ar.push(usercomplete[i].id_user);	
		}
		
		res.send({suggest: data, user_index: ar});
	}); 
}