db = global.db;
errors = global.errors;

exports.checkParamPermission = function(req, res, next) {
	db.activityParentType.findOne({where: {id: req.params.idtype}}).then(function(type){
		if(type != null)
			next();
		else{
			errors.sendError(res, errors.codes.invalidParam);
		}
	});
}

/**
 * set receiving permission
 *  @param {Number} idtype - idtype permission
 *  @param {Bool} status - specify if set from_all
 */
exports.setReceivingPermission = function(req, res) {
	if(req.params.idtype == 9) {
		db.UserPermissionReceiving.update({ from_all: req.query.status }, {where:{ id_user: req.user.id }, $and:[{id_parenttype:{$ne: 4}},{id_parenttype:{$ne: 5}},{id_parenttype:{$ne: 6}},{id_parenttype:{$ne: 7}},{id_parenttype:{$ne: 8}}]})
		.then(function(permission){
			db.ActivityStream.emit(42, req.user.id, function(activity){
				permission.from_all = req.query.status;
				permission.id_useractivitystream = activity.id;
			});
			res.send(permission);
		});
	}
	else {
		db.UserPermissionReceiving
			.findOne({where: {id_user: req.user.id, id_parenttype: req.params.idtype}})
			.then(function(permission){
				db.ActivityStream.emit(42, req.user.id, function(activity){
					permission.from_all = req.query.status;
					permission.id_useractivitystream = activity.id;
					
					permission.save(); 
					res.send(permission);
				});
			});			
	}
}

/**
 * set sending permission
 *  @param {Number} idtype - idtype permission
 *  @param {Bool} status - specify if set at all
 */
exports.setSendingPermission = function(req, res) {
	if(req.params.idtype == 9) {
		db.UserPermissionSending.update({
			at_all: req.query.status
		},  {where:{ id_user: req.user.id }, $and:[{id_parenttype:{$ne: 4}},{id_parenttype:{$ne: 5}},{id_parenttype:{$ne: 6}},{id_parenttype:{$ne: 7}},{id_parenttype:{$ne: 8}}]})
		.then(function(permission){
			db.ActivityStream.emit(42, req.user.id, function(activity){
				permission.at_all = req.query.status;
				permission.id_useractivitystream = activity.id;
			});
			res.send(permission);
		});
	}
	else {
		db.UserPermissionSending
			.findOne({where: {id_user: req.user.id, id_parenttype: req.params.idtype}})
			.then(function(permission){
				db.ActivityStream.emit(42, req.user.id, function(activity){
					permission.at_all = req.query.status;
					permission.id_useractivitystream = activity.id;
					
					permission.save(); 
					res.send(permission);
				});
			});		
	}
}


/**
 * getPermission Controller.
 * get all the user permission (sending and receiving)
 * @param {object} - req
 * @param {object} - res
 */
exports.getPermission = function(req, res) {
	db.UserPermissionSending.findAll({where: {id_user: req.user.id, $and:[{id_parenttype:{$ne: 4}},{id_parenttype:{$ne: 5}},{id_parenttype:{$ne: 6}},{id_parenttype:{$ne: 7}},{id_parenttype:{$ne: 8}}]}, include: [db.activityParentType]}).then(function(sending){
		db.UserPermissionReceiving.findAll({where: {id_user: req.user.id, $and:[{id_parenttype:{$ne: 4}},{id_parenttype:{$ne: 5}},{id_parenttype:{$ne: 6}},{id_parenttype:{$ne: 7}},{id_parenttype:{$ne: 8}}]}, include: [db.activityParentType]}).then(function(receiving){
			var permission = {
				sending: sending,
				receiving: receiving
			};
			
			res.send(permission); 
		});
	});
}

/*
 * set all the user permission (sending and receiving)
 * @param {int} - userID - The user ID
 */

module.exports.setDefaultPermission = function(req, res, userID) {
		
	db.ActivityStream.emit(39, userID, function(activity){		
		db.UserPermissionSending.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 9,
			at_all: 1
		});
		
		db.UserPermissionSending.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 8,
			at_all: 1
		});
		
		db.UserPermissionSending.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 7,
			at_all: 1
		});
		
		db.UserPermissionSending.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 6,
			at_all: 1
		});
			
		db.UserPermissionSending.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 5,
			at_all: 1
		});
			
		db.UserPermissionSending.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 4,
			at_all: 1
		});
			
		db.UserPermissionSending.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 3,
			at_all: 1
		});
		
		db.UserPermissionSending.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 2,
			at_all: 1
		});
		
		db.UserPermissionSending.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 1,
			at_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 9,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 8,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 7,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 6,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 5,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 4,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 3,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 2,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: userID,
			id_useractivitystream: activity.id,
			id_parenttype: 1,
			from_all: 1
		});
		
		res.send('OK');
	});
}


/*
 * set all the user permission (sending and receiving)
 * @param {object} - req
 * @param {object} - res 
 * @parama {int} - req.user.id - The id of the user
 */

module.exports.setDefault = function(req, res) {
	db.ActivityStream.emit(39, req.user.id, function(activity){
		db.UserPermissionSending.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 9,
			at_all: 1
		});
		
		db.UserPermissionSending.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 8,
			at_all: 1
		});
		
		db.UserPermissionSending.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 7,
			at_all: 1
		});
		
		db.UserPermissionSending.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 6,
			at_all: 1
		});
		
		db.UserPermissionSending.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 5,
			at_all: 1
		});
		
		db.UserPermissionSending.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 4,
			at_all: 1
		});
		
		db.UserPermissionSending.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 3,
			at_all: 1
		});
		
		db.UserPermissionSending.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 2,
			at_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 9,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 8,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 7,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 6,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 5,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 4,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 3,
			from_all: 1
		});
		
		db.UserPermissionReceiving.create({
			id_user: req.user.id,
			id_useractivitystream: activity.id,
			id_parenttype: 2,
			from_all: 1
		});
		
		res.send('OK');
	});
}