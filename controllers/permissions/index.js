var extend = require('./class.js');
var filters = global.filters;

module.exports = function(app) {
	
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {get} /permission/me/:idtype/setReceiving Set receiving permission
	*
	*   @apiName SetReceivingPermission
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method set receiving permission user. See notification_type table on database for permission type
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/permission/me/1/setReceiving
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idtype The id type permission
	*	@apiParam {number} status  The status of permission
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		TO DO
	*
	*/

	app.get('/permission/me/:idtype/setReceiving', filters.auth, extend.checkParamPermission, extend.setReceivingPermission);
	
	/**
	*
	*   @api {get} /permission/me/:idtype/setSending Set sending permission
	*
	*   @apiName SetSendingPermissionUser
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method set sending permission user. See notification_type table on database for permission type
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/permission/me/1/setSending
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idtype The id type permission
	*	@apiParam {number} status  The status of permission
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		TO DO
	*
	*/
	app.get('/permission/me/:idtype/setSending', filters.auth, extend.checkParamPermission , extend.setSendingPermission);
	
	/**
	*
	*   @api {get} /user/me/permission Get user permission list
	*
	*   @apiName GetUserPermissionList
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive user permission list
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/me/permission
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idtype The id type permission
	*	@apiParam {number} status  The status of permission
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		TO DO
	*
	*/
	app.get('/user/me/permission', filters.auth, extend.getPermission);
	app.get('/permission/default', filters.auth, extend.setDefault); /* This function is used for test */
}

global.controllers.permissions = extend;