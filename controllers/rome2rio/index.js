var extend = require('./class.js');
var filters = global.filters;

module.exports = function(app) {
	
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {post} /rome2rio Multimodal search
	*
	*   @apiName GetMultimodalSearch
	*	@apiGroup Multimodal
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method perform multimodal search
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/rome2rio
	*
	*   @apiParam {float} latF The latitude of start location
	*	@apiParam {float} lngF The longitude of start location
	*   @apiParam {float} latT The latitude of end location
	*	@apiParam {float} lngT The longitude of end location
	*	@apiParam {String} APIType The APItype request (XML/JSON)
	*	@apiParam {String} currency The currency of the search (EUR/USD)
	*	@apiParam {String} departureDate The departure date (yyyy-mm-gg)
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*
	*
	*/
	app.post('/rome2rio', filters.authOrNot, extend.checkParam, extend.getAll);
	
	
	/**
	*
	*   @api {post} /rome2rio/:vehicle/improveprice Improve vehicle price
	*
	*   @apiName ImproveVehiclePrice
	*	@apiGroup Multimodal
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method improve vehicle price segment
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/rome2rio/train/improveprice
	*
	*   @apiParam {Object} route The route to be improved
	*	@apiParam {Data} data The travel data
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*
	*
	*/
    app.post("/rome2rio/:vehicle/improveprice",filters.authOrNot, extend.checkP, extend.improvePriceController);

	
	/**
	*
	*   @api {post} /rome2rio/decodepolyline Decode Rome2rio polyline
	*
	*   @apiName DecodeR2rpolyline
	*	@apiGroup Multimodal
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method decode rome2rio polyline and return an array of coordinates
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/rome2rio/decodepolyline
	*
	*   @apiParam {Object} encodedPolyline The polyline that must be decrypted
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*
	*
	*/
	app.post('/rome2rio/decodepolyline', extend.decodePolyline);
}

