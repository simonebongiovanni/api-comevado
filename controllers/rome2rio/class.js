db = global.db;
db_car = global.db_car;
var settings = global.settings.external_service.rome2rio;
var skyscannerSettings = global.settings.external_service.skyscanner;
var google = global.settings.external_service.google;
var r2r = {};
var async = require('async');
var moment = require('moment');
var polyline = require('polyline');
var stations = require('../autostrade/station.js');
var utilities = global.utilities;
errors = global.errors;


/**
 * checkParam
 * This method will check the params if they are correct or not
 * @param  {Object} req
 * The checked param would be:
 * @param {float} - req.body.latF - Latitude From
 * @param {float} - req.body.lngF - Longitude From   
 * @param {float} - req.body.latT - Latitude To
 * @param {float} - req.body.lngT - Longitude To
 * @param {string} - req.body.APIType - Type of the Rome2Rio API. It must be 'json' or 'xml'
 * @param {string} - req.body.currency - ISO 3 letter for currency. Example 'EUR' or 'USD'
 * @param {string} - req.body.departureDate - yyyy-mm-gg form for the start date
 */
exports.checkParam = function(req, res, next) {
    if(!((typeof req.body.latF === 'undefined' || typeof req.body.lngF === 'undefined' || typeof req.body.latT === 'undefined' || typeof req.body.lngT === 'undefined' || typeof req.body.APIType === 'undefined' || typeof req.body.departureDate === 'undefined'))){
        if((req.body.latF.length == 0) || (req.body.lngF.length == 0) || (req.body.latT.length == 0) || (req.body.lngT.length == 0) || (req.body.APIType.length == 0)) {
            global.errors.sendError(res, global.errors.codes.invalidParam);
        }
        else{
            if((isNaN(req.body.latF))|| (isNaN(req.body.lngF))||(isNaN(req.body.latT))||(isNaN(req.body.lngT))) {
                global.errors.sendError(res, global.errors.codes.invalidParam);
            }
            else {
                if((req.body.APIType.toLowerCase()!="json") && (req.body.APItype.toLowerCase()!="xml")) {
                    global.errors.sendError(res, global.errors.codes.invalidParam);
                }
                else {
                    next();
                }
            }
        }    
    }
    else {
        global.errors.sendError(res, global.errors.codes.invalidParam);
    }        
}

/**
 * Rome2Rio Controller.
 * Give the Rome2Rio results
 * @param {object} - req
 * @param {object} - res
 */
exports.getAll = function(req, res) {
		now = moment().format('YYYY-MM-DD');
		var d = moment(req.body.departureDate,"YYYY-MM-DD").diff(moment(now,"YYYY-MM-DD"), "days");
		if(d >= 0) {
	        var romeToRio = require('../../utilities.js');
	        var returnJSON = {};
	        var apiKey = settings.client_key;
	        var host = 'free.rome2rio.com';
	        var coordFrom = req.body.latF+','+req.body.lngF;
	        var coordTo = req.body.latT+','+req.body.lngT;
	        //var currency = req.body.currency;
	        var currency = "EUR";
	        var queryString = '&currencyCode='+currency+'&oPos='+coordFrom+'&dPos='+coordTo;    
	        var path = '/api/1.2/'+req.body.APIType+'/Search?key='+apiKey+queryString;
	        romeToRio.http.get(host, path, function(flag, data) {
	            if((flag==true)&&(romeToRio.isJSON(data)==true)) {     
	            	var agencies = "";       	
	                r2r = JSON.parse(data);
	                delete r2r.serveTime ;
	                delete r2r.places ; 
	               // delete r2r.airports ;
	                //delete r2r.airlines ; 
	                delete r2r.aircrafts ;
	 
	                var index = 0;             
	                var index1 = 0; 	
	                var co2 = 0;
				    elaborateData(res, req, r2r, req.body.departureDate, function(data){
	                    res.send(data);
	                });
	                
	            }
	            else {
	                global.errors.sendError(res, global.errors.codes.externalError, data);
	            }
	        });
		}
		else {
			var user = {};
			if(req.user != null) {
				user = {
							'logged': true
					   };	
				}
			else {
				user = {
							'logged': false
					   };	
			}
			res.send(user);
		}
}

/**
 * decodePolyline - decrypt a polyline and return an array of coordinates
 * @param {string} - encodedPolyline - The polyline that must be decrypted
 */
exports.decodePolyline = function(req, res) {
	res.send(polyline.decode(req.body.encodedPolyline));
}



/*
 * 
 * elaborateData
 * @param res
 * @param res
 * @param data
 * @param departureDate
 * @param callbackG
 * 
 * This function is used to do the scraping and elaborate Rome2Rio Data
 */
function elaborateData(res, req, data, departureDate, callbackG) {  
  //  delete data.airlines;
//	delete data.agencies;  
    
    var flag = false;
    var i = 0;
    var j = 0;
    var deleteI = -1 ;
    var deleteJ = -1 ;
    
    while(i <data.routes.length && flag == false) {
    	j = 0;
    	while(j < data.routes[i].segments.length && flag == false){
    		if(data.routes[i].segments[j].kind == 'car' && data.routes[i].segments[j].subkind == 'car' && data.routes[i].segments.length == 1) {
				car = data.routes[i].segments[j];       			
    			if(flag == false) {
    				flag = true ;
    				deleteI = i ;
    				deleteJ = j ;
    			}        			
    		}
    		j++;
    	}
    	i++;
    }
    
    if(deleteI != -1)
    	data.routes.splice(deleteI, 1);
    else
    	car = [];
    
    if(car.length == 0){
        carc = [];
    }
    else{
        carc = [car];
    }
    
    for(i = 0; i < data.routes.length; i++){
        data.routes[i].indexRoute = i;
        data.routes[i].duration = 0;

        vehicle = [];
        path = "";
        
        for(j = 0; j < data.routes[i].segments.length; j++){
	        path += data.routes[i].segments[j].path;
	        data.routes[i].segments[j].indexSegment = j;
	        data.routes[i].segments[j].indexRoute = i;
	        data.routes[i].duration +=  data.routes[i].segments[j].duration;
	        
	        if(j > 0){
		        delete data.routes[i].segments[j].sName;
		        
		        data.routes[i].segments[j].sName = data.routes[i].segments[j - 1].tName;
	        }
	        	        
	        if(vehicle.indexOf(data.routes[i].segments[j].vehicle) == -1)
	        	vehicle.push(data.routes[i].segments[j].vehicle)
	        
	        if(data.routes[i].segments[j].kind == 'flight'){
		        for(k = 0; k < data.airports.length; k++){
			        if(data.routes[i].segments[j].sCode == data.airports[k].code){
				        data.routes[i].segments[j].sName = "Aereoporto di " + data.airports[k].name;
			        }
			        
			         if(data.routes[i].segments[j].tCode == data.airports[k].code){
				        data.routes[i].segments[j].tName = "Aereoporto di " + data.airports[k].name;
			        }   
		        }
	        }
        }
        
        data.routes[i].vehicle = vehicle;
        data.routes[i].path = path;
    }
    
    co2 = co2BubbleSort(data);
    duration = durationBubbleSort(data);
    segment = segmentBubbleSort(data);
    price = priceBubbleSort(data);
	
	var vehicle = [];
   	vehicle = splitVehicle(data);
	
	var returnJSON = {
    	car: carc,
    	route: data.routes,
    	co2: co2,
    	duration: duration,
    	segment: segment,
    	price: price,
    	vehicle: vehicle,
    	airlines: data.airlines,
    	agencies: data.agencies,
    };
    
    callbackG(returnJSON);
}

/*
 * priceUpdate
 * Return rome2rio JSON with the total price updated
 * @param - {Object} - data
 */
function priceUpdate(data) {
	var i = 0;
	var j = 0;
	var tot = 0;
	for(i=0; i<data.routes.length; i++) {
		tot = 0;
		for(j=0; j<data.routes[i].segments.length; j++) {
			if(data.routes[i].segments[j].indicativePrice.price >=0)
				tot+= parseFloat(data.routes[i].segments[j].indicativePrice.price);
		}
		if(isNaN(tot)) {
			data.routes.splice(i,1);	
		}
		else {
			data.routes[i].indicativePrice.price = tot;
		}
	}
}

function segmentToSegment(segment, index, index1, res, co2) {
	if(index < segment.routes.length) {
		if(index1 < segment.routes[index].segments.length) {
			db.Vehicle.findAll({
				where:  {
					vehicle: segment.routes[index].segments[index1].kind
				}
			}).then(function(vehicle){
				db.Co2.findAll({
					where: {
						id: vehicle[0].dataValues.id
					}	
				}).then(function(dbRes){
						co2 += parseFloat(segment.routes[index].segments[index1].distance*dbRes[0].dataValues.value) ;
						segment.routes[index].segments[index1].segmentsCo2 = parseFloat(segment.routes[index].segments[index1].distance*dbRes[0].dataValues.value).toFixed(2);
						index1++;
						segmentToSegment(segment, index, index1, res, co2);
					});
				});		
		}
		else {
			segment.routes[index].totCo2 = co2.toFixed(2);
			index++;
			index1 = 0;
			co2 = 0;
			segmentToSegment(segment, index, index1, res, co2);
		}
	}
	else {
		//Creating JSON
		co2JSON = JSON.parse(JSON.stringify(segment));
		durationJSON = JSON.parse(JSON.stringify(segment));
		segmentJSON = JSON.parse(JSON.stringify(segment));
		priceJSON = JSON.parse(JSON.stringify(segment));
		
		//Sorting the JSON
        co2 = co2BubbleSort(co2JSON); //Dovrà diventare Co2. Vedere tabella Co2 Vehicle
        duration = durationBubbleSort(durationJSON);
        segment = segmentBubbleSort(segmentJSON);
        price = priceBubbleSort(priceJSON);
        
		var vehicle = [];
       	vehicle = splitVehicle(segment);
       	
        var returnJSON = {
        	co2: co2,
        	duration: duration,
        	segment: segment,
        	price: price,
        	vehicle: vehicle
        };
		res.send(returnJSON);
	}
}


function splitVehicle(rome2rio) {
	var vehicle = [];
	var sVehicle = [];
	var i = 0;
    rome2rio.routes.forEach(function(rOne) {
    	sVehicle = [];
    	rOne.segments.forEach(function(rTwo) {
    		sVehicle[i] = rTwo.kind;
    		i++;
    		if(vehicle.indexOf(rTwo.kind)== -1) {
    			vehicle.push(rTwo.kind);
    		}
    	});
    	i=0;
    });
	return vehicle;
}

/*
 * co2BubbleSort
 * Return rome2rio JSON sorted by distance
 * @param - {Object} - rome2rio
 */
function co2BubbleSort(rome2rio) {
   var high;
   for (high = rome2rio.routes.length - 1; high > 0; high--)
     { 
         for (i=0; i<high; i++)
         {
           if (rome2rio.routes[i].co2>rome2rio.routes[i+1].co2)
           { 
             aus = rome2rio.routes[i];
             rome2rio.routes[i] = rome2rio.routes[i+1];
             rome2rio.routes[i+1] = aus;
           } 
         }
     }    
     
     var inde = [];
     
     for(i = 0; i < rome2rio.routes.length; i++){
	     inde.push(rome2rio.routes[i].indexRoute)
     }
     
     return inde;
}
 
/*
 * durationBubbleSort
 * Return rome2rio JSON sorted by duration
 * @param - {Object} - rome2rio
 */
function durationBubbleSort(rome2rio) {
   var high;
   for (high = rome2rio.routes.length - 1; high > 0; high--)
     { 
         for (i=0; i<high; i++)
         {
           if (rome2rio.routes[i].duration>rome2rio.routes[i+1].duration)
           { 
             aus = rome2rio.routes[i];
             rome2rio.routes[i] = rome2rio.routes[i+1];
             rome2rio.routes[i+1] = aus;
           } 
         }
     }   
     
      var inde = [];
     
     for(i = 0; i < rome2rio.routes.length; i++){
	     inde.push(rome2rio.routes[i].indexRoute)
     }
     
     return inde;
}

/*
 * segmentBubbleSort
 * Return rome2rio JSON sorted by segment
 * @param - {Object} - rome2rio
 */
function segmentBubbleSort(rome2rio) {
   var high;
   for (high = rome2rio.routes.length - 1; high > 0; high--)
     { 
         for (i=0; i<high; i++)
         {
           if (rome2rio.routes[i].segments.length>rome2rio.routes[i+1].segments.length)
           { 
             aus = rome2rio.routes[i];
             rome2rio.routes[i] = rome2rio.routes[i+1];
             rome2rio.routes[i+1] = aus;
           } 
         }
     }
   
     var inde = [];
     
     for(i = 0; i < rome2rio.routes.length; i++){
	     inde.push(rome2rio.routes[i].indexRoute)
     }
     
     return inde;
}

/*
 * priceBubbleSort
 * Return rome2rio JSON sorted by price
 * @param - {Object} - rome2rio
 */
function priceBubbleSort(rome2rio) {
   var high;
   for (high = rome2rio.routes.length - 1; high > 0; high--)
     { 
         for (i=0; i<high; i++)
         {
           if (rome2rio.routes[i].indicativePrice.price>rome2rio.routes[i+1].indicativePrice.price)
           { 
             aus = rome2rio.routes[i];
             rome2rio.routes[i] = rome2rio.routes[i+1];
             rome2rio.routes[i+1] = aus;
           } 
         }
     }
   
     var inde = [];
     
     for(i = 0; i < rome2rio.routes.length; i++){
	     inde.push(rome2rio.routes[i].indexRoute)
     }
     
     return inde;
}

/*
 * trenitaliaPriceBubbleSort
 * Return trenitalia JSON sorted by price
 * @param - {Object} - trenitalia
 */
function trenitaliaPriceBubbleSort(trenitalia) {
   var high;
   var k=0;
   for(k=0; k<trenitalia.length; k++) {
   		trenitalia[k].price = parseFloat(trenitalia[k].price);
		if(trenitalia[k].price == "")
			trenitalia.splice(k,1);
   }
   for (high = trenitalia.length - 1; high > 0; high--)
     { 
         for (i=0; i<high; i++)
         {
           if (trenitalia[i].price>trenitalia[i+1].price)
           { 
             aus = trenitalia[i];
             trenitalia[i] = trenitalia[i+1];
             trenitalia[i+1] = aus;
           } 
         }
     }
     return trenitalia;
}

/*
 * getCasello
 * Return the coordinates of the nearest point
 * @param - {string} - path
 */
function getCasello(path, callback) {
	var index = 0;
	var i = 0;
	var j = 0;
	var distance = 0.5;
	var coord = "";
	var stz = [];
	
	db.Caselli.findAll().then(function(stations){
		for(index=0; index<path.length; index++) {
			lat = path[index][0];
			lng = path[index][1];
			for(i = 0; i < stations.length; i++) { //Ciclo per tutti i caselli italiani
				if(distance > utilities.getDistanceByHaversine(lat, lng, stations[i].lat, stations[i].lng) && stz.indexOf(stations[i] == -1)) {
					if(stz.indexOf(stations[i]) == -1){
						console.log(stations[i]);
						stz.push(stations[i]);
					}
				}
			}
		}

		callback(stz);
	});
}

/*
 * getGooglePath
 * 
 */
function getGooglePath(res, sLat, sLng, eLat, eLng, callback) {
	var googleUtility = require('../../utilities.js');
	var waypoints = '';
	
//	waypoints += "|" +  eLat + ',' + eLng;
		
	googleKey = "AIzaSyDUKIIniewSKkDo07aglEbzx4axt0MXV4s";
	host = 'maps.googleapis.com';
	path = '/maps/api/directions/json?origin=' + sLat + ',' + sLng + '&destination='+ eLat + ',' + eLng + '&key=' + googleKey;
	
	console.log(googleKey);
	
	var gPath = [];
	var j = 0;
	
	googleUtility.https.get(host, path, function(flag, data){
		if((flag==true)&&(googleUtility.isJSON(data)==true)){
			data = JSON.parse(data);
			if(data.status!="ZERO_RESULTS") {
				n = data.routes[0].legs[0].steps.length;
				for(i=0; i<n; i++) {
					data.routes[0].legs[0].steps[i].polyline.points = googleUtility.decodePolyline(data.routes[0].legs[0].steps[i].polyline.points);
					gPath[j] = data.routes[0].legs[0].steps[i].polyline.points;
					j++;
				}
				callback(gPath);
			}
			else {
				global.errors.sendError(res, global.errors.codes.externalError, data);
			}
		}
		else {
			global.errors.sendError(res, global.errors.codes.externalError, data);
		}
	});
}

module.exports.checkP = function(req, res, next){
	if(req.params.vehicle === undefined || req.params.vehicle == '' || req.body.segment === undefined || req.body.segment == '' || req.body.data === undefined || req.body.data == '')
		global.errors.sendError(res, global.errors.codes.externalError, 'Invalid Params');
	else
		next();
}

module.exports.improvePriceController = function(req, res){
	switch(req.params.vehicle){
		case 'train':
			improveTrainPrice(req, res);
		break;
		case 'flight': 
			improveFligthData(req, res);
		break;
		case 'car':
			improveCarPrice(req, res);
		break;
		case 'bus':
			improveBusPrice(req, res);
		break;
		case 'walk':
			improveWalkPrice(req, res);
		break;
		case 'unknown':
			improveUnknPrice(req, res);
		break;
		case 'ferry':
			improveFerryPrice(req, res);
		break;
		default:
			res.send({segment: req.body.segment});
		break;
	}
}

var improveUnknPrice = function(req, res){
	if(typeof segment.indicativePrice.length == 'undefined') {
		segment.indicativePrice.price = 1;
	}
}

var improveWalkPrice = function(req, res){
	segment.indicativePrice.price = 0;
}	
							
var improveBusPrice = function(req, res){
	var segment = req.body.segment;
	var agencies = req.body.agencies;
	
	db.sequelize.query('SELECT * FROM co2_costant JOIN travel_vehicle ON travel_vehicle.id = co2_costant.id_vehicle WHERE vehicle = :vehicle', { type: db.sequelize.QueryTypes.SELECT, replacements : {vehicle: req.params.vehicle}})
	.then(function(dbRes){

		if(typeof segment.indicativePrice.price == 'undefined') {
			segment.indicativePrice.price = -1;
		}
	
		segment.co2 = parseFloat(segment.distance*dbRes[0].value);
		
		if(typeof segment.itineraries != 'undefined') {
			var name = segment.itineraries[0].legs[0].hops[0].agencies[0].agency; 
			
			for(j = 0; j < agencies.length; j++){
				if(agencies[j].code == name){
					segment.iconPath = "http://www.rome2rio.com" + agencies[j].iconPath;
					segment.iconOffset = agencies[j].iconOffset;
					segment.iconSize = agencies[j].iconSize;
					segment.company = agencies[j].name;
				}
			}
		}

		res.send({segment: segment});	
	});
}

var improveFerryPrice = function(req, res){
	var segment = req.body.segment;
	var agencies = req.body.agencies;
	
	db.sequelize.query('SELECT * FROM co2_costant JOIN travel_vehicle ON travel_vehicle.id = co2_costant.id_vehicle WHERE vehicle = :vehicle', { type: db.sequelize.QueryTypes.SELECT, replacements : {vehicle: req.params.vehicle}})
	.then(function(dbRes){
		
		segment.co2 = parseFloat(segment.distance*dbRes[0].value);
		
		if(typeof segment.itineraries != 'undefined') {
			var name = segment.itineraries[0].legs[0].hops[0].agencies[0].agency; 
			
			for(j = 0; j < agencies.length; j++){
				if(agencies[j].code == name){
					segment.iconPath = "http://www.rome2rio.com" + agencies[j].iconPath;
					segment.iconOffset = agencies[j].iconOffset;
					segment.iconSize = agencies[j].iconSize;
					segment.company = agencies[j].name;
				}
			}
		}
		
		res.send({segment: segment});	
	});
}

function improveFligthData(req, res){
	var segment = req.body.segment;
	var	data = req.body.data; //format YYYY-MM-DD
	var airlines = req.body.agencies;
	
    db.sequelize.query('SELECT * FROM co2_costant JOIN travel_vehicle ON travel_vehicle.id = co2_costant.id_vehicle WHERE vehicle = :vehicle', { type: db.sequelize.QueryTypes.SELECT, replacements : {vehicle: req.params.vehicle}})
	.then(function(dbRes){
		d = data.split("-");
		newday = parseInt(d[2]);
		newday += 1;

		var data1 = data.replace(/-/ig, "");
		
		if((typeof segment.sName == 'undefined'))
			segment.sName = segment.sCode;
		if((typeof segment.tName == 'undefined'))
			segment.tName = segment.tCode;
		if((typeof segment.subkind == 'undefined'))
			segment.subkind = 'flight';
			
		if(typeof segment.itineraries != 'undefined') {
			for(i = 0; i < segment.itineraries.length; i++){
				price = segment.itineraries[i].legs[0].indicativePrice.price;
				
				if(price <= segment.indicativePrice.price ){
					segment.indicativePrice.price = price;
					segment.flightData = segment.itineraries[i].legs[0].hops[0];
					segment.flightCompany = segment.itineraries[i].legs[0].hops[0].airline;
				
				}
			}
			
			for(j = 0; j < airlines.length; j++){
				if(airlines[j].code == segment.flightCompany){
					segment.iconPath = "http://www.rome2rio.com" + airlines[j].iconPath;
					segment.iconOffset = airlines[j].iconOffset;
					segment.iconSize = airlines[j].iconSize;
					segment.company = airlines[j].name;
				}
			}
		}
		
		segment.SACode = segment.sCode;
		segment.STCode = segment.tCode;
		
		sCode = segment.sCode+'-sky' ;
		tCode = segment.tCode+'-sky' ;
			        			
	    var link = "http://www.skyscanner.it/trasporti/voli/" + segment.SACode + "/" + segment.STCode + "/" + data1 + "/" + d[0] + d[1] + newday + "?adults=1&children=0&infants=0&cabinclass=economy&rtn=1&preferdirects=false&outboundaltsenabled=false&inboundaltsenabled=false"; 
	    segment.link = link;
		
		segment.co2 = parseFloat(segment.distance*dbRes[0].value);

		res.send({segment: segment});	
	});	   			
}

var improveTrainPrice = function(req, res){
	var dDate = req.body.data;
	var agencies = req.body.agencies; 
		
	if(req.body.segment.itineraries[0].legs[0].host == 'orario.trenitalia.com') {
		var segment = req.body.segment;
		var sName = segment.sName ;
		var tName = segment.tName ;
		var trenitalia = [];

		db.sequelize.query("SELECT * FROM co2_costant JOIN travel_vehicle ON travel_vehicle.id = co2_costant.id_vehicle WHERE vehicle = 'train'", { type: db.sequelize.QueryTypes.SELECT, replacements : {vehicle: req.params.vehicle}})
			.then(function(dbRes){
				segment.co2 = parseFloat(segment.distance*dbRes[0].value);
				
				global.controllers.train.trenitalia.getTrenitalia(sName, tName, dDate, function(train) {

					trenitalia = trenitaliaPriceBubbleSort(train);
				
					if(!(typeof trenitalia[0]==="undefined")) {
						if(trenitalia[0].price.length != 0 && (!isNaN(trenitalia[0].price))) {
							segment.indicativePrice.price = trenitalia[0].price;
						}
						else {
							var i = 0;
							var flag = false;
							while(i < trenitalia.length && flag == false) {
								if(trenitalia[i].price.length != 0  && (!isNaN(trenitalia[0].price))) {
									segment.indicativePrice.price = trenitalia[i].price;
									flag = true;
								}
								i++;
							}
						}
					}
					
					if(typeof segment.itineraries != 'undefined') {
						var name = segment.itineraries[0].legs[0].hops[0].agencies[0].agency; 

						for(j = 0; j < agencies.length; j++){
							if(agencies[j].code == name){
								segment.iconPath = "http://www.rome2rio.com" + agencies[j].iconPath;
								segment.iconOffset = agencies[j].iconOffset;
								segment.iconSize = agencies[j].iconSize;
								segment.company = agencies[j].name;
							}
						}
					}
						
					res.send({segment: segment});	
				});
		});
	}
	else { //CO2 per Metro + Italo
		var segment = req.body.segment;
		
		db.sequelize.query('SELECT * FROM co2_costant JOIN travel_vehicle ON travel_vehicle.id = co2_costant.id_vehicle WHERE vehicle = :vehicle', { type: db.sequelize.QueryTypes.SELECT, replacements : {vehicle: req.params.vehicle}})
		.then(function(dbRes){
			if(Object.keys(segment.indicativePrice).length==0) {
				segment.indicativePrice.price = 1;
				segment.indicativePrice.currency = 'EUR';
				segment.indicativePrice.isFreeTransfer = 0;
			}
			
			segment.co2 = parseFloat(segment.distance*dbRes[0].value);
			
			if(typeof segment.itineraries != 'undefined') {
				var name = segment.itineraries[0].legs[0].hops[0].agencies[0].agency; 

				for(j = 0; j < agencies.length; j++){
					if(agencies[j].code == name){
						segment.iconPath = "http://www.rome2rio.com" + agencies[j].iconPath;
						segment.iconOffset = agencies[j].iconOffset;
						segment.iconSize = agencies[j].iconSize;
						segment.company = agencies[j].name;
					}
				}
			}

			res.send({segment: segment});	
		});
	}
}

var improveCarPrice = function(req, res){	
	if(req.body.segment.vehicle != 'taxi'){
		if(req.user == null){
			idVehicle = 18596;
			improvePrice(idVehicle, req, res);
		}	
		else{
			db.getVehicle(req.user.id, function(allestiment){
			
				if(allestiment.length > 0)
					idVehicle = allestiment[0];
				else
					idVehicle = 18596;
					
				improvePrice(idVehicle, req, res);
			});
		}	
	}
	else{
		db.sequelize.query('SELECT * FROM co2_costant JOIN travel_vehicle ON travel_vehicle.id = co2_costant.id_vehicle WHERE vehicle = :vehicle', { type: db.sequelize.QueryTypes.SELECT, replacements : {vehicle: 'taxi'}})
			.then(function(dbRes){
				console.log()
				req.body.segment.co2 = parseFloat(req.body.segment.distance*dbRes[0].value) ;
				req.body.segment.indicativePrice.price = 5.5 + req.body.segment.distance * 1.45;
				res.send({segment: req.body.segment});
		});
	}
}

var improvePrice = function(idVehicle, req, res){
	var segment = req.body.segment;
	
	if(segment.subkind == 'unknown') {
		segment.subkind = 'car';
	}
	
	db.sequelize.query('SELECT * FROM co2_costant JOIN travel_vehicle ON travel_vehicle.id = co2_costant.id_vehicle WHERE vehicle = :vehicle', { type: db.sequelize.QueryTypes.SELECT, replacements : {vehicle: req.params.vehicle}})
	.then(function(dbRes){
		db_car.getMixedByAllestiment(idVehicle, function(mixed) {
			db_car.getTypeFuel(idVehicle, function(idfueltype){
				db.sequelize.query('SELECT price FROM fuel_type WHERE id = :fuel',{type: db.sequelize.QueryTypes.SELECT, replacements: {fuel: idfueltype[0].IDCarFuel}}).then(function(fuel){
					fuel = fuel[0].price;
					
					//Consumo Co2
					segment.co2 = parseFloat(segment.distance*dbRes[0].value) ;
					//Costo Vettura
					cost = (segment.distance/(100 / mixed[0].mixed))*fuel;
					segment.indicativePrice.price = cost ;
					if(typeof segment.indicativePrice.fuelCost == 'undefined')
						segment.indicativePrice.fuelCost = cost;
					if(typeof segment.fuelQuantity == 'undefined')
						segment.fuelQuantity = (segment.distance * mixed[0].mixed)/100;     
					//Costo Autostrada
					var from = segment.sPos;
					var to = segment.tPos;
													
					from = from.split(",");
					to = to.split(",");
					
					
					getGooglePath(res, from[0], from[1], to[0], to[1], function(data){
						gPath = [];
			
						for(i = 0; i < data.length; i++){
							for(j=0; j <data[i].length; j++)
								gPath.push(data[i][j]);
						}
						
						req.body.segment.coordiantes = gPath;
					
		
						getCasello(gPath, function(stz){
							if(stz.length > 0) {
								var start = stz[0].id_real_pop;
								var end = stz[stz.length-1].id_real_pop;
								var host = 'www.autostrade.it';
								var path = '/autostrade-gis/ricercaPercorso.do?tipo=P&equivalenzaClassi=A&dtxpDa=' + start + '&dtxpA=' + end + '&soloAuto=on';		
															
								utilities.http.get(host, path, function(flag, dataTwo) {
									global.autostrade.getRouteInfoByBody(dataTwo, function(response) {
										help = response.total_price.toString();
									
											if(help!='tratto non soggetto a pagamentodel pedaggio') {
												if(help.length!=0 && help.length!=1) {
													help = help.split(',');
													help[0] = help[0].replace(/ /g,'');
													help[0] = help[0].replace(/[^\w\s]/gi, '');
													help[1] = help[1].replace(/ /g,'');
													help[1] = help[1].replace(/[^\w\s]/gi, '');
													price = parseFloat(help[0]+"."+help[1]);
													segment.indicativePrice.price += price ;
													if(typeof segment.indicativePrice.tax == 'undefined') {
														segment.indicativePrice.tax = price;
													}
													segment.tax = response;	
													segment.tax.start = stz[0];
													segment.tax.end = stz[stz.length - 1];
												}
	
												res.send({segment: segment});
											}
											else {
												
												 res.send({segment: segment});
											}
									});
								});
							}
							else{		
								 res.send({segment: segment});
							}
						});
					});
				});
			});
		});
	});
}