var extend = require('./class.js')

module.exports = function(app) {
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {get} /skyscanner Skyscanner
	*
	*   @apiName GetUserActivity
	*	@apiGroup Flight
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method get fligth info
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/skyscanner
	*
	*   @apiParam {String} country The standard two letters country identifier (EN, IT, US)
	*   @apiParam {String} currency The standard three letters country identifier (EUR, USD)
	*   @apiParam {String} locale The standard 4 letters locale identifier (it-IT, fr-FR)
	*   @apiParam {String} originplace The IATA CODE of the origin airport (MXP-sky)
	*   @apiParam {String} destinationplace The IATA CODE of the destination airport (BCN-sky)
	*   @apiParam {String} outbounddate The departure date (yyyy-mm-gg)
	*
	*	TO DO
	*/	
	app.post('/skyscanner', extend.checkParam, extend.getAll);
}

global.controllers.skyscanner = extend;