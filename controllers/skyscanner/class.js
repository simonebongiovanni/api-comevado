var settings = global.settings.external_service.skyscanner;
var r2r = {};
var request = require('request');
errors = global.errors;



/**
 * checkParam
 * @param {string} -  req.body.country - The standard two letters country identifier (EN, IT, US)
 * @param {string} -  req.body.currency - The standard three letters country identifier (EUR, USD)
 * @param {string} -  req.body.locale - The standard 4 letters locale identifier (it-IT, fr-FR)
 * @param {string} -  req.body.originplace - The IATA CODE of the origin airport (MXP-sky)
 * @param {string} -  req.body.destinationplace - The IATA CODE of the destination airport (BCN-sky)
 * @param {string} -  req.body.outbounddate - The departure date (yyyy-mm-gg)
 */
exports.checkParam = function(req, res, next) {
    if(!((typeof req.body.country === 'undefined' || typeof req.body.currency === 'undefined' || typeof req.body.locale === 'undefined' || typeof req.body.originplace === 'undefined' || typeof req.body.destinationplace === 'undefined' || typeof req.body.outbounddate === 'undefined'))){
        if((req.body.country.length == 0) || (req.body.currency.length == 0) || (req.body.locale.length == 0) || (req.body.originplace.length == 0) || (req.body.destinationplace.length == 0) || (req.body.outbounddate.length == 0)) {
            global.errors.sendError(res, global.errors.codes.invalidParam);
        }
        else{
        		next();
        }
    }    
    else {
        global.errors.sendError(res, global.errors.codes.invalidParam);
    }        
}


/**
 * Skyscanner Controller.
 * Give Skyscanner results
 * @param {object} - req
 * @param {object} - res
 */

exports.getAll = function(req, res) {
	var skyscanner = require('../../utilities.js');
	apiKey = settings.client_key;
	var host = 'partners.api.skyscanner.net';
	var path = '/apiservices/pricing/v1.0';
	
	formData = exports.getFormData(apiKey, req.body.country, req.body.currency, req.body.locale, req.body.originplace, req.body.destinationplace, req.body.outbounddate);
    request.post({url: 'http://'+host+path, formData: formData }, function(error_precall, response_precall, body) {
        if (!error_precall) {
            request(response_precall.headers.location+"?apiKey="+apiKey, function (error, response, body) {
                if (!error) {
                    res.send(body);
                }
                else
                {
                    global.errors.sendError(res, global.errors.codes.externalError, error);
                }
            })
        }
        else
        {
            global.errors.sendError(res, global.errors.codes.externalError, error_precall);
        }
    });
}

/*
 * getFormData
 * This function is used for set the formData of a request
 */
exports.getFormData = function(key, country, currency, locale, originplace, destinationplace, outbounddate) {
	var result = {
		apiKey: key,
		country: country,
		currency: currency,
		locale: locale,
		originplace: originplace,
		destinationplace: destinationplace,
		outbounddate: outbounddate
	};
	return result;
}

/*
 * Rome2Rio Skyscanner call
 * This function is the rome2Rio call to skyscanner for get correct price
 */
exports.getSkyscanner = function(formData, callback) {
	var error_flag = false;
	var skyscanner = require('../../utilities.js');
	apiKey = settings.client_key;
	var host = 'partners.api.skyscanner.net';
	var path = '/apiservices/pricing/v1.0';
    request.post({url: 'http://'+host+path, formData: formData }, function(error_precall, response_precall, body) {
        if (!error_precall) {
            request(response_precall.headers.location+"?apiKey="+apiKey, function (error, response, body) {
                if (!error) {
                	body = JSON.parse(body);
                
					callback(error_flag, body);
                }
                else
                {
                    error_flag = true;
                }
            })
        }
        else
        {
            error_flag = true;
        }
    });
}
