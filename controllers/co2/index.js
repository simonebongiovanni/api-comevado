var controllers = require('./class.js');

module.exports = function(app) {
	
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {post} /co2/:vehicle Retrive co2 vehicle consuption
	*
	*   @apiName Co2Consuption
	*	@apiGroup CO2 consuption
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive co2 consuption. You can select this type of vehicle: 'car', 'train', 'rideshare', 'flight', 'ferry', 'bus', 'tram', 'taxi','ncc', 'rentcar', 'walk', 'uber', 'metro', 'unknown'
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/co2/car
	*
	*   @apiParam {String} vehicle The type of vehicle for getting co2 consuption
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    {"offset":0,"activity":[{"id":47,"id_typeactivity":4,"id_author":2,"description":"travel_searched","notification_type":"search","data":"2015-12-21T17:51:08.000Z","id_recordref":33},{"id":42,"id_typeactivity":4,"id_author":2,"description":"travel_searched","notification_type":"search","data":"2015-12-21T17:31:28.000Z","id_recordref":32},{"id":12,"id_typeactivity":4,"id_author":2,"description":"travel_searched","notification_type":"search","data":"2015-12-21T10:47:04.000Z","id_recordref":10},{"id":11,"id_typeactivity":4,"id_author":2,"description":"travel_searched","notification_type":"search","data":"2015-12-21T10:45:04.000Z","id_recordref":9},{"id":17,"id_typeactivity":15,"id_author":1,"description":"new_group","notification_type":"group","data":"2015-12-21T10:50:21.000Z","id_recordref":1},{"id":48,"id_typeactivity":5,"id_author":2,"description":"travel_saved","notification_type":"travel","data":"2015-12-21T17:51:23.000Z","id_recordref":4},{"id":43,"id_typeactivity":5,"id_author":2,"description":"travel_saved","notification_type":"travel","data":"2015-12-21T17:31:35.000Z","id_recordref":3},{"id":4,"id_typeactivity":5,"id_author":2,"description":"travel_saved","notification_type":"travel","data":"2015-12-21T10:17:16.000Z","id_recordref":1}]}
	*
	*
	*/
	app.post('/co2/:vehicle', controllers.checkParam, controllers.callbackCo2);
}