var db = global.db;
var errors = global.errors;

/**
*
*	Callback
*
*/
module.exports.callbackCo2 = function(req, res) {
	vehicle = req.params.vehicle;
	db.getCo2(vehicle, req, res);
}

/**
*
*	Check param function
*
**/
module.exports.checkParam = function(req, res, next) {
	var allowVehicle = ['car', 'train', 'rideshare', 'flight', 'ferry', 'bus', 'tram', 'taxi','ncc', 'rentcar', 'walk', 'uber', 'metro', 'unknown'];
	
	if(req.params.vehicle === undefined || req.params.vehicle == '' || allowVehicle.indexOf(req.params.vehicle) == -1)
		errors.sendError(res, 400);
	else
		next();
}