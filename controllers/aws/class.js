var AWS = require('aws-sdk');
var fs = require('fs');
var settings = global.settings.external_service.aws;
var utilities = global.utilities;

var s3 = new AWS.S3({    
    "accessKeyId": settings.accessKeyId,
    "secretAccessKey": settings.secretAccessKey,
    "region": 'eu-west-1'    
});

exports.s3 = {};

exports.s3.upload = function(fileSource, fileDest, user, callback)
{
    fs.readFile(fileSource, function (err, data) {
      if (err) { 
          callback(false, err); 
      }
      else
      {
          var base64data = new Buffer(data, 'binary');

          s3.putObject({
            Bucket: 'demo.comevado.com',
            Key: 'content/user/'+user.id+'/'+fileDest,
            Body: base64data,
            ACL: 'public-read'
          },function(err, data) {
              if (err) callback(false, err.stack);
              else     callback(true, data);
          });

      }
      
    });
    
}
