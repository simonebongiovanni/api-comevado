var station = require('./station.js');
var request = require('./request.js');
var errors = global.errors;

/**
*
*	Callback route info request
*
**/
module.exports.callbackRouteInfo = function(req, res) {
	request.getRouteInfo(req, res);
}

/**
*
*	Checking parameter function of route info request
*
**/
module.exports.checkParamRouteInfo = function(req, res, next) {
	if(req.body.start === undefined || req.body.end === undefined || req.body.end == '' || req.body.start == '' || isNaN(req.body.start) || isNaN(req.body.end)) {
		errors.sendError(res, 400);
	}
	else {
		var startR = checkCode(req.body.start);
		var endR = checkCode(req.body.end);
		
		if(startR == false || endR == false)
			errors.sendError(res, 400);
		else
			next();
	}
}

/**
*
*	Check if code tollbooth exists
*
**/
function checkCode(code) {
	var find = false;
	
	for(i = 0; i < station.station.features.length; i++) {
		if(code == station.station.features[i].properties.stz)
			find = true;
	}
	
	return find;
}

/**
*
*	Callback
*
**/
module.exports.callbackName = function(req, res) {
	codex = getCaselliCode(req.body.name);
	if(codex != '')
		res.status(200).send({code: codex})
	else
		errors.sendError(res, 400);
}

/**
*
*	Retrive cod by name tollboth
*
**/
function getCaselliCode(name) {
	var cod = '';
	
	name = name.toLowerCase();
	name = name.substr(0, 1).toUpperCase() + name.substr(1);
	
	for(i = 0; i < station.station_name.length; i++) {
		if(name == station.station_name[i].name)
			cod = station.station_name[i].cod;
	}
	
	return cod;
}

/**
*
*	Callbacks
*
**/
module.exports.callbackListCaselli = function (req, res) {
	res.status(200).send(station);
}

module.exports.callbackNameListCaselli = function(req, res) {
	res.status(200).send(station.station_name);
}