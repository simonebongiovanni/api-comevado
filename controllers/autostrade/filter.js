var cheerio = require('cheerio');
var Entities = require('html-entities').Html5Entities;
var station = require('./station.js');
entities = new Entities();
global.autostrade = {};

module.exports.getRouteInfo = function(req, res, body) {
	$ = cheerio.load(body, {
	    normalizeWhitespace: true,
	    xmlMode: true,
	    decodeEntities: true
	});
	
	var val = $('.km').text();
	var loc1 = $('.tit_percorso').find('span').first().text();
	var loc2 =  $('.tit_percorso').find('span').last().text();
	var price = entities.decode(val);
	
	var segment = [];
	
	var step = $('#informativaPedaggio h5').each(function(i, elem) {
		var val = entities.decode($(elem).find('strong').html());
		var loc = val.split(':');
		var price1 = entities.decode(entities.decode($(elem).find('span').html()));
		var coord = getCaselliCoord(loc[0].trim())
		
		segment.push({locality: loc[0].trim(), price: price1, coordinates: coord});
	});
	
	var response = {
		start: loc1,
		end: loc2,
		total_price: price,
		step_topay: segment
	};
	
	res.status(200).send(response);
}



global.autostrade.getRouteInfoByBody = function(body, callback) {
	$ = cheerio.load(body, {
	    normalizeWhitespace: true,
	    xmlMode: true,
	    decodeEntities: true
	});
	
	var val = $('.km').text();
	var loc1 = $('.tit_percorso').find('span').first().text();
	var loc2 =  $('.tit_percorso').find('span').last().text();
	var price = entities.decode(val);
	
	var segment = [];
	
	var step = $('#informativaPedaggio h5').each(function(i, elem) {
		var val = entities.decode($(elem).find('strong').html());
		var loc = val.split(':');
		var price1 = entities.decode(entities.decode($(elem).find('span').html()));
		var coord = getCaselliCoord(loc[0].trim())
		
		segment.push({locality: loc[0].trim(), price: price1, coordinates: coord});
	});
	
	var response = {
		start: loc1,
		end: loc2,
		total_price: price,
		step_topay: segment
	};
	
	callback(response);
}



/**
*
*	Retrive tollbooth coord by name
*
**/
function getCaselliCoord(name) {
	var cod = '';
	var coord = '';
	
	name = name.toLowerCase();
	name = name.substr(0, 1).toUpperCase() + name.substr(1);
	
	for(i = 0; i < station.station_name.length; i++) {
		if(name == station.station_name[i].name)
			cod = station.station_name[i].cod;
	}
	
	if(cod != '') {
		for(i = 0; i < station.station.features.length; i++) {
			if(station.station.features[i].properties.stz == cod)
				coord = station.station.features[i].geometry.coordinates;
		}
	}
	
	return coord;
}