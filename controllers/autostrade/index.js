controller = require('./class.js');

module.exports = function(app) {

	app.post('/autostrade/route/info', controller.checkParamRouteInfo, controller.callbackRouteInfo);
	
	
	
	/**
	*
	*	Retrive cod tollbooth by station name
	*	@param {number} name - name of tollbooth
	*
	**/
	app.post('/autostrade/caselli/name', controller.callbackName);
	
	
	
	/**
	*	
	*	Retrive full tollbooth list info
	*
	**/
	app.post('/autostrade/caselli/list', controller.callbackListCaselli);
		
		
			
	/**
	*	
	*	Retrive full tollbooth name list
	*
	**/
	app.post('/autostrade/caselli/namelist', controller.callbackNameListCaselli);
}