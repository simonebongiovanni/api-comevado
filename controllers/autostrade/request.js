var filter = require('./filter.js');
var utilities = require('../../utilities.js')

module.exports.getRouteInfo = function(req, res) {
	var host = 'www.autostrade.it';
	var path = '/autostrade-gis/ricercaPercorso.do?tipo=P&equivalenzaClassi=A&dtxpDa=' + req.body.start + '&dtxpA=' + req.body.end + '&soloAuto=on';

	var callback = function(flag, data) {
		 filter.getRouteInfo(req, res, data);
	}
	
	utilities.http.get(host, path, callback);
}