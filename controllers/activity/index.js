var controllers = require("./class.js");
var filters = global.filters;

module.exports = function(app) {
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {get} /activity/:id_user Retrive user activity
	*
	*   @apiName GetUserActivity
	*	@apiGroup Activity
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive User activity list.
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/activity/me?offset=0
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {String/number} id_user The user id. It may be me/user-id
	*	@apiParam {number} offset  The index of offset number notification
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    {"offset":0,"activity":[{"id":47,"id_typeactivity":4,"id_author":2,"description":"travel_searched","notification_type":"search","data":"2015-12-21T17:51:08.000Z","id_recordref":33},{"id":42,"id_typeactivity":4,"id_author":2,"description":"travel_searched","notification_type":"search","data":"2015-12-21T17:31:28.000Z","id_recordref":32},{"id":12,"id_typeactivity":4,"id_author":2,"description":"travel_searched","notification_type":"search","data":"2015-12-21T10:47:04.000Z","id_recordref":10},{"id":11,"id_typeactivity":4,"id_author":2,"description":"travel_searched","notification_type":"search","data":"2015-12-21T10:45:04.000Z","id_recordref":9},{"id":17,"id_typeactivity":15,"id_author":1,"description":"new_group","notification_type":"group","data":"2015-12-21T10:50:21.000Z","id_recordref":1},{"id":48,"id_typeactivity":5,"id_author":2,"description":"travel_saved","notification_type":"travel","data":"2015-12-21T17:51:23.000Z","id_recordref":4},{"id":43,"id_typeactivity":5,"id_author":2,"description":"travel_saved","notification_type":"travel","data":"2015-12-21T17:31:35.000Z","id_recordref":3},{"id":4,"id_typeactivity":5,"id_author":2,"description":"travel_saved","notification_type":"travel","data":"2015-12-21T10:17:16.000Z","id_recordref":1}]}
	*
	*
	*/
	app.get("/activity/:id_user", filters.auth, controllers.checkParams, controllers.callbackActivity);
	
	
	/**
	*
	*   @api {get} /activity/:idactivity/info Get activity info
	*
	*   @apiName GetActivityInfo
	*	@apiGroup Activity
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive activity info like id_author, data and id_typeactivity
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/activity/47/info
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {Number} idactivity The activity id for getting info
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    {"activity":{"id":47,"id_typeactivity":4,"id_author":2,"data":"2015-12-21T17:51:08.000Z"}}
	*
	*   
	*/
	app.get("/activity/:idactivity/info", filters.auth, controllers.callbackActivityInfo);
	
	
	/**
	*
	*   @api {get} /friendactivity/:id_user Retrive user's friend activity
	*
	*   @apiName GetFriendActivity
	*	@apiGroup Activity
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive user friend activity list
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/friendactivity/:id_user
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {String/number} id_user The activity id for getting user friend info
	*	@apiParam {number} offset  The index of offset number notification
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    {"offset":0,"activity":[]}
	*
	*
	*/
	app.get("/friendactivity/:id_user", filters.auth, controllers.checkParams, controllers.callbackFriendActivity);
	
	
		/**
	*
	*   @api {get} /activity/:idactivity/hide Hide activity user from  wall
	*
	*   @apiName HideUserActivity
	*	@apiGroup Activity
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method hide user activity from his wall
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/activity/47/hide
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {Number} idactivity The activity id to hide
	*
	*
	*/
	app.get("/activity/:idactivity/hide", filters.auth, controllers.checkHideActivity, controllers.callbackHide);
}