var db = global.db;
var utilities = global.utilities;
var errors = global.errors;
var code_erros = global.errors.codes;

module.exports.checkParams = function(req, res, next){
	if(req.params.id_user === "undefined" || req.params.id_user == '' || req.query.offset === undefined || req.query.offset == '' || isNaN(req.query.offset) || req.query.offset < 0)
		errors.sendError(res, code_erros.invalidParam);
	else
		next();  
};

module.exports.callbackActivity = function(req, res){
	var activity = [];
	var offset = parseInt(req.query.offset);
	
	if(req.params.id_user == "me") {
		getActivity(res, activity, offset, req);
	}
	else{
		db.User.find({where: {
			id: req.params.id_user
		}}).then(function(ur) {
			idUser = ur.dataValues.id ;
			if(ur != null){
				getActivity(res, activity, offset, req);
			}
			else {
				errors.sendError(res, code_erros.invalidParam);
			}
		});
	}
};

var getActivity = function(res, activity, off, req) {
	if(activity.length < 5) {
		db.getActivityGroup(req, off, function(data){
			data.forEach(function(item){
				activity.push(item);
			});
			
			if(data.length != 0) {
				if(activity.length >= 5){
					res.send({offset:off, activity: activity});
				}
				else {
					off += 5;
					getActivity(res, activity, off, req);
				}
			}
			else 
				res.send({offset:off, activity: activity}); 
		});
	}
	else {
		res.send({offset:off, activity: activity});
	}
};

var getFriendActivity = function(res, activity, off, req) {
	if(activity.length < 5) {
		db.getFriendActivityGroup(req, off, function(data){
			data.forEach(function(item){
				activity.push(item);
			});
			
			if(data.length != 0) {
				if(activity.length >= 5){
					res.send({offset:off, activity: activity});
				}
				else {
					off += 5;
					getFriendActivity(res, activity, off, req);
				}
			}
			else 
				res.send({offset:off, activity: activity}); 
		});
	}
	else {
		res.send({offset:off, activity: activity});
	}

};

module.exports.callbackFriendActivity = function(req, res){
	var activity = [];
	var offset = parseInt(req.query.offset);
	
	if(req.params.id_user == "me") {
		getFriendActivity(res, activity, offset, req);
	}
	else {
		db.User.find({where: {
			id: req.params.id_user
		}}).then(function(ur) {
			if(ur != null)
				getFriendActivity(res, activity, offset, req);
			else 
				errors.sendError(res, code_erros.invalidParam);
		});
	}
};

module.exports.callbackActivityInfo = function(req, res){
	db.ActivityStream.find({where: {id: req.params.idactivity}}).then(function(activity){
		if(activity != null){
			res.send({activity: activity});
		}
		else{
			res.send({activity: ''});
		}
	})
};

module.exports.checkHideActivity = function(req, res, next){
	if(req.params.idactivity === undefined || req.params.idactivity == '' || req.params.idactivity <= 0 || isNaN(req.params.idactivity))
		errors.sendError(res, code_erros.invalidParam);
	else
		next();
}

module.exports.callbackHide = function(req, res){
	db.ActivityStream.find({where : {
		id: req.params.idactivity
	}}).then(function(activity){
		if(activity != null){
			db.ActivityHide.create({id_user: req.user.id, id_activity: req.params.idactivity})
				.then(function(ac){
					res.send({id: ac});
				});
		}
		else{
			errors.sendError(res, code_erros.invalidParam);
		}
	});
}
