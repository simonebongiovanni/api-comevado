var errors = global.errors;
var db = global.db;
/**
*
*	Check parameter for comment insert
* 	The request need ALL parameter
*	@param {Object} req - request object
*   @param {Object} res - result object
*
**/
module.exports.checkParamInsert = function(req, res, next) {
	var status = 200;
	
	if(req.body.parent === undefined || req.body.parent === '' || isNaN(req.body.parent) || req.body.parent < 0)
		status = 400;
	
	if(typeof req.body.type === 'undefined' || req.body.type == '' || isNaN(req.body.type) || req.body.type < 0)
		status = 400;
		
	if(req.body.comment === 'undefined' || req.body.comment == '')
		status = 400;
		
	if(req.body.starsnumber !== undefined){
		if(req.body.starsnumber == '' || isNaN(req.body.starsnumber) || req.body.starsnumber < 0)
			status = 400;
	}


	if(status == 200)
		next();
	else 
		errors.sendError(res, 400, errors.invalidParam);

}

/**
*
*	Callback for insert comment function
*
**/
module.exports.callBackInsert = function(req, res) {	
	
	db.Comment.newComment(req, function(data){
		db.GroupsActivity.create({
			id_groups: req.params.idgroup,
			id_activity: data.activity.id
		});
		
		db.Comment.find({
				where: { id : data.comment.id},
				include: [ {model: db.ActivityStream, include: [{model : db.GroupsActivity}]}]
		}).then(function(comment){
			
			db.Groups.find({where: {id: req.params.idgroup}}).then(function(group){
				
				db.GroupUser.findAll({where: {id_group: req.params.idgroup, is_confirmed: 1, is_delete: 0, is_refused: 0}})
				.then(function(groupuser){
					if(groupuser.length > 0){
						groupuser.forEach(function(su){
							if(su.id_user != req.user.id){
								global.notificator.notify(req.user.id, "ha appena inserito un commento", 9, su.id_user, {
				                    activity:data.activity.id,
				                    id: req.params.idgroup,
				                    from: group.from_city,
				                    to: group.to_city
				                 });
				             }
						});
					}
					
					if(group.id_usercreated != req.user.id){
						global.notificator.notify(req.user.id, "ha appena inserito un commento", 9, group.id_usercreated, {
		                    activity:data.activity.id,
		                    id: req.params.idgroup,
		                    from: group.from_city,
		                    to: group.to_city
		                 });
					}
				})
			});
				
			res.send({comment: comment});
			
		});
	});
}

exports.checkParamsGroup = function(req, res, next){	
	if(req.params.idgroup === undefined || req.params.idgroup == '' || isNaN(req.params.idgroup) || req.params.idgroup <= 0)
		errors.sendError(res, 400, errors.invalidParam);
	else
		next();
	
}; 	

/**
*
*	Check parameter for comment wizard
*	@param {Object} req - request object
*   @param {Object} res - result object
*
**/
module.exports.checkParamWizard =  function(req, res, next) {
	var paramAllow = ['comfort', 'speed', 'price', 'punctuality'];
	var addon = 0;
	
	if(typeof req.body.comment === 'undefined' || req.body.comment == '' || isNaN(req.body.comment))
		errors.sendError(res, 400);
	
	for(var key in req.body) {
		if(key != 'comment') {
			addon++;
			
			if(req.body[key] == '' || isNaN(req.body[key]))
				errors.sendError(res, 400);
		}
	}
	
	if(addon == 0)
		errors.sendError(res, 400);
	
	next();
}

/**
*
* 	Callback for wizard insert function
*
**/
module.exports.callBackWizard =  function(req, res) {
	db.wizard(req, res);
}


/**
*
*	Check parameter for comment preferences
*	@param {Object} req - request object
*   @param {Object} res - result object
*
**/
module.exports.checkParamPreference = function(req, res, next) {
	if(req.params.idcomment === undefined || req.params.idcomment == '' || isNaN(req.params.idcomment)){
		errors.sendError(res, 400);
	}
	else {
		db.Comment.find({where: {id: req.params.idcomment}})
			.then(function(comment){
				if(comment != null)
					next();
				else
					errors.sendError(res, 400);
			});
	}
		
};

module.exports.callBackLike = function(req, res){
	db.ActivityStream.emit(12, req.user.id, function(activity){
		db.Comment.findOne({
				where: { id: req.params.idcomment},
				include: [ {model: db.ActivityStream, include: [{model : db.GroupsActivity}]}]
		}).then(function(comment){

			id_groups = comment.activityStream.groupsActivity.id_groups;
			db.GroupsActivity.create({id_groups: id_groups, id_activity: activity.id});
			
			var queryString = "CALL insert_preferences(:author,:parent, 'like'," + activity.id + ")";
		
			db.sequelize.query(queryString, {replacements: {author: req.user.id, parent: req.params.idcomment}}).then(function(data) {
				db.Comment.find({where: {is_preferences: 1, id_author: req.user.id, id_parent: req.params.idcomment}}).then(function(pref){
					if(pref.is_like == 1){
						db.Comment.find({where: {id: pref.id_parent}}).then(function(sc){
							
							console.log(sc.id_author + " - " + req.user.id)
							
							if(sc.id_author != req.user.id){
								global.notificator.notify(req.user.id, " piace il tuo commento", 11, sc.id_author, {
									activity: activity.id,
									id: id_groups,
					                body: sc.body
					            });
							}
						});
					}
				});

				
				exports.callbackListPreferences(req, res);
			});
		});
	});
};

module.exports.callbackDislike = function(req, res){
	db.ActivityStream.emit(13, req.user.id, function(activity){
		db.Comment.findOne({
				where: { id: req.params.idcomment},
				include: [{model: db.ActivityStream, include: [{model : db.GroupsActivity}]}]
		}).then(function(comment){
			id_groups = comment.activityStream.groupsActivity.id_groups;
			
			db.GroupsActivity.create({id_groups: id_groups, id_activity: activity.id});
			
			var queryString = "CALL insert_preferences(:author,:parent, 'dislike'," + activity.id + ")";
		
			db.sequelize.query(queryString, {replacements: {author: req.user.id, parent: req.params.idcomment}}).then(function(data) {
				db.Comment.find({where: {is_preferences: 1, id_author: req.user.id, id_parent: req.params.idcomment}}).then(function(pref){
					if(pref.is_dislike == 1){
						db.Comment.find({where: {id: pref.id_parent}}).then(function(sc){
							if(sc.id_author != req.user.id){
								global.notificator.notify(req.user.id, " non piace il tuo commento", 10, sc.id_author, {
					                  body: sc.body,
					                  id: id_groups,
					                  activity: activity.id
					            });
				            }
						});
					}
				});
				
				exports.callbackListPreferences(req, res);	
			});
		});
	});
};

module.exports.callbackCommentList = function(req, res){
	var list = [];
		
	var query = "SELECT comment.*, user_permission_receiving.from_all, user_permission_sending.at_all  FROM comment LEFT OUTER JOIN activity_stream ON comment.id_activity = activity_stream.id INNER JOIN groups_activity ON activity_stream.id = groups_activity.id_activity JOIN activity_stream_type ON activity_stream_type.id = activity_stream.id_typeactivity JOIN activity_parent_type ON activity_parent_type.id = activity_stream_type.id_parenttype JOIN user_permission_sending ON user_permission_sending.id_parenttype = activity_parent_type.id AND user_permission_sending.id_user = comment.id_author JOIN user_permission_receiving ON user_permission_receiving.id_parenttype = activity_parent_type.id AND user_permission_receiving.id_user = :recuser WHERE comment.is_preferences = 0 AND groups_activity.id_groups = :group ORDER BY datainsert ASC;";
	
	db.sequelize.query(query , { type: db.sequelize.QueryTypes.SELECT, replacements: {group: req.params.idgroup, recuser: req.user.id}})
	  .then(function(comment){
		for(i = 0; i < comment.length; i++){
		
			if(comment[i].id_author == req.user.id || ((comment[i].id_author != req.user.id && comment[i].at_all == 1) && (comment[i].id_author != req.user.id && comment[i].from_all == 1)))
				list.push(comment[i]);
			
		}
			
		res.send({comment: list});
		
	});
};

module.exports.callbackListPreferences = function(req, res){
	db.Comment.findAll({where: {id_parent: req.params.idcomment, is_preferences: 1}})
	.then(function(prefer){
		like = [];
		dislike = [];
		
		for(i = 0; i < prefer.length; i++){
			if(prefer[i].is_like)
				like.push(prefer[i]);
			if(prefer[i].is_dislike)
				dislike.push(prefer[i]);
		}
		
		res.send({like: like, dislike: dislike});
	});
};
