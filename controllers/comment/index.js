var controller = require('./class.js');
var filters = global.filters;

module.exports = function(app) {
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {post} /group/:idgroup/comment/new Insert comment in group post
	*
	*   @apiName InsertCommentGroup
	*	@apiGroup Group Comment
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method insert new comment in travel group
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/group/1/comment/new'
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} parent The id parent comment. If is main comment the value is 0
	*	@apiParam {number} type  The type comment 
	*	@apiParam {String} comment  The body of real comment
	*
	*	
	*	TO DO INSERIRE IL RESPONSO
	*
	*/
	app.post('/group/:idgroup/comment/new', filters.auth, controller.checkParamInsert, controller.checkParamsGroup, controller.callBackInsert);
	
	/**
	*
	*   @api {post} /group/:idgroup/comment/list Group comment list
	*
	*   @apiName GetCommentList
	*	@apiGroup Group Comment
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive group comment list
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/group/1/comment/list
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idgroup The group id
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    {"comment":[{"id":1,"id_parent":0,"id_typecomment":1,"body":"resd","id_author":7,"is_approved":1,"datainsert":"2016-01-14T16:02:46.000Z","starsnumber":0,"is_preferences":0,"is_like":0,"is_dislike":0,"id_activity":113,"from_all":1,"at_all":1},{"id":3,"id_parent":0,"id_typecomment":1,"body":"re","id_author":4,"is_approved":1,"datainsert":"2016-01-14T16:05:38.000Z","starsnumber":0,"is_preferences":0,"is_like":0,"is_dislike":0,"id_activity":125,"from_all":1,"at_all":1},{"id":4,"id_parent":0,"id_typecomment":1,"body":"re","id_author":4,"is_approved":1,"datainsert":"2016-01-14T16:05:43.000Z","starsnumber":0,"is_preferences":0,"is_like":0,"is_dislike":0,"id_activity":126,"from_all":1,"at_all":1}]}	*
	*
	*/
	app.get('/group/:idgroup/comment/list', filters.auth, controller.checkParamsGroup, controller.callbackCommentList);
	


	
	/**
	*
	*	Catch request for insert like to comment
	*	@param {Object} req - request object
	*   @param {Object} res - result object
	*
	*   POST PARAMETER(require at least 1 optional parameter)
	*   @param {number} comment - id of commente
	*   @param {number} [speed] - value of speed feedback
	*   @param {number} [comfort] - value of comfort feedback
	*   @param {number} [price] - value of price feedback
	*   @param {number} [punctuality] - value of punctuality feedback
	*
	**/
	//app.post('/comment/wizard/', controller.checkParamWizard, controller.callBackWizard);
	
	
	/**
	*
	*   @api {get} /comment/:idcomment/like Comment like
	*
	*   @apiName CommentLikeInsert
	*	@apiGroup Group Comment
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method asign like to a comment. It also return comment preference list
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/comment/1/like
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idcomment The id of the comment to asign the preference
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    {"like":[{"id":2,"id_parent":1,"id_typecomment":1,"body":"","id_author":7,"is_approved":true,"datainsert":"2016-01-14T16:02:51.000Z","starsnumber":0,"is_preferences":true,"is_like":true,"is_dislike":false,"id_activity":116},{"id":5,"id_parent":1,"id_typecomment":1,"body":"","id_author":2,"is_approved":true,"datainsert":"2016-01-19T15:46:52.000Z","starsnumber":0,"is_preferences":true,"is_like":true,"is_dislike":false,"id_activity":301}],"dislike":[]}	*
	*
	*/
	app.get('/comment/:idcomment/like', filters.auth, controller.checkParamPreference, controller.callBackLike);
	
	/**
	*
	*   @api {get} /comment/:idcomment/dislike Comment dislike
	*
	*   @apiName CommentDislikeInsert
	*	@apiGroup Group Comment
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method asign dislike to a comment. It also return comment preference list
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/comment/1/dislike
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idcomment The id of the comment to asign the preference
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	  {"like":[{"id":2,"id_parent":1,"id_typecomment":1,"body":"","id_author":7,"is_approved":true,"datainsert":"2016-01-14T16:02:51.000Z","starsnumber":0,"is_preferences":true,"is_like":true,"is_dislike":false,"id_activity":116}],"dislike":[{"id":5,"id_parent":1,"id_typecomment":1,"body":"","id_author":2,"is_approved":true,"datainsert":"2016-01-19T15:49:06.000Z","starsnumber":0,"is_preferences":true,"is_like":false,"is_dislike":true,"id_activity":302}]}
	*
	*/
	app.get('/comment/:idcomment/dislike', filters.auth, controller.checkParamPreference, controller.callbackDislike);
	
	/**
	*
	*   @api {get} /comment/:idcomment/preferences/list Comment preference list
	*
	*   @apiName CommentPreferenceList
	*	@apiGroup Group Comment
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method asign retrive comment preference list
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/comment/1/preferences/list
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idcomment The id of the comment to retrive preference list
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	 {"like":[{"id":2,"id_parent":1,"id_typecomment":1,"body":"","id_author":7,"is_approved":true,"datainsert":"2016-01-14T16:02:51.000Z","starsnumber":0,"is_preferences":true,"is_like":true,"is_dislike":false,"id_activity":116}],"dislike":[{"id":5,"id_parent":1,"id_typecomment":1,"body":"","id_author":2,"is_approved":true,"datainsert":"2016-01-19T15:49:06.000Z","starsnumber":0,"is_preferences":true,"is_like":false,"is_dislike":true,"id_activity":302}]}
	*
	*/
	app.get('/comment/:idcomment/preferences/list', filters.auth,controller.checkParamPreference, controller.callbackListPreferences)
	
}