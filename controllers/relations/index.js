
var relationController = require("./class");
var filters = global.filters;

module.exports = function(app)
{	
		/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {get} /relateto/:userid/type/:relationtype Send friendship request
	*
	*   @apiName SendFriendshipRequest
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method send friendship request. In this method you can specify the relation type, but in this version(1.0.0) the unique relation type supported is 1.
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/relateto/6677/type/1
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} userid The user id tho send the request
	*	@apiParam {number} relationtype  The id of relation type
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*
	*     TO DO
	*
	*
	*/
    app.get('/relateto/:userid/type/:relationtype', filters.auth, relationController.requestReleation);
    
   	/**
	*
	*   @api {get} /relation/:idrelation/accept Accept friendship request
	*
	*   @apiName AcceptFriendshipRequest
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method accept friendship request
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/relation/1/accept
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idrelation The user relation to accept
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*
	*     TO DO
	*
	*
	*/
    app.get('/relation/:idrelation/accept', filters.auth, relationController.acceptRelation);
    
    /**
	*
	*   @api {get} /relation/:idrelation/refuse Refuse friendship request
	*
	*   @apiName RefuseFriendshipRequest
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method accept friendship request
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/relation/1/remove
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idrelation The user relation to accept
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*
	*     TO DO
	*
	*/
    app.get('/relation/:idrelation/remove', filters.auth, relationController.removeRelation);
    
    /**
	*
	*   @api {post} /relation/:idrelation/edit Edit friendship relation
	*
	*   @apiName EditFriendshipRelation
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method edit friendship relation
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/relation/1/edit
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idrelation The user relation to accept
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*
	*     TO DO
	*
	*
	*/
    app.post('/relation/:idrelation/edit', filters.auth, relationController.editRelation);
    
    /**
	*
	*   @api {get} /user/:userid/relation/list User relation list
	*
	*   @apiName GetUserRelation
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method get user relation list
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/1/relation/list
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} userid The user relation to get relation list
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*
	*     TO DO
	*
	*/
    app.get('/user/:userid/relation/list', filters.auth, relationController.getAllRelation);
    
    /**
	*
	*   @api {get} /user/relation/loaddetail Relation detail from activity id
	*
	*   @apiName GetDetailRelationfromActivitt
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive relation detail from activity id
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/relation/loaddetail?id_activity=1
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*
	*     TO DO
	*
	*
	*/
    app.get('/user/relation/loaddetail', filters.auth, relationController.getRelationdetail);
    
      /**
	*
	*   @api {get} /user/relation/loaddetail Get common friend list
	*
	*   @apiName GetCommonFriendList
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive common friend list between two user
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/66/count/commonfriend
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} userid The user relation to get relation list
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*
	*     TO DO
	*
	*
	*/
    app.get('/user/:iduser/count/commonfriend',  filters.auth, relationController.checKP, relationController.getFriendCount)
}

global.controllers.relation = relationController;


