
var db = global.db;
var errors = global.errors;
var code_erros = global.errors.codes;
 
exports.requestReleation = function(req, res){
    
    if(typeof req.params.userid != "undefined")
    {
        db.User.find({where:{id: req.params.userid}}).then(function(user){
            
            if(user != null)
            {
	            db.UserRelation.find({where: {
		            $or:[
		            	{
			            id_user1: req.user.id,
			            id_user2: req.params.userid
			          },{
				        id_user1: req.params.userid ,
			            id_user2: req.user.id 
			          }],
			        	  is_confirmed: 0,
			        	  is_deleted: 0
			          }
			      }).then(function(friendship){
				        	if(friendship == null){
					        	db.ActivityStream.emit(3, req.user.id, function(activity){
							          db.UserRelation.create({
					                    id_user1: req.user.id,
					                    id_user2: req.params.userid,
					                    id_type_relation: req.params.relationtype || 0
					                }).then(function(user_relation){
						                  db.UserRelationStatus.create({
							               	id_relation: user_relation.id,
							               	id_activity: activity.id
						                }).then();
						                
						                global.notificator.notify(user_relation.id_user1, "ti ha inviato una richiesta di amicizia", 7, user_relation.id_user2.toString(), {
						                    id_user1: user_relation.id_user1,
						                    id_user2: user_relation.id_user2
						                })
						                
					                    res.send({id_relation: user_relation.id});
					                });
					            });
				        	}else {
					        	errors.sendError(res, code_erros.invalidParam, 'Frienship already exists'); 
				        	}
			          })
            }
            else
            {
               errors.sendError(res, code_erros.invalidParam); 
            }
        })
    }
    else
    {
        errors.sendError(res, code_erros.invalidParam);  
    }
}


exports.acceptRelation = function(req, res){
    
    if(typeof req.params.idrelation != "undefined")
    {
        db.UserRelation.find({where:{id: req.params.idrelation, id_user2: req.user.id}}).then(function(user_relation){
            if(user_relation != null)
            {
               global.notificator.notify(user_relation.id_user2, "ha stretto amicizia", 4, user_relation.id_user1.toString(), {
                    id_user1: user_relation.id_user1,
                    id_user2: user_relation.id_user2
               })
                
	           db.ActivityStream.emit(27, req.user.id, function(activity){
	               user_relation.is_confirmed = true;
	               
	               user_relation.save().then(function(user_relation){
	                    db.UserRelationStatus.create({
			              	id_relation: user_relation.id,
			              	id_activity: activity.id
		               }).then(function(accept){
				                           
			               res.send(accept);
		               });
	               });
	           });
            }
            else
            {
               errors.sendError(res, code_erros.invalidParam); 
            }
        })
    }
    else
    {
        errors.sendError(res, code_erros.invalidParam);  
    }
}


exports.removeRelation = function(req, res){
    
    if(typeof req.params.idrelation != "undefined")
    {
        db.UserRelation.find({where:{id: req.params.idrelation}}).then(function(user_relation){
			if(user_relation != null && (user_relation.id_user1 == req.user.id || user_relation.id_user2 == req.user.id))
            {
              if(!user_relation.is_deleted) {
            	db.ActivityStream.emit(14, req.user.id, function(activity){
		       	 	 user_relation.is_deleted = true;
			   	 	 
	                user_relation.save().then(function(user_relation){
	                   db.ActivityStream.emit(14, req.user.id, function(activity){
					   		 db.UserRelationStatus.create({
				               	id_relation: user_relation.id,
				               	id_activity: activity.id
			                }).then(function(){
				                res.send('ok');
			                });	
		                });
	                });   
              	});
              }
              else {
	              errors.sendError(res, code_erros.invalidParam, 'Friendship also deleted');  
              }
            }
            else
            {
               errors.sendError(res, code_erros.invalidParam); 
            }
        })
    }
    else
    {
        errors.sendError(res, code_erros.invalidParam);  
    }

}

exports.editRelation = function(req, res){
    
    if(typeof req.params.idrelation != "undefined" && typeof req.body.id_type_relation != "undefined")
    {
        db.UserRelation.find({where:{id: req.params.idrelation}}).then(function(user_relation){
            if(user_relation != null && (user_relation.user1 == req.user.id || user_relation.user2 == req.user.id))
            {
                user_relation.id_type_relation = req.body.id_type_relation;
                user_relation.save().then(function(user_relation){
                    res.send("ok");
                })
            }
            else
            {
               errors.sendError(res, code_erros.invalidParam); 
            }
        })
    }
    else
    {
        errors.sendError(res, code_erros.invalidParam);  
    }
    
}


exports.getAllRelation = function(req, res){
        
    if(typeof req.params.userid != "undefined")
    {
        var user_id = null;
        if(req.params.userid == "me")
            user_id = req.user.id;
        else
            user_id = req.params.userid;
        
        db.UserRelation.findAll({
            where: { $or: {
                  id_user1: user_id,
                  id_user2: user_id
                }, is_confirmed: true, is_deleted: false}})
            .then(function(data){ 
				res.send(data);
        });
    }
    else
    {
        errors.sendError(res, code_erros.invalidParam);  
    }
}

exports.getRelationdetail = function(req, res){
	if(req.query.id_activity === undefined || req.query.id_activity == '' || isNaN(req.query.id_activity ))
		 errors.sendError(res, code_erros.invalidParam); 
    else{ 
	    db.UserRelationStatus.find({where: {id_activity: req.query.id_activity}}).then(function(activity){
		 if(activity != null){
			    db.UserRelation.find({where: {id: activity.id_relation}}).then(function(friend){
				   	res.send(friend);
			    });
		    }
		    else{
			    errors.sendError(res, code_erros.invalidParam); 
		    }
	    });
    }
};

exports.checKP = function(req,res,next){
	if(req.params.iduser === undefined || req.params.iduser == '' || isNaN(req.params.iduser) || req.params.iduser <0 ){
		errors.sendError(res, code_erros.invalidParam); 
	}
	else{
		db.User.find({where: {id: req.params.iduser}}).then(function(user){
			if(user != null)
				next();
			else
				errors.sendError(res, code_erros.invalidParam); 
		})
	}
}

exports.getFriendCount = function(req, res){
	db.sequelize.query("CALL findCommonFriendNumber(:user1, :user2)",{replacements:{user1: req.user.id, user2: req.params.iduser}}).then(function(cont){
		res.send({commond_friend_count: cont[0].common_friend});
	});
}