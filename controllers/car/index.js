var extend = require('./class.js');

module.exports = function(app) {
	
	/**
	*
	*   @api {get} /car/maker Retrive car maker
	*
	*   @apiName GetCarMaker
	*	@apiGroup Car
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive car maker list
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/car/maker
	*	
	*
	*   @apiParam {number} [maker] The id car maker
	*	@apiParam {number} [model]  The id car model
	*	@apiParam {number} [allestiment]  The id car allestiment
	*	@apiParam {number} [fuel]  The id of fuel type
	*	@apiParam {number} [power]  The id of power
	*	@apiParam {number} [generic]  The id car of generic model
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    [{"id":1,"Descrizione":"Abarth"},{"id":2,"Descrizione":"Ac "}]	
	*/
	
	/**
	*
	*   @api {get} /car/model Retrive car model 
	*
	*   @apiName GetCarModel
	*	@apiGroup Car
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive car model list. This function nedd at least 1 params filter
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/car/model
	*	
	*
	*   @apiParam {number} [maker] The id car maker
	*	@apiParam {number} [model]  The id car model
	*	@apiParam {number} [allestiment]  The id car allestiment
	*	@apiParam {number} [fuel]  The id of fuel type
	*	@apiParam {number} [power]  The id of power
	*	@apiParam {number} [generic]  The id car of generic model
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    [{"id":1,"Descrizione":"500"}]
	*/
	
	/**
	*
	*   @api {get} /car/fuel Retrive car fuel 
	*
	*   @apiName GetCarFuel
	*	@apiGroup Car
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive car fuel type list. This function nedd at least 1 params filter
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/car/fuel
	*	
	*
	*   @apiParam {number} [maker] The id car maker
	*	@apiParam {number} [model]  The id car model
	*	@apiParam {number} [allestiment]  The id car allestiment
	*	@apiParam {number} [fuel]  The id of fuel type
	*	@apiParam {number} [power]  The id of power
	*	@apiParam {number} [generic]  The id car of generic model
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    [{"id":1,"Descrizione":"benzina"}]
	*/
	
	/**
	*
	*   @api {get} /car/power Retrive car power 
	*
	*   @apiName GetCarPower
	*	@apiGroup Car
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive car power list. This function nedd at least 1 params filter
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/car/power
	*	
	*
	*   @apiParam {number} [maker] The id car maker
	*	@apiParam {number} [model]  The id car model
	*	@apiParam {number} [allestiment]  The id car allestiment
	*	@apiParam {number} [fuel]  The id of fuel type
	*	@apiParam {number} [power]  The id of power
	*	@apiParam {number} [generic]  The id car of generic model
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    [{"id":113,"Descrizione":135},{"id":118,"Descrizione":140}]
	*/
	
	
	/**
	*
	*   @api {get} /car/generic Retrive car model 
	*
	*   @apiName GetCarModelGeneric
	*	@apiGroup Car
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive car model generic list. This function nedd at least 1 params filter
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/car/generic
	*	
	*
	*   @apiParam {number} [maker] The id car maker
	*	@apiParam {number} [model]  The id car model
	*	@apiParam {number} [allestiment]  The id car allestiment
	*	@apiParam {number} [fuel]  The id of fuel type
	*	@apiParam {number} [power]  The id of power
	*	@apiParam {number} [generic]  The id car of generic model
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    [{"id":64,"Descrizione":"500"}]
	*/
	
	
	/**
	*
	*   @api {get} /car/allestiment Retrive car allestiment
	*
	*   @apiName GetCarAllestiment
	*	@apiGroup Car
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive cars allestiment list. This function nedd at least 1 params filter
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/car/generic
	*	
	*
	*   @apiParam {number} [maker] The id car maker
	*	@apiParam {number} [model]  The id car model
	*	@apiParam {number} [allestiment]  The id car allestiment
	*	@apiParam {number} [fuel]  The id of fuel type
	*	@apiParam {number} [power]  The id of power
	*	@apiParam {number} [generic]  The id car of generic model
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    [{"id":1,"Descrizione":"500 1.4 Turbo T-Jet","Trazione":"anteriore","Trasmission":"meccanico 5 rapporti","cm3":"1368","Inizio_produzione":2008,"Fine_produzione":2013},{"id":2,"Descrizione":"500 1.4 Turbo T-Jet","Trazione":"anteriore","Trasmission":"meccanico 5 rapporti","cm3":"1368","Inizio_produzione":2008,"Fine_produzione":2013},{"id":3,"Descrizione":"500 1.4 Turbo T-Jet","Trazione":"anteriore","Trasmission":"meccanico 5 rapporti","cm3":"1368","Inizio_produzione":2008,"Fine_produzione":2013},{"id":4,"Descrizione":"500 1.4 Turbo T-Jet MTA","Trazione":"anteriore","Trasmission":"meccanico sequenziale con possibile funzionamento automatico a contr.elettronico 5 rapporti","cm3":"1368","Inizio_produzione":2008,"Fine_produzione":2013},{"id":5,"Descrizione":"500 1.4 Turbo T-Jet Custom","Trazione":"anteriore","Trasmission":"meccanico 5 rapporti","cm3":"1368","Inizio_produzione":2008,"Fine_produzione":2013},{"id":6,"Descrizione":"500 1.4 Turbo T-Jet MTA Custom","Trazione":"anteriore","Trasmission":"meccanico sequenziale con possibile funzionamento automatico a contr.elettronico 5 rapporti","cm3":"1368","Inizio_produzione":2008,"Fine_produzione":2013},{"id":7,"Descrizione":"500 C 1.4 Turbo T-Jet MTA","Trazione":"anteriore","Trasmission":"meccanico sequenziale con possibile funzionamento automatico a contr.elettronico 5 rapporti","cm3":"1368","Inizio_produzione":2008,"Fine_produzione":2013},{"id":8,"Descrizione":"500 C 1.4 Turbo T-Jet MTA","Trazione":"anteriore","Trasmission":"meccanico sequenziale con possibile funzionamento automatico a contr.elettronico 5 rapporti","cm3":"1368","Inizio_produzione":2008,"Fine_produzione":2013},{"id":9,"Descrizione":"500 C 1.4 Turbo T-Jet MTA Bicolore","Trazione":"anteriore","Trasmission":"meccanico sequenziale con possibile funzionamento automatico a contr.elettronico 5 rapporti","cm3":"1368","Inizio_produzione":2008,"Fine_produzione":2013},{"id":10,"Descrizione":"500 C 1.4 Turbo T-Jet","Trazione":"anteriore","Trasmission":"meccanico 5 rapporti","cm3":"1368","Inizio_produzione":2008,"Fine_produzione":2013},{"id":11,"Descrizione":"500 C 1.4 Turbo T-Jet","Trazione":"anteriore","Trasmission":"meccanico 5 rapporti","cm3":"1368","Inizio_produzione":2008,"Fine_produzione":2013},{"id":12,"Descrizione":"500 C 1.4 Turbo T-Jet Custom","Trazione":"anteriore","Trasmission":"meccanico 5 rapporti","cm3":"1368","Inizio_produzione":2008,"Fine_produzione":2013},{"id":13,"Descrizione":"500 C 1.4 Turbo T-Jet MTA Custom","Trazione":"anteriore","Trasmission":"meccanico sequenziale con possibile funzionamento automatico a contr.elettronico 5 rapporti","cm3":"1368","Inizio_produzione":2008,"Fine_produzione":2013}]

	*/
	
	
	app.post('/car/:type', extend.checkParam, extend.callBackCheck);
	
	
}

