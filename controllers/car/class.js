var db_car = global.db_car;
var errors = global.errors;

/**
*
*	Manage the server correct request.
*
**/
var bp = function() {
	
	/**
	*	Get the car maker
	*   @param {String} res - the object for printing result
	*   @param {Object} query - object containing filter for query
	*   @param {String} type - the type of the request
	*
	**/
	var getManufacter = function (res, req, type) {
		db_car.maker(res, req, type);
	}
	
	/**
	*	Get the car model
	*   @param {String} res - the object for printing result
	*   @param {Object} query - object containing filter for query
	*   @param {String} type - the type of the request
	*
	**/
	var getModel = function(res, req, type) {
		db_car.model(res, req, type);
	}
	
	/**
	*	Get the car fuel type
	*   @param {String} res - the object for printing result
	*   @param {Object} query - object containing filter for query
	*   @param {String} type - the type of the request
	*
	**/
	var getFuel = function(res, req, type) {
		db_car.fuel(res, req, type);
	}
	
	/**
	*	Get the car power
	*   @param {String} res - the object for printing result
	*   @param {Object} query - object containing filter for query
	*   @param {String} type - the type of the request
	*
	**/
	var getPower = function(res, req, type) {
		db_car.power(res, req, type);
	}
	
	/**
	*	Get the car allestiment
	*   @param {String} res - the object for printing result
	*   @param {Object} query - object containing filter for query
	*   @param {String} type - the type of the request
	*
	**/
	var getAllestiment = function(res, req, type) {
		db_car.allestiment(res, req, type);
	}
	
	/**
	*	Get the car model generic
	*   @param {String} res - the object for printing result
	*   @param {Object} query - object containing filter for query
	*   @param {String} type - the type of the request
	*
	**/
	var getGeneric = function(res, req, type) {
		db_car.generic(res, req, type);
	}
		
	return {
		maker: getManufacter,
		model: getModel,
		fuel : getFuel,
		power : getPower,
		allestiment : getAllestiment,
		generic : getGeneric
	};
}();

/**
*
*  Check the paramters of body request
*  @param {Object} req - The body of the request
*  @param {Object} res - The body of the response
* 
**/
module.exports.checkParam = function(req, res, next) {
	if(req.params.type != 'maker') {
		var i = 0;
		var block = false;
		var paramAllow = ['maker', 'model', 'allestiment', 'fuel', 'power', 'generic'];
		for(k in req.body) {
			if(paramAllow.indexOf(k) == -1 || req.body[k] == '' || isNaN(req.body[k])) {
				block = true;
				errors.sendError(res, 400);
			}
			
			i++;
		}
		
		if(i == 0) 
			errors.sendError(res, 400);
		else {
		  if(block == false)	
			  next();
		}	
	}
	else {
		next();
	}
}

module.exports.callBackCheck =  function(req, res) {
	 type = req.params.type;
	 switch(type) {
		 case 'maker':
		     bp.maker(res, req, type);
		 break
		 case 'model':
		     bp.model(res,req, type);
		 break
		 case 'fuel':
		     bp.fuel(res, req, type);
		 break
		 case 'power':
		      bp.power(res, req, type);
		 break
		 case 'generic':
		 	  bp.generic(res, req, type);
		 break
		 case 'allestiment':
		     bp.allestiment(res, req, type);
		 break  
	 }
	 res.status(200);
}