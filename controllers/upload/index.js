
var controller = require("./class");

/** @autentication */
module.exports = function(app)
{	
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {post} /upload/userprofile Upload user progile photo
	*
	*   @apiName UploadPhoto
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method upload user profile photo
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/activity/me?offset=0
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*
	*/
    app.post("/upload/userprofile", global.filters.auth, controller.uploadUserProfile);  
}

global.controllers.upload = controller;


