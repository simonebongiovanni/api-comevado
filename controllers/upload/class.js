var db = global.db;
var utilities = global.utilities;
var errors = global.errors;
var aws = global.aws;
var settings = global.settings;
var fs = require('fs');

/**
 * Upload Controller
 */

module.exports.uploadUserProfile = function(req, res) {
    
    var fstream;
    req.pipe(req.busboy);
    req.busboy.on('file', function (fieldname, file, filename) {
        console.log("Uploading: " + filename);
        filename = utilities.randomString(10) + filename.substr(filename.indexOf("."));
        
        fstream = fs.createWriteStream(settings.upload_dir + filename);
        file.pipe(fstream);
        fstream.on('close', function () {
            console.log("Upload Finished of " + filename);
            
            var file_user_image = "user_profile"+filename.substr(filename.indexOf("."));
            
            req.user.setInfo(9, file_user_image);
            
            aws.s3.upload(settings.upload_dir + filename, file_user_image, req.user, function(status, data){
                
                fs.unlink(settings.upload_dir + filename, function(){
                    
                    if(status)
                        res.send("ok");
                    else
                        errors.sendError(res, errors.codes.externalError, data);
                    
                })
                
            })
            
        });
    });

}

