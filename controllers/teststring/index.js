var utilities = global.utilities;

module.exports = function(app){
	
	/**
	*
	*   @api {get} /teststring Check string validation
	*
	*   @apiName CheckStringValidation
	*	@apiGroup Test
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method check string validation
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/teststring?string=pippo
	*
	*
	*   @apiParam {String} string The input string to check
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*		TO DO
	*/
	app.get("/teststring", function(req, res ){
		var string = req.query.string;
		
		var response = {
			empty: utilities.checkString.empty(string),
			checkValid: utilities.checkString.checkInvalidChar(string),
			checkSpace: utilities.checkString.checkSpace(string),
			checkSnail: utilities.checkString.checkSnail(string),
			checkPoint: utilities.checkString.checkPoint(string)
		};
		
		res.send(response);
	});
}