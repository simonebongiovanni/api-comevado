
var controller = require("./class.js");

module.exports = function(app) {
		
	app.get("/forcenotification", controller.forceNotification);
	app.get("/forcenotification/travel", controller.forceNotificationTravel);
	
	app.get("/fakehome", controller.fakehome);
	
}