var settings = global.settings;
var utilities = global.utilities;
var db = global.db;
var errors = global.errors;
var code_erros = global.errors.codes;

var travels = require("./localities.json");

global.fake_activity = {};

if(settings.fake_activity)
{
    autoFireFakeNotification();  
}

function autoFireFakeNotification(){
    
    db.NotificationType.findAll().then(function(notification_types){
        
        var min = utilities.randomInt(1, 5);
                
        setTimeout(function() {
                           
            fireNotify(
                function(status, data){  
                    autoFireFakeNotification();
                }
            );  
            
        }, min * 1000 * 60);
        
    })
}

function getDescription(type)
{
    var description = "";
            
    switch(type){
        case 1:
            description = "ha compiuto un'azione";
            break;
        case 2:
            description = "si è collegato";
            break;
        case 3: 
            description = "ha cercato un viaggio";
            break;
        case 4: 
            description = "ha stretto amicizia";
            break;
        case 5:
            description = "si è registrato"; 
            break;
        case 6:
            description = "ha ricevuto un promemoria"; 
            break;
    }
    
    return description;
}

exports.forceNotification = function(req, res)
{
    fireNotify(
        function(status, data){  
            if(status)
                res.send(data);
            else
                errors.sendError(res, code_erros.internalError, data)
        }
    );    
}

exports.forceNotificationTravel = function(req, res)
{
    db.NotificationType.findAll().then(function(notification_types){
        
        var sender = "";
        var type = {id: 3};
        
        var data = {
            travel_type: utilities.randomInt(1, 5),
            from: travels[utilities.randomInt(0, travels.length - 1)],
            to: travels[utilities.randomInt(0, travels.length - 1)]
        }
        
        var description = getDescription(type.id)+" da "+data.from.description+" a "+data.to.description;

        global.notificator.notifyAll(sender, description, type.id, data, function(status, data){          
            res.send(data); 
        });
    }); 
}


function fireNotify(callback)
{
    db.NotificationType.findAll().then(function(notification_types){
        
        var sender = "";
        var type = notification_types[utilities.randomInt(0, notification_types.length - 1)];

        var description = getDescription(type.id);

        var data = "";
                
        if(type.id == 3)
        {
            data = {
                travel_type: utilities.randomInt(1, 5),
                from: travels[utilities.randomInt(0, travels.length - 1)],
                to: travels[utilities.randomInt(0, travels.length - 1)]
            }
        }

        global.notificator.notifyAll(sender, description, type.id, data, function(status, data){          
            callback(status, data)
        });
    });
}

exports.fakehome = function(req,res){
	var query = "SELECT * FROM(SELECT * FROM fake_history WHERE id_type = 3 order by time desc LIMIT 6) a UNION SELECT * FROM (SELECT * FROM fake_history WHERE id_type = 2 order by time desc LIMIT 6) b UNION SELECT * FROM (SELECT * FROM fake_history WHERE id_type = 1 order by time desc LIMIT 6) c";
	
	db.sequelize.query(query, { type: db.sequelize.QueryTypes.SELECT}).then(function(fake){
		res.send({notification: fake});
	});
}

function generateUserCity(idtype, content, callback){
	var response = {};
	db.sequelize.query("SELECT * FROM fake_user ORDER BY RAND() LIMIT 1", { type: db.sequelize.QueryTypes.SELECT}).then(function(user){
		response.name = user[0].name + " " + user[0].surname;
		
		db.sequelize.query("SELECT * FROM fake_city ORDER BY RAND() LIMIT 1", { type: db.sequelize.QueryTypes.SELECT}).then(function(fc){
			response.from = fc[0].value;
			
			do{
				db.sequelize.query("SELECT * FROM fake_city ORDER BY RAND() LIMIT 1", { type: db.sequelize.QueryTypes.SELECT}).then(function(tc){
					response.to = tc[0].value;
					
					if(response.from != response.to){
						db.FakeHistory.build({
							id_user: user[0].id,
							id_from: fc[0].id,
							id_to: tc[0].id,
							id_type: idtype,
							time: db.sequelize.fn('NOW'),
							content: content
						}).save().then(function(datacreated){
							response.id = datacreated.id;
							response.type = idtype;
							response.subtype = content;
							
							callback(response);
						});
					}
				});
			}while(response.from == response.to && (response.from.length + response.to.length) > 20);
		});
	});
}

setInterval(function(){
	var type = ['car', 'price', 'speed', 'co2', 'confort'];
	
	var select =  Math.floor(Math.random() * type.length);

	generateUserCity(1, type[select], function(response){
			global.notificator.notifyAll('', '', 12, response, function(status, data){     
				db.sequelize.query("UPDATE fake_history SET data = '" + JSON.stringify(data.data) + "' WHERE id = "+ response.id).then(function(){
						
				});   
	        });
	        
	        db.sequelize.query("DELETE FROM fake_history WHERE id NOT IN (SELECT d.id FROM (SELECT * FROM(SELECT * FROM fake_history WHERE id_type = 3 order by time desc LIMIT 15) a UNION SELECT * FROM (SELECT * FROM fake_history WHERE id_type = 2 order by time desc LIMIT 15) b UNION SELECT * FROM (SELECT * FROM fake_history WHERE id_type = 1 order by time desc LIMIT 15) c)d) ");

		});
}, 7000);

setInterval(function(){
	generateUserCity(2, '', function(response){
			global.notificator.notifyAll('', '', 12, response, function(status, data){     
				db.sequelize.query("UPDATE fake_history SET data = '" +  JSON.stringify(data.data) + "' WHERE id = "+ response.id).then(function(){		
				});     
	        });

		});
},45000);

setInterval(function(){
	var type = ['manred-saved', 'search', 'manyellow', 'manred-create', 'manred-buy'];
	var select =  Math.floor(Math.random() * type.length);
	
	generateUserCity(3, type[select], function(response){
			if(select == 2){					
				findAnother(response.id_user, function(second_user){
					response.second_user = second_user;
					
					
					global.notificator.notifyAll('', '', 12, response, function(status, data){     
						
						db.sequelize.query("UPDATE fake_history SET data = '" +  JSON.stringify(data.data) + "' WHERE id = "+ response.id).then(function(){
						
						});   
			        });
				})
			}
			else{
				global.notificator.notifyAll('', '', 12, response, function(status, data){     
					db.sequelize.query("UPDATE fake_history SET data = '" +  JSON.stringify(data.data) + "' WHERE id = "+ response.id).then(function(){
						
					}); 
		        });
			}
		});
},2000);

var findAnother = function(user, callback){
	db.sequelize.query("SELECT * FROM fake_user ORDER BY RAND() LIMIT 1", { type: db.sequelize.QueryTypes.SELECT}).then(function(user){
		mem = user[0].name + " " + user[0].surname;
		
		if(mem == user){
			findAnother(user, callback);
		}
		else{
			callback(mem);
		}
	});	
}