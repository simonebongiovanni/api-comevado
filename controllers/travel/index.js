var controllers = require("./class.js");
var filters = global.filters;


module.exports = function(app) {
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {post} /travel/addsearch Add generic search
	*
	*   @apiName AddGenericSearch
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method add generic search to db
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/addsearch
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {String} startLocation The name of start location
	*	@apiParam {Float} sLat  The start location latitude
	*	@apiParam {Float} sLng  The start location longitude
	*   @apiParam {String} endLocation The name of end location
	*	@apiParam {Float} eLat  The end location latitude
	*	@apiParam {Float} eLng  The end location longitude
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/ 
	app.post("/travel/addsearch", filters.authOrNot, controllers.checkParamTravelNew, controllers.checkLocalityStart, controllers.checkLocalityEnd, controllers.callbackNewTravel);
	
	/**
	*
	*   @api {get} /travel/me/addussearch Add user search
	*
	*   @apiName AddGenericSearch
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method add user search
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/me/addussearch
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {String} search The generic search id of travel
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/ 
	app.get("/travel/me/addussearch", filters.auth, controllers.checkParamUserSearch, controllers.callbackInsertUserSearch);

	/**
	*
	*   @api {post} /travel/save Save travel
	*
	*   @apiName AddGenericSearch
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method save travel in db. It'll be used for building wishlist
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/save
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {Object} route The route of the travel
	*	@apiParam {String} type The solution type (co2/price/comfort/car/speed)
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/ 
    app.post("/travel/save", filters.auth, controllers.saveTravel);
    
    /**
	*
	*   @api {get} /travel/state/:idactivity Travel state from activity
	*
	*   @apiName RetriveTravelStatusFromActivity
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive travel state from activity id
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/state/6
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {Number} idactivity The activity id
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/ 
	app.get("/travel/state/:idactivity", filters.auth, controllers.checkTravelParams, controllers.callbackTravState);
    
    /**
	*
	*   @api {post} /travel/issaved Check travel is saved
	*
	*   @apiName CheckTravelSaved
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method check if travel is saved
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/issaved
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {Number} path The activity path of the travel
	*   @apiParam {Object} date The date of the travel
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/ 
    app.post("/travel/issaved", filters.auth, controllers.isSaved);
    
    /**
	*
	*   @api {get} /travel/saved/all Get travel saved list
	*
	*   @apiName GetTravelSavedList
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method get user travel saved list
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/saved/all
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {Number} offset The offset from taking travel
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/ 
    app.get("/travel/saved/all", filters.auth, controllers.checkParamAll, controllers.allSavedTravel);
    
    /**
	*
	*   @api {get} /travel/saved/:idtravel/details Get travel saved detail
	*
	*   @apiName GetTravelSavedDetail
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method get travel saved detail
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/saved/1/details
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {Number} idtravel The id of the travel to get detail
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/ 
    app.get("/travel/saved/:idtravel/details", filters.auth, controllers.savedTravelDetails);
	
	/**
	*
	*   @api {get} /travel/saved/:idtravel/pdf Get travel pdf name
	*
	*   @apiName GetTravelPdf
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method get pdf travel
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/saved/1/pdf
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {Number} idtravel The id of the travel to get pdf
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/ 
    app.get("/travel/saved/:idtravel/pdf", filters.auth, controllers.createPDF);
    
    
	/**
	*
	*   @api {get} /travel/:idtravel/done Set travel done
	*
	*   @apiName SetTravelDone
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method set travel done. It set travel_done column from table travel_saved from 0 to 1 and all its segment set also  segment_done to 1
    *  
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/1/done
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {Number} idtravel The id of the travel.
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/
    app.get("/travel/:idtravel/done", filters.auth, controllers.travelDone);
    
   	/**
	*
	*   @api {get} /travel/:idtravel/:idsegment/done Set segment to done
	*
	*   @apiName SetSegmentDone
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method set segment to done. It set segment_done column from table travel_segment_saved from 0 to 1 and if all segment_done are 1, travel_done will become 1
    *  
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/1/1/done
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {Number} idtravel The id of the travel.
	*   @apiParam {Number} idsegment The id of the segment.
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/
    app.get("/travel/:idtravel/:idsegment/done", filters.auth, controllers.segmentDone);
    
   	/**
	*
	*   @api {get} /travel/:idtravel/buyed Set travel to buyed
	*
	*   @apiName SetTravelBuyed
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method set travel to buy. It also set all segment to buy.
    *  
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/1/buyed
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {Number} idtravel The id of the travel.
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/
    app.get("/travel/:idtravel/buyed", filters.auth, controllers.travelBuyed);
    
     
   	/**
	*
	*   @api {get} /travel/:idtravel/:idsegment/buyed Set segment to buyed
	*
	*   @apiName SetSegmentBuyed
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method set segment buyed
    *  
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/1/1/buyed
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {Number} idtravel The id of the travel.
	*   @apiParam {Number} idsegment The id of the segment.
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/
    app.get("/travel/:idtravel/:idsegment/buyed", filters.auth, controllers.segmentBuyed);
    
    /**
	*
	*   @api {get} /travel/saved/download/:file Download PDF travel
	*
	*   @apiName DownloadPDFTravel
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method download PDF from the server
    *  
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/saved/download/prova.pdf
	*	
	*
	*   @apiParam {Number} idtravel The id of the travel.
	*   @apiParam {Number} idsegment The id of the segment.
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/
    app.get("/travel/saved/download/:file",  controllers.downloadPDF);
    
   	/**
	*
	*   @api {post} /travel/detail Get travel detail
	*
	*   @apiName GetTravelDetail
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive travel info and other user that made same travel
    *  
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/detail
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {Number} travel The id of the travel.
	*   @apiParam {Date} date The date of the travel.
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/
	app.post("/travel/detail", filters.auth, controllers.checkParamTravelR, controllers.callbackTravelDetail);

	/*
	*
	*	Draw google path beetween two point 
	*
	*	@param {Number} sLat - start latitude
	*	@param {Number} sLng - start longitude
	*   @param {Number} eLat - end latitude
	*	@param {Number} eLng - end longitude
	* 
	* 	DA VERIFICARE......FORSE DUPLICATA
	*/
	app.post("/travel/staticpath", controllers.checkParamInsert, controllers.callbackStaticPath);  
	
	/**
	*
	*   @api {get} /travel/:idtravel/delete Delete saved travel
	*
	*   @apiName DeleteSavedTravel
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method delete a saved travel
    *  
	*	@apiExample  Example usage:
	*      http://api.comevado.com/travel/1/delete
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {Number} idtravel The id of the travel.
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/
	app.get("/travel/:idtravel/delete", filters.auth, controllers.checkDeleteParams, controllers.deleteTravel);
	
	
	/**
	*
	*   @api {get} /user/travel/info Get user travel info km and co2 consuption
	*
	*   @apiName GetTravelDetails
	*	@apiGroup Travel
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive total co2 consuption and km made calculated by all travel saved
    *  
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/travel/info
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/
	app.get("/user/travel/info", filters.auth, controllers.getUserTravelInfo);
	
	/*
	 * Get travel reminder
	 *
	 *	da rivedere
	 */
	app.get("/user/travel/reminder", filters.auth, controllers.getTravelReminder);
	
	
	/**
	*
	*   @api {get} /user/search/list Get user search list
	*
	*   @apiName GetUserSearch
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive user search list
    *  
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/search/list
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*	 TO DO
	*/
	app.get("/user/search/list", filters.auth, controllers.callbackSearchList)
}; 