var db = global.db;
var errors = global.errors;
var code_erros = global.errors.codes;
var async = require("async");
var utilities = global.utilities;
var moment = require('moment');
var fs = require('fs');


module.exports.checkParamTravelNew = function(req,res, next){
	
	if(req.body.startLocation === undefined || req.body.endLocation === undefined || req.body.sLat === undefined ||req.body.eLat === undefined || req.body.sLng === undefined || req.body.eLng === undefined || req.body.startLocation == '' || req.body.endLocation == '' || req.body.sLat == '' || req.body.eLat == '' || req.body.sLng == '' || req.body.eLng == '' || isNaN(req.body.sLat) || isNaN(req.body.eLat) || isNaN(req.body.sLng) || isNaN(req.body.eLng))
	 	 errors.sendError(res, errors.codes.invalidParam);
	else 
	 	next();
}

module.exports.checkLocalityStart = function(req, res, next){
	db.locality.findAll({
		where: 
			["lat LIKE ? AND lng LIKE ?", req.body.sLat, req.body.sLng]
	}).then(function(ris){

		if(ris.length == 0) {
			db.locality.addCity(req.body.startLocation, req.body.sLat, req.body.sLng, function(data){ 
				req.body.startCode = data.dataValues.id;
				next();
			});
		} else{
			req.body.startCode = ris[0].dataValues.id;
			next();
		}
	});
};

module.exports.checkLocalityEnd = function(req, res, next){
	db.locality.findAll({
		where: 
			["lat LIKE ? AND lng LIKE ?", req.body.eLat, req.body.eLng]
	}).then(function(ris){
		if(ris.length == 0) {
			db.locality.addCity(req.body.endLocation, req.body.eLat, req.body.eLng, function(data){
				req.body.endCode = data.dataValues.id;
				next();
			});
		}
		else{
			req.body.endCode = ris[0].dataValues.id;
			next();
		}
	});
};


module.exports.callbackNewTravel = function(req, res) {
	
    var user_id = null;
    if(req.user)
        user_id = req.user.id;
    
    db.travel.findAll({
		where : {
			id_start: req.body.startCode,
			id_end: req.body.endCode
		}
	}).then(function(rs){
		if(rs.length == 0) {
			db.travel.insertTravel(req.body.startCode, req.body.endCode, function(data){
                                
                db.locality.find({where:{ id: req.body.startCode }}).then(function(l_from){
                    db.locality.find({where:{ id: req.body.endCode }}).then(function(l_to){
                         global.notificator.notify(user_id, "ha appena cercato un viaggio", 3, '', {
                                from: l_from.dataValues,
                                to: l_to.dataValues
                           });
                    })
                });
             
                res.send(data);
			});
		}
		else {
            
            db.locality.find({where:{ id: req.body.startCode }}).then(function(l_from){
                db.locality.find({where:{ id: req.body.endCode }}).then(function(l_to){
                    global.notificator.notify(user_id, "ha appena cercato un viaggio", 3,'', {
                            from: l_from.dataValues,
                            to: l_to.dataValues
                       });
                })
            })
            
			res.send(rs[0]);
		}
        
	});
};

module.exports.checkParamUserSearch = function(req, res, next){
	if(req.query.search === undefined || req.query.search == '')
		errors.sendError(res, errors.codes.invalidParam);
	else
		next();
}

module.exports.callbackInsertUserSearch = function(req, res){
	db.ActivityStream.emit(4, req.user.id, function(data1){
		db.usertravelsearch.addSearch(req.user.id, req.query.search, data1.dataValues.id, function(data){
			res.send({activity: data1.dataValues.id})
		})
	});
}

module.exports.checkParamStatusTravel = function(req, res, next){
	if(req.query.search === undefined || req.query.search == '' || isNaN(req.query.search)){
		errors.sendError(res, errors.codes.invalidParam);
	}else {
		db.travelstate.findAll().then(function(ri){
			for(i = 0; i < ri.length; i++) {
				if(ri[i].dataValues.description == req.params.action)
					req.query.idaction = ri[i].dataValues.id;
			}
			
			if(!req.query.idaction)
				errors.sendError(res, errors.codes.invalidParam);
		}).then(function(){
			db.usertravelstate.find({
				where:{
					id_user: req.user.id,
					id_travel: req.query.search,
					id_state: req.query.idaction
				}
			}).then(function(data){
				
				if(data == null)
					next()
				else
					errors.sendError(res, errors.codes.invalidParam, 'travel is alredy ' + req.params.action);
	
			})
		});
	}
}

module.exports.callbackTravelDetail = function(req, res){
	var result = [];
	var data = new Date(req.body.data);
	
	db.travel
		.find({
			where: {
				id: req.body.travel
			},
			include:[
				{model:db.locality, as: 'LocalityStart'},
				 {model:db.locality, as: 'LocalityEnd'}
			]}
		)
		.then(function(ris){
			 db.usertravelsearch.searchSameTravel(data, req.body.travel, function(rs){
				 var user = [];
				  
				 rs.forEach(function(d){
					 if(d.id_author != req.user.id && user.indexOf(d.id_author) == - 1)
						 user.push(d.id_author);
				 });
				 
				 var result = {
					from: ris.dataValues.LocalityStart.dataValues.description,
					to: ris.dataValues.LocalityEnd.dataValues.description,
					other_user: user || undefined
				 }
				 
				 res.send(result);
			 });
		});
};

module.exports.checkParamTravelR = function(req, res, next){
	if(req.body.data === undefined || req.body.data == '' || req.body.travel == '' || req.body.travel === undefined || isNaN(req.body.travel))
		errors.sendError(res, errors.codes.invalidParam);
	else
		next();
}

module.exports.saveTravel = function(req, res){
    
    var route = req.body.route;
    route.date = new Date(route.date);
    route.path = utilities.md5(route.path);
    
    if(typeof route.date != "undefined" && typeof route.path != "undefined")
    {
        db.TravelSaved.find({
            where: {
                    path: route.path,
                    date: route.date
                   }
        }).then(function(data){
            if(data == null)
            {
                 db.TravelSaved.create({
                    id_user: req.user.id,
                    id_travel_searched: route.id_travel_searched,
                    from: route.from,
                    to: route.to,
                    date: route.date,
                    distance: route.distance,
                    duration: route.duration,
                    total_transfer_duration: route.totalTransferDuration,
                    indicative_price: route.indicativePrice.price,
                    co2: route.co2,
                    path: route.path,
                    solution_type: req.body.type
                }).then(function(travelsaved){
					
					db.ActivityStream.emit(5, req.user.id, function(activity){
						db.TravelState.create({
							id_activity: activity.id,
							id_travel: travelsaved.id,
							id_state: 1
						});
					});
					
					if(route.kind == 'car' && route.subkind == 'car') {			
	                        db.TravelSavedSegment.create({
	                            id_travel_saved: travelsaved.id,
	                            kind: route.subkind,
	                            vehicle: route.vehicle,
	                            distance: route.distance,
	                            duration: route.duration,
	                            s_name: route.sName,
	                            s_pos: route.sPos,
	                            t_name: route.tName,
	                            t_pos: route.tPos,
	                            path: route.path,
	                            indicative_price: route.indicativePrice.price,
	                            company: '',
	                            iconPath: '',
	                            iconOffset: '',
	                            link: ''                           
	                        }).then(function(segmentsaved){
	                          	res.send({
	                            	"travelsaved_id": travelsaved.id
	                      		}); 
	                        })
	                }
					else {
						
						saveSegmentTravel(0, route, travelsaved, function(){
							res.send({
	                            "travelsaved_id": travelsaved.id
	                        });  
						});
					}      
                })
            }
            else
            {
                errors.sendError(res, errors.codes.invalidParam, 'travel already saved');
            }
        });
    }
    else
    {
        errors.sendError(res, errors.codes.invalidParam, 'path or date not valid');
    }
}

var saveSegmentTravel = function(index, route, travelsaved, callback){
	
	
	if(index < route.segments.length){
		segment = route.segments[index];
		
		if(segment.link === undefined){
			if(segment.itineraries)
				var link = segment.itineraries[0].legs[0].url;
			else
				var link = '';
		}
		else{
			var link = segment.link;
		}
		
		
		db.TravelSavedSegment.create({
	        id_travel_saved: travelsaved.id,
	        kind: segment.subkind,
	        vehicle: segment.vehicle,
	        distance: segment.distance,
	        duration: segment.duration,
	        s_name: segment.sName,
	        s_pos: segment.sPos,
	        t_name: segment.tName,
	        t_pos: segment.tPos,
	        path: segment.path,
	        indicative_price: segment.indicativePrice.price,
	        company: segment.company,
	        astart_code: segment.SACode,
			aend_code: segment.STCode,
	        iconPath: segment.iconPath,
	        iconOffset: segment.iconOffset,
	        link: link              
	    }).then(function(segmentsaved){
		    index++; 
		    
			saveSegmentTravel(index, route, travelsaved, callback);
	    });
	}
	else {
		 callback();
	}
}

module.exports.isSaved = function(req, res){
    var route = req.body;
    route.date = new Date(route.date);
    route.path = utilities.md5(route.path);
    
    if(typeof route.date != "undefined" && typeof route.path != "undefined")
    {
        db.TravelSaved.find({
            where: {
                    path: route.path,
                    date: route.date
                   }
        }).then(function(data){
            if(data == null)
                res.send("false");
            else
                res.send("true");
        });
    }
    else
    {
        errors.sendError(res, errors.codes.invalidParam, 'path or date not valid');
    }
}

module.exports.allSavedTravel = function(req, res){
    
     db.TravelSaved.findAll({
         where:{
         	id_user: req.user.id,
         	in_wishlist: 1
         },
         include: [db.TravelSavedSegment],
         order: [["id","DESC"]],
		 offset: req.query.offset,
		 limit: 10,
        
     }).then(function(data){
	     scoreTravel(0, data, req, res);
	     
			    	     
	   /*   db.TravelSaved.findAll({
	         where:{
	         	id_user: req.user.id,
	         	in_wishlist: 1
	         }     
	     }).then(function(tr){
		    
		     
		     //res.send({count: tr.length, travel:data});
	     });*/
     });
}

function scoreTravel(indexTravel, travels, req, res){
	if(indexTravel < travels.length){
		travel = travels[indexTravel];
		
		db.TravelGroup.find({where: {id_travel: travel.id}})
	    	.then(function(gr){
		    	if(gr != null){
			    	db.Groups.find({where: {id: gr.id}})
			    		.then(function(groupinfo){
				    		travels[indexTravel].dataValues.group = {};
				    		
				    		db.GroupUser.findAll({where: {id_group: gr.id, is_delete: false}}).then(function(groupuser){
					    		
					    		scoreUser(0, groupuser, function(){
						    		db.Comment.findAll({where:{id_parent: 0},include: [{model: db.ActivityStream, include:[{model: db.GroupsActivity, where: {id_groups: groupinfo.id}}]}]})
							    		.then(function(comment){
							    	
								    		groupinfo.dataValues.comment = comment;
								    		groupinfo.dataValues.groups_member = groupuser;
								    		
								    		travels[indexTravel].dataValues.group = groupinfo;
								    		
								    		scoreComment(0, comment, function(){
									    		indexTravel++;
												scoreTravel(indexTravel, travels, req, res);
								    		});
						    		});
					    		});
				    		});
				    		
			    		});
		    	}
		    	else{
			    	indexTravel++;
			    	scoreTravel(indexTravel, travels, req, res);
		    	}
	    	});
		 

	}
	else{
		 db.TravelSaved.findAll({
	         where:{
	         	id_user: req.user.id,
	         	in_wishlist: 1
	         }     
	     }).then(function(tr){
		     res.send({count: tr.length, travel:travels});
	     });
	}
	
}

function scoreComment(indexComment, comment, callback){
	if(indexComment < comment.length){
		db.getUserInfo(comment[indexComment].id_author, function(userinfo){
			comment[indexComment].dataValues.authorinfo = userinfo;  			
			
			comment[indexComment].dataValues.preferences = {};
									    		
			comment[indexComment].dataValues.preferences.like = [];
			comment[indexComment].dataValues.preferences.dislike = [];
			
			delete comment[indexComment].dataValues.activityStream;
			
			db.Comment.findAll({where : {id_parent: comment[indexComment].id}}).then(function(pref){
				
				scorePreferences(0, pref, comment[indexComment], function(){
					indexComment++;
					scoreComment(indexComment, comment, callback);
				});
			});		
		});		
	}
	else{
		callback();
	}
}

function scorePreferences (indexPreferences, preferences, c, callback){
	if(indexPreferences < preferences.length){
		if(preferences[indexPreferences].is_like)
			c.dataValues.preferences.like.push(preferences[indexPreferences]);
			
		if(preferences[indexPreferences].is_dislike)
			c.dataValues.preferences.dislike.push(preferences[indexPreferences]);
			
		indexPreferences++;
		scorePreferences(indexPreferences, preferences, c, callback);
	}
	else{
		callback();
	}
}

function scoreUser(indexUs, users, callback){
	if(indexUs < users.length){
		
		
		 db.getUserInfo(users[indexUs].id_user, function(data){
				users[indexUs].dataValues.info = data;
				indexUs++;			
				scoreUser(scoreUser, users, callback);
			});
	}
	else{
		callback();
	}
	
}

module.exports.savedTravelDetails = function(req, res){
    /*
	    SELECT travel_saved.*, travel_state_type.description FROM travel_saved 
	    JOIN travel_saved_segment ON travel_saved_segment.id_travel_saved = travel_saved.id
	    JOIN travel_state ON travel_state.id_travel = travel_saved.id
	    JOIN activity_stream ON activity_stream.id = travel_state.id_activity
	    JOIN travel_state_type ON travel_state_type.id = travel_state.id_state
	    WHERE travel_saved.id = :idtravel
	  	  
	*/
    
    
    db.TravelSaved.find({
        where:{id: req.params.idtravel}, 
        include: [db.TravelSavedSegment]
    }).then(function(travel){
        console.log(travel);  
        if(travel != null)
        {
	        
	        db.TravelState.findAll({where: {id_travel: travel.id}, include: [ db.ActivityStream]})
    			.then(function(ts){
	     				for(i = 0; i < ts.length; i++){
		     				
		     				if(ts[i].id_state == 2){
			     				travel.dataValues.dataBuyed = moment(ts[i].activityStream.data).format("DD-MM-YYYY");
			     				
		     				}
		     			
		     				if(ts[i].id_state == 3){
			     				
			     				travel.dataValues.dataDone = moment(ts[i].activityStream.data).format("DD-MM-YYYY");

		     				}
	     				}
	     				
	     				travel.dataValues.date = moment(travel.dataValues.date).format("DD-MM-YYYY");
		 					res.send(travel);	     				 
    			});
        }
        else
        {
            errors.sendError(res, errors.codes.invalidParam, 'travel saved not found');
        }
         
     })
}

module.exports.callbackStaticPath = function(req, res){
	utilities.https.get("maps.googleapis.com", "/maps/api/directions/json?origin=" + req.body.sLat + "," + req.body.sLng+"&destination=" + req.body.eLat + "," +  req.body.eLng + "&key=AIzaSyA0nmo_Sin6FAu1doMW8OPWETigyX73NdM", function(status, data){ 

		var polyline = "";
		var decoded = "";
		var path = [];
		if(data.status != 'NOT_FOUND'){
			var data = JSON.parse(data);
			var steps = data.routes[0].legs[0].steps
			
			steps.forEach(function(items){
				polyline = items.polyline.points;
				var decoded = utilities.decodePolyline(polyline);
				
				decoded.forEach(function(items){
					path.push({lat: items[0], lng: items[1]});
				});
				
			});
		}		
		
		res.send({path: path});
	})
}

module.exports.checkParamInsert = function(req, res, next){
	if(req.body.sLat === undefined || req.body.sLng === undefined || req.body.eLat === undefined || req.body.eLng === undefined || req.body.sLat == '' || req.body.sLng == '' || req.body.eLat == '' || req.body.eLng == '' || isNaN(req.body.sLat) || isNaN(req.body.sLng) || isNaN(req.body.eLat) || isNaN(req.body.eLng))
		 errors.sendError(res, errors.codes.invalidParam);
    else
    	next();
}



module.exports.createPDF = function(req, res){
    
    db.TravelSaved.find({
        where:{id_user: req.user.id, id: req.params.idtravel}, 
        include: [db.TravelSavedSegment]
    }).then(function(data){
        
        if(data != null)
        {
            var imageLogo_y = 'data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAKMAAACiCAYAAAAtMH0lAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAIGNIUk0AAG2YAABzjgAA+H8AAILqAABwogAA61oAADDkAAAPtwOsfnYAAAzhSURBVHja7J1LdpvKFoZ/vDwAMoLgVugFt7lrGY8g8ggsjSDWCGyPwPIIzBmB5REYrxXaIT1uy2QEhzsCboOtY44i8aqiqIL9dxK/BKI+7UfVrl1WURRgsXTQCT8CFsPIYjGMLIaRxWIYWQwji9VRp4e+aVnWbB9IkfoBABuAB+AzAId+5NH32yoHkND/MwC/6evccuNots+3ZirROvTDucBI4AUAvhJsjsLLZwTnLwDRXABlGD/g8wAsAFwQhLopAvAGYGu5ccIwTgzGIvUXAL4RhLZBt54D2AJ4sdx4yzCa7X6vDQSwCcy/THfns4CxSH0bwBLAd8Wxn2plAB4BhJYb5wyjXhA6AG4nZAW7Wst7y40zhlEPCJdghaZAOSkYGUKzoZwEjBQTPjCEraFc6xhT1sFoxHJgkfo3AN4ZxNZaAnin52aMtLaMNEn9hHJ1hNVPCYCVLpPoRlrGIvXvAPxkEIXlAfhZpP4DW0a2hmwlTbSMFOOwNRzeSmoZS2phGSlTfkI5cc1Soy1ZSaUZt9ZTO+yWR1UG4Eql29bWTVNFzSuDOJocAK80DqPrZEQQlwCeMa/1ZB1lA3im8ZgfjEXqP5FrZumjJxqX+cBIb3jJY6+llmMCecIgsnQB8oRBZOkC5AmDyNIFyBMGkaULkCcDg7hkECcB5I2KCw22AkMg8vTNdLSy3DgU5kL1ciAt8b2CJ7SnpBzApejSoVIYqejhJ6a9XXSuygCcixRXqF6bfmYQJyuHxlf/BIaqswMes0kroHHWN4GhliKvPFaz0WWfViuDx4wcJ3L8qFPMeMsgDjbgkab35tC46+Om2T1LVQLgBZXmodRB430q7nowN83uWZr1+wtlV7HswDPWfTm1k7uug/FU8EZuGMTeylG2IAlrPuwO9F9OdYgD4Qy7t2U0wH3oDOEjgE2TNTGsyOSsTcOpoSzjLXPVKyZstYneEKu4z8NKuWWcWdKSE0QJgP9Vstts3xJQDO3RlwH9+5X+n1Gw3y62KitlHgx7Vo3JzBCW8Xbi8G1RnjoQdel1SKDtBiMSvI9vBj67W5H33dkyTtQq7gDU5mSBIvVNPQi81jrKtoxTsooZgHuU567kutwUleCZqt7W8bTjQwowjUKIDGW74bAjIDZNZTgoj3L7feB1EwntQmyDn21QpH7QZ926q2W8nhOEReo/04fP7vih3cWMmeXGnTNMy42jIvU3KOfvTNR1H+vYOmacwLziPVrM7cmO2yw37l2pTD1wngy1lAfnHWUVSnw32BqeW2581yMuFM2II0GQtwDOAGwMfO6deenippcGPpAtxHoQvlB8GKGc6smOxUIUTwMfRwMH9PeiljUHsC5S/80wK7kEsJbupsldPBsG4sZy4zUmJAM3ul3tT5XJcNOmJS6rqYFIVjIBcIlyXtSUREaeZaQlrr8NAzHEhGVYt99P1TBJ1DIuGERtLWRiwO225qcNjKaskYZzAHEvsbkywGW35qfWTRvkoreWG19hhjKkVuAfVy3ipgPFN52jnEaJOvxNBsE6OsMtZIRyQl9nteKoaZ7xQuENJ6jU+3X4xK/GLHKozC/+8SFRddSu5cZ3RepfQN+6gQuUc75CMKpKXrIdiLv2a5Ybb4rUv0d9lVDYZ0FeEL4FPdygKZstUn9n6V8wfGXQGuXmOGMt49GYUXG8uLLcOKS2GTv4VjSQ7zUu/UyVVaQWf6L7w0MMeDi55ntmPllunPeNGVWa/N3gfK1mYQ2D9qgCxCL1gyL131HO6zmCL7dEeQ703UC3q3Ps2MhTHYyewhvdXeux8j27IdHZKADxhuJWR/JL3xap/0reR2bsmJH11VGeCIwqk5fvlcxwVYHhGASDV2aTy2u7ISqpzAIkHSzFEJ19dbWOjTydamIZnSL1byw33lDsmJBlPJZAPSqwiMsGyxyi3DMTHXkNj+7/usayvsi+d8uNsyL1t9Bv5ayRp4MJDP77HwfjFNL+aznvSHFpZrnx2YAgeg1Z6YaSkLwj3LeK38eCANBpyucTvvzIu7ppZ6Sbfap21q8Ul97jY9lrO/A9PDR8WNZdQwTLjTcAzvdc+KDW3XLjLRUUX1K1+TmFQBuMt6bt9bGMNxh3A/kneqB55ZNuk+vcDjg1EuD4RPu95cZ3gq9vVxKis5En6x2UNaoqw7E1vvzYdLWM9oggrmnA3vesZE4xZTbgtY/V32WiIFY+XFdd3fxAljOD+sofu49lfB4pAF4TEN5eproeYqWFMmZnz40cemAZPuZCRdVrx+DAsaWqKv4tvvy46ppNq7aMOU2L3B64tofytPiIYrZM0iAEaL9a4UiMo3Ur6ogUXquWKx3cdEJWZ9Fw3S7wtNEYnTEy3WouFYcLtVyd9sl6VGZYAyYqY0x36F7qNepYn8z0oYxhFfM5VaL30elM3/f9ASt1bEonRNlzW0ZczGIY/4iTogOu+9iv/1ZdMzlXnfAjYLFl1EBUV9hUTXJNJf0ilviSUZsejF8lgui0TGQciFd36/yB9NhN95Mj8bVUZdS6T+csGMZ+8mRURys81iJUtUNQwCp+Zxj7KzDIKj5qCqFN8bJWHc1MTGC+Qbym8fcR91lXlR3izx7edcol9PaWFRt7+Ci09aBpSz0TYVxA9CSmI+VgNNd4zGo6OlXbNAAY0PsITBrYY2460fiebSp7GiTGqwsPqORMFJSnmi4UsvSgKYhJHxhzzT9EgwTdlGxsan5lWaT+c58kqkh9p0j916ETJwLd03Tc8inCGAxoXe5RX0i7QFmFvuwA4QPKTV4qrJXOh0blfWLGX9C/SajQOXU11jEvUv8K9TsEbZSbxx4omfq154IcfBxgqcxKGXBo1K8+MOpuGf+xjkMUMVhunBSpv0LzJnsbevW20f0ovV5uOoEZGmwHI9UeGtPMncIG3bPnZMowegM2UdqVmp1D3l7tHOXOwEgyiDbMOJu6lqu6lnh/w5zzRs6HnmCmeOy6p1tOUBbohkPsOaEsXXermFtu/KmuJV4djCa8wZ0yAnJwl0pWaJeYXFRiRw//3tKaUMAeDbk+vdfTUmdFlhtf9j1v+s0gGB2U21kvhwaSXn+L4dustI0TTTn/+63pF076+ncd40dD4iaZID4ZdMuJCIyRgWO0lLFkxyAO46YbvU7DOTA/YcaRYIfe+NXY/WwYxA+raLnxOSB2Dkxk6JgFFEM6EwPxxkAQW3PUBOObwWPnAfg5BSCpGPbZ4Jj4TQaMkeHjaMPMQ9urIHoo18kXBr8NcctYmcZgjacHjNdJWIZaHwbQZg/MC/PAElBrftrAaLplvDD8/m3D738rDUZ21VokYpN30W0tIyCnCxdblvmpEzetYKQjMHJDH4iQZaGzA0cBeqzrSlJO3LRWl62qIYCbOX2sq0dx0DbWiH7UNG/2GR89ehw6h2VuLjrs+gddYHw0FcYi9W1JS4PB3r+sel46qXV7E81P7NTawgjsZDTVTffqMdS1146piUzfQQ1Gvr6pbroXJ51gpL0bkYEPp++m/2tJ178e+foqFfXd49OnC5mJx0cEbTfdV1zr/ulZIlqMfH2V6s1HbT1jzYMyaX/MfobX5EJssqTBBK+vwirWtozutSGrRUD+ygkja0+XTS5apLi2LnYM+dmzKtqK7gcX6Vx7z8+fVdFa9AV6w0jzSAwkCyi7ZGSjwUjaQN45zCwzlaG+p6UaGGmJbcXjMWutZO3CFD7tgILWDY/JLLWR2cRK1tEbTd1eWdN0z1JzBikwsrtm96yTZdy5a86u55M9R7JftNcKTJ0MXipktVMkckqs9BWYBl1x/DjpOPFqqBeXDiPFEVcwd88M67ByDNxMa5CDLKml8ZrHb1JaD92qerBTVem0AM6wpwNiOPRFpCcwBxKaJxjefGnmCmUe4Kk6gdm3kCtwuRmDOKabZiAZRC1hZCAZRK1gZCAZRK1gZCAZRK1grADJ0z56aTUmiKPBSECG4JUaHZSjXFkZ3VsNPs/YJGqg/gSzO26ZqoxATJSN95jzjC0sZILyXOcts6FUWyg4jdYoy7hnJW8wo/P/RtTacuPNKGMsu6MEu21jlVCiMpo1NArGCpR3MOf4WhO0sdx49EoqI2FkKzkdazgJGPdiyVvwyQVdlKPcq7LRaixNh5GAtCm5WTJnjQopScm1G8cpwFiB0iEryVAehlBK3xuGkaGcLISThvEAlIuZxZQ5yolrIyCcBYx7MeUSZQtiZ8IQZijPWAl1jAkZxj/B9AjKqVjLnRV81Gn5jmHsDuYCwDcDwdwB+NL1DD6G0RyLuUB5BnWg4S1GKM8l3JpuARnG7nAGBOVXlKs8KmPNDOXqyC8IHOTDME4fUJvg/FwB1Ovo5nOCbQfeb/o6nwt4UmBkscbQCT8CFsPIYjGMLIaRxWrQ/wcAZm8rhX8hdakAAAAASUVORK5CYII='
            var imgMap = 'data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAAA3CAYAAAC7DJKyAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAIGNIUk0AAG2YAABzjgAA+H8AAILqAABwogAA61oAADDkAAAPtwOsfnYAAAQ6SURBVHja5FrRcaMwEH3W+P/o4OIOcAWHKzCu4Ow/ZhjGcQWJK0huGGb4i11BSAX2VWCugnAVhA7uPrK+URQJSRiw8e2MP8Bi0Wrfvt2VGKBDiZN0DmAKwAPgcH+VAPYAXqIw2HQ1n0FHRrsAngC4BsNzAIsoDPLeG0/efqrx6KJtFAxaNtwDsDtBxSQKg31b82MtGu4AeJb8VQBYARhHYTAAMKbrQjL2mfS0IsMWHX8rkBoAZATn8niDYjuPk3RD4eFz4x3Sc98rzwNYKoislA2m+wsaV6Xnso2nWBe9vlIZLizASrjtkL7eeF5MaYUpcdG4QqPvoo13JCRnI4VG38XH/MVLW8aLpOWZpiwa52n09cp4AJgbPjs31He5FV6cpAeBqEqq2HJND7ATYjyPwmDct5j/ISGtXZykvsJwX2K4TE9vavtXADcKGL9w11NFOiuiMBj1sbwFgLWio3MNc/e67y3toWaR0lqsd5nnVx0/147niY19g6GbKAwK7rmdJHdXyT4Kgwn3/I1hqsxsdoBsY35pOImv1KHxsetZcgUvdzXf2yjsfcNxc74To2Zlb+H1vYC2ecPzszOe8rBNg3FXk7nFcQ82DZWqjjjV81PhuiRvqn7HWLXxvuj14/NV7yk182wk5n0Jqdkysi7210JvXwCYaBD5gPetLn6ei8Y8r4D81ja1aLy/r7lTu60LfVYT8sUJhwrrJqs5mkdRB/qsJuSzuoWFwvv7E/fnszqsz7qCvKZ6O7WaqwV91jHkeahuOOJsQp819IddQl4S43NVrFOR5AH4AmDLLxAVPo4QKpkt6w/PAPl/aSxO0gnfA3CGHYThv/FxK8sHcBcnKQhBW/rditCPwiCrC/vGIS8hv08bGLLKTbj+xpfStANU2kKfnQnyiJPUiZP0Pk7SN6ESLA3e4yq6SCvWZ+eAPOk+UP3vSOr34xZXTgbxJa8j8fC6DusPLSAPNHdyssTHvT0/TlKPC4OMcn8hCZUSwJjv8Y/jojDI4yQtBN1TFZKGlu3hE4CRLbwBeALxyGr86dHDZGCpI0wIR9f0rlJix8IY9hXt60qI2Tmxs0zHbZykzwDe8P6RgSsQXcYR3KRGkwSFc1xT6A8tIJ8J3vPoZaCU82HrCcB3YSJLwQMrAD+jMHhEc/KiQKwU+swQ8rkEOroKSkyJPn9eF4VB0bDhoA+YFqY7PMwQ8jPJhwWeZi6/JPnbQctCC7AygT4zgHwuqcIc8mwVKR3jegVgFIXBWMbeLS3AowR5U5OY93W5nVAw4xbCFReCKsEZzidbgXM+sf5AAnnx87FRVx5rUqgOeJWEb6aCvRbyfRGadyX0mS3keybbKvuYhuWznhufVbE+u0bIm0KfXTHktdBnVwx5LfTZtULeBPpDBeTdOEn/4HrFB7AYKAqb/0FmDBanmlcmUwbLA/1rgv7fAQCNrtyO8R9NrAAAAABJRU5ErkJggg==';
            var imgEur = 'data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAAA3CAYAAAC7DJKyAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAIGNIUk0AAG2YAABzjgAA+H8AAILqAABwogAA61oAADDkAAAPtwOsfnYAAAI4SURBVHja7JndTeNAEIA/oyvAJZgOchWQVHChAsxTrDgWUMEpFQAyPvmejg6OqyCmAkIFUII7yD1kokMR4HG0vmzWO9IqL5so387/TLBareirHNFj8fAe3sN7+N7IFxM/EgTBTt/Li3IAjIETIJLznizlPM6mk3ur4FsCh0AMXHwCuy0DOTFwmPB5UcbANRA6Y/ZKbf8SE3fL5xXgCzFbegMv4E8tfNuNVPdG41aCd6357y1M/RW4BaosTZZbDzgEvkm8MBooAxMt7Xaez4tyKFpvkhqYZ2lyow2as+nk1Hb4hWisCXz0VtMaMTl/MA7fQutf24Kbhu8i4J0p7sx3AT+EaB8rzP0GC8QofF6UmgruPkuT2gZ4Iz5/9+PnxscjRV5fivZVkqXJqCufN5Xnhy3uWlPm+klOX8WU2VdK86/F560Qo0VOXpRNP1ZtBzDXihzv84cgpvJ8Z9vOLE0Cb/be7D28dXl+Lp8nilz/ADy7mOdj1vP5pq7u3MU8/6C4M5Z5nFs+L3160wOEwKWrAe+P4s6FbGjd8fmN5EX5QvNQo2bP09uuUt2V4k4ILCRIqqTN3b1pXv7ob/Rb2Vfe39hEYkGbjU00m04CU/BdrqvOWY+sIsXdiPXenrwoD7/Ck8h/SothpVPlrZjwyNYH6Ly2lwc45t+oq1+NTZYmtYyvrmyygv/a1ckq+lgaob0/QmepTpm3x9IJDuSEHxRDS+ARqGbTSWUVvB9meHgP7+E9vIe3V/4OAEb1zybpemeYAAAAAElFTkSuQmCC';
            var imgH = 'data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAAA3CAYAAAC7DJKyAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAIGNIUk0AAG2YAABzjgAA+H8AAILqAABwogAA61oAADDkAAAPtwOsfnYAAAO7SURBVHja7FvLcdswEH3y+B52ELqC0BVEqsDswNIpmCAYxxVIqsDycDjDm+gKLFcgqQKzAzMdsAPnoPWEo5D4kACkRHpH86N9u8u3uwA8eH9/x6niAieMM/lTxaXqhsFgcDTGJmk2BrAEMBeczVT3q/RsoLzhSMjXiH+gADARnBVdyf8Tad9AHAAiAGu61gnOI5+k2ZAM/QIgbLltC6AEsBGclXvPhwDeFD9zLzhbHEXaJ2kWA7gBEAMIDB8vAawAPH44oiXy+8gFZ5ODkScjp5IImyIncSs1HbAQnN17JZ+kWUSGRY4++wWAOWWSygETwVnuRfCSNJsBeHVIHAB+0m8UlA0yLCkY7gQvSbOAohAfYYEoAVz/+P6tsh55Ir4+UuIgzXno3eG14NkgzStS7y2AUnC22XNiBGBI1SEyjO6jhGQMYGKVfJJmSzJWx7g5gJXgrGqo3WHNGRsAM/r7FMBYM7oFEVw2OHxk9Zun+v2sYdicyk7V0vSsa0ZetzQ2Ok6uAFztVYEKwEhwVlhT+5rAqYwZCc5mTcQJdUJBU5QFZ6XgbEROlCEA8EClbVInbmWqq2Gq6NaMflgHgrNZkmalwunjJM2eyAG5yfsvNKMeUq2VwSrxmgNyjQyYulzMUAnQvQvi9QwgUWzDkALkhPyt5NqmaaJygIni+p118qTOoULZnYMqguybjl1EXlZuinrT4gEyR4e6Pb0J+a+Sa08+e1aKvkxbjMjrlDrZC21E/TN9WtL+oSaoLxKbQtvkA0kkbCj8WKedTdLsY04vOmap1Xm+gF/c1popK+hDvvJMvrT9wkv4x6ajo/NjIj/s8lBthD04eq3h0aTnG5EtHbromabDA5CXKfov2+Rl3rzxyZoybegz8lvFLO0z9WNJ31GZttp90x4ac75NyOb2lXXBo+UoWZmZmg4UHVN+pmhfX1ypvWqAWbpMf3KuLOql4GzlhLxGbY6gsUnQg/i6x6jbCu2lazLiVfG+HLslrcoy8UCxpnDddMHa0jVNcCoPj7E7LRFZIB5rEAfUy1v9I18zSndHtnXjQkE6hN6GBaA4mGR9f56E7Q16Jy4qKkFPshpM74zx5zSHDv46ieGcvMG3KOsZtgA+UQYFMN/b39CODryT7+mAvlBG3LrgtQjglefxdK5L3IngSbqvqUPSJXZnbYwc7e00luHeuskKzqPOUdODkt9zwh05oaseFNidulj1aZgOevaW1uOHtAARSZxRUGpviXBpI216k/+fcf5ngzP5M/nTwu8BAIjawETDELlNAAAAAElFTkSuQmCC';
            var imgBenz = 'data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAAA3CAYAAAC7DJKyAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAIGNIUk0AAG2YAABzjgAA+H8AAILqAABwogAA61oAADDkAAAPtwOsfnYAAAIMSURBVHja7JvRbSIxEIa/jfJ+WwIlbDqACgIdkDdLliVSQbgKiOTzyfeUSwUkFQAdUAIl0AF5OF/kIBRlyRrY9czTYmTvfh6P/Xu8W+x2O75qRVEcLLfOl8AMGNOcbYB7o9VLnUp1eK4aetBFw+AAPWBunR+SyL4Nb50fAxXpbHax8MFDKS1Z+9cJH3oNbGvW6XNCSwV/Z7T6e0QIlWH+qE4Bf5XC48eAAxittsDPU3k+Bfz2zPXPCt+4hXDIEx54yBl+kkLstAUe4Mk638sVvgTmOcL/XwEq6/zskuF7CeqPUsR/Enjr/FHxGTZJswPiZ7knfp4uWd6OgbF1vrEGjVZT6/xtkL5lTjGfRP21DR6BF3iBzxPeOj//7m6vbfCv0fUQWFjnqyzgjVaPQe29y93QAcMshn04xBjwL0H6vuGxzk+ziHmj1Tp0wEtU/GCdn3cePnTA1mg12tP8wyzgY80PLGWdF/jzb2mbFjSL6OcKeAwHHN2H5+MZXh+4BW5yGfbLvUmtaiKV1Qp4o9XAaDUA7uIOyG3C28hsL/ACL/ACL/ACnwX8j+i67LK2PyRyJiF5WUZqb9NZzxutNnxMXfX3ZO59p4d9lLqKt7RrYFT3De3rNgZ7SF1NZbYXeIEXeIH/ohV1Psj59fvP4kBxj/RfWyw/+e85fsW9Dk/dpa5/Jid9dt/VsY2+DQCwZpAG1MejnwAAAABJRU5ErkJggg==';
            var co2 = 'data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAAA3CAYAAAC7DJKyAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAIGNIUk0AAG2YAABzjgAA+H8AAILqAABwogAA61oAADDkAAAPtwOsfnYAAALaSURBVHja7JpdrppAFMf/GN/rEtxB6QqurqC6gspTSSlRV9DLCq4Jl4Y+QVcgXUHpCmQHpTuYHdiHHltzM4wMM4NU5yTExI+BH+d/vgad4/GIe7UR7tgsvIW38Bbewt+DjZs+cBxH64niJJ0CcOl4DWACYEpHWyvptQbwi17rMPDLph+I+hin6UNV+DhJJwAWAN4CmBGsSavo+AGgDAO/7h0+TtIZgDWBX9MqANHHD+8Ladl3gF4AeJKUsUlz6XrMwVMsZyTtIVkEYCf6gpLsydtZD/EsK3cvDPzqUsyPFDy+ArAfGHgBYH4C71zqWoBnA5N5Hga+J/MDadmT1PcGLr58Wb8pjzxQ8hIprAgDfylb58eSHp9q8jgj2FNNrjj9QdayclQAvC4XISv7J4UYZwByAN94HRnd2DWAlcQ5GCU3ZhSempdFxyT0NQz8QqCmTwQtXc7aJjdVz7+TTUB0cbUBaFC47IwMNpw+fTUQ6PMmBsbhW3ZvJYBtkwzpBm4orlV7g1w0yemGf7iQdCKRBA30/ZGORdrCu4Iys7wgcd19f950PlPwPJnuwsDfCry9odjW3f5Guhbq6nkvDPxcENt7Q1OeNq93GWwYyTwXxPZPg+NtpHOxsST4XJDNH0nmJgeX+hrw9fmMzJF5BvPbVpHuBdvCLwXg3wXVQBu4bq8r7eTESeoSuOnNjBrAm67Di/adnB7BT6pjJhYeDRzcU5natMqeOrZDj+C56iJaZH/WvJgGZ7rAdco+42R1BsFDgS4zOiW3vAdltZ7nHzl1/G/TQ6qY0fQ3kyx97Gy3p0SPdjHmKcEdZGKSboaLf7uur85uSI0/O7QMQGUaWOlBZZykB44nG7eKh2adEx7J3eW1urgBa/T88+cvE5rQXmb3ed+xeQ3Pbzjgxf8ErlLq1pz3trghk6nzRiarocJ7lNxOtXiHGzPH/uvawlt4C2/hLbyFt/C3aL8HADzRPx1nljvPAAAAAElFTkSuQmCC';
            
           var segment = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAV4AAAAuCAYAAABgfnFvAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4FJREFUeNrs3UFy0lAcx/GQwYWrMq51YO+i3AB6AjlCdMa1dKerhpVLcasL0xvgDeIN4AZ0HLcOXaqdwfen/9AYCASTN8ML389MhpYH7z2S9td/A0kay+XSw3FqNh8Fd3d/ItYEUC8+q+CodVgFQP00qHiPuuLtmIp3zpoACF4AQAnsagAAghcACF4AAMELAAQvAIDgBQCCFwBA8AIAwQsABC8AgOAFAIIXAEDwAgDBCwAgeAGA4AUAghcAQPACgOuaRR707PO7lrnppu5afH/9fsrqA3CKTCZ2zM3ALEk2Lswy1yU2+Tjf9fzca65p2A7NEpilnfP8r2aJzCATNgWAEwjcQHPxfM9DJRvHJhvjwsFrOg+187OC85lJQFMFA6hp4EohKgVm78CnfjRLaLJxkRu8JTpPvDQDRGwmADUKXdmVEB9QiG4rTPvp8PUzoRuXCF3xRUtxAKhLpVsmdMW59uFtBK+Uw97+/RZFjPUvBAC4blIydNfhq7twH4LX3NE3N28qmqhMcsz2AuB4tdsvuQcg60o/DbGueMOK59zTSQOAq0ILfQ5XwasJ3LM1AAA4WO3aysVBUvEOLM2diheAq2zlV1veA5Pg7Vga4CzZnwEAjrGZXS05ZNjaJxBe/X4yuBwO5aAKWVr6YhYfxuOpuT/5izLV+6U9Oeyuu6Vtru2rNtNHbPro7mhL+p+nVuS2sY9hXtm2ffNatzk4r3/aCswr3ebavFz42eN3YrNt6j1/bDXVmzY7//XjZ6AvZqgvKNAVId+H+rDksOSutkU5bZG2h6l/BXa1JV9Heps39jHMK9u2b17pNtfmlW3rHzBn1+blws8evxObbdbfn2o8/fRWPqf2wlL/F3nHKgPAsdLP3F7ZykVfU94KQheAo2xm19S3OMCMbQfARVo03trIRTlng68D3FgYIGLzAXCYjdPdro7q9S2F5C3BC8BxYcX93SRh7mtZHVa8a2Dj/JMA4BK9isSowi6HSS6mz04WeNXs07g2nXOSHAB1CF8pSq8r6GqUvlKPnxpAPt3QLxm+EroBmwtAjcI3KBm+Iw1wbyN4M+E7+8/OCV0AdQ3fywMLU9mne5ENXbHrYpcykDyhva/K9e4v6sb11gDUml6RItAl78IR37z7iwBHef3kBm9qoK5WwR3v4TLGErJzs0x4Ew3ACYdwN1MZx0We+1eAAQBPhCddr/93jwAAAABJRU5ErkJggg==';
             
            var separator = 'data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAZABkAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAAdAvIDAREAAhEBAxEB/8QAFwABAQEBAAAAAAAAAAAAAAAAAAQDCP/EABQBAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhADEAAAAeqQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACYmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABYbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH//EABoQAAMBAAMAAAAAAAAAAAAAAAAEFQMCYHD/2gAIAQEAAQUC8/2WxYJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTJiZMTMsc8OPYf/xAAUEQEAAAAAAAAAAAAAAAAAAACA/9oACAEDAQE/AT5//8QAFBEBAAAAAAAAAAAAAAAAAAAAgP/aAAgBAgEBPwE+f//EACAQAAIDAAIBBQAAAAAAAAAAAAEDADSTAhESEzFRYHD/2gAIAQEABj8C/Px6quDevbz49yojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMSojMTxXw4r4/HEdfYv/xAAdEAADAQABBQAAAAAAAAAAAAAAwfARcQEhYHCB/9oACAEBAAE/IfX/AAvozxpAogUQKIFECiBRAogUQKIFECiBRAogUQKIFECiBRAogUQKIFECiBRAogUQKIFECiBRAogUQKIFECiBRAogUQKIFECiBRAogUQKIFECiBRAogUQKIFECiBRAogUQKIFECiBRAogUQKIFECiBRAogUQKIFECiBRAogUQKIFECiBRAogUQKIFECiBRAogUQKIFECiBR325ufo355F/9oADAMBAAIAAwAAABCSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSST//EABQRAQAAAAAAAAAAAAAAAAAAAID/2gAIAQMBAT8QPn//xAAUEQEAAAAAAAAAAAAAAAAAAACA/9oACAECAQE/ED5//8QAHBABAQACAgMAAAAAAAAAAAAAAREhQTFgUWFw/9oACAEBAAE/EPn+9A37LgZYWeDrcSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIi57TqblgC4M+uxf/9k=';
            var separator_light = 'data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAZABkAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAAZAuwDAREAAhEBAxEB/8QAFwABAQEBAAAAAAAAAAAAAAAAAAIBCP/EABQBAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhADEAAAAeqQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACSQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/EABkQAQEBAAMAAAAAAAAAAAAAAAASARFgcP/aAAgBAQABBQLr+7wpSlKUrVKUpWqUpStUpSlapSlK1SlKVqlKUpSlKVqlKUrVKUpSlKUrVKUpSlKUrVKUpWqUpStUpSlapSlK1Slef//EABQRAQAAAAAAAAAAAAAAAAAAAID/2gAIAQMBAT8BPH//xAAUEQEAAAAAAAAAAAAAAAAAAACA/9oACAECAQE/ATx//8QAGhAAAwADAQAAAAAAAAAAAAAAAAEyETFgcP/aAAgBAQAGPwLn9ZJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZLJZL8//AP/EAB4QAAMBAQEAAgMAAAAAAAAAAAAR0fEB8CFgYXBx/9oACAEBAAE/Ifr/AMD5fwPnR86PnR86PnR86evlHzo+dHzo+dPXyj50fOj50fOnr5R86PnR86PnT18o+dHzo+dHzp6+UfOj50fOj509fKPnR86PnR86PnR86PnR86PnT18o+dHzo+dHzp6+UfOj50fOj50fOj50fOj50fOnr5R86PnR86PnR86PnR86PnR86evlHzo+dHzo+dPXyj50fOj50fOnr5R86PnR86PnT18o+dHzo+dHzp6+UfOj50fOnOvnOpfjv6+//9oADAMBAAIAAwAAABCSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSQASACQASACQASACQASACQASACQASACQASACQASACQASACSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSST/xAAUEQEAAAAAAAAAAAAAAAAAAACA/9oACAEDAQE/EDx//8QAFBEBAAAAAAAAAAAAAAAAAAAAgP/aAAgBAgEBPxA8f//EACQQAAIBAwQDAQADAAAAAAAAAAERIQAxcWGhwdFBgZGxYHDw/9oACAEBAAE/EP4+YkaKMI1LP+VA4EDV8uG4qPSfy1+M6TRGCXXgXwOWxohJDE5F0rtgUCkBDN/CV2x8oHAiavbfhuKi0n8tfjOk0RgzXgXwOWxohJDE5F0rtgUCkBD8+Ertg+qBwImr234biotJ/LX4zpNEYM14F8DlsaISQxORdK7YFApAQ/PhK7YPqgcCJq9t+G4qLSfy1+M6TRGDNeBfA5bGiEkMTkXSu2BQKQEPz4Su2D6oHAiavbfhuKi0n8tfjOk0RgzXgXwOWxohJDE5F0rtgUCkBD8+Ertg+qBwImr234biotJ/LX4zpNEYM14F8DlsaISQxORdK7YFApAQ/PhK7YPqgcCJq9t+G4qLSfy1+M6TRGDNeBfA5bGiEkMTkXSu2BQKQEM38JXbB9UDgQNXsvw3FR6T+WvxnSaIwZk7XQOWxohJDE5F0rtgUCkBDN/CV2wfVA4ETV8+G4qLSfy1+M6TRGDMnAvty2NEJIYnIuldsD1QKQEM38JXbB9UDgRNXz4biotJ/LX4zpNEYMydr7ctjRCSGJyLpXbAoFICH58JXbBoHAiag2X4bio9J/LX4zpNEYMycC+3LY0Qkhici6V2wPVApAQzc2Su2D6oHAiag234biotJ/LX4zpNEYMydseWxohJDEb+UrtgeqBSAhqTZK7YPqgcCK0Gy/DcVHpP5a/GdJojBmTgXW5bGiEkMXnyldsCgUgIak2Su2D6oHAiag234biotJ/LX4zpNEYMydseWxohJDEb+UrtgeqBSAhqTZK7YPqgcCJqDbfhuKi0n8tfjOk0RgzJ2x5bGiEkMRv5Su2B6oFICGpNkrtg0DgRNXsg8NxUWk/lr8Z0miMGZO19uWxohJDF58pXbA9UCkBDUmyV2waBwIrQbYPDcVFpP5a/GdJojBmTtfblsaISQxefKV2wPVApAQ1JsldsGgcCK0G2Dw3FRaT+WvxnSaIwZk7X25bGiEkMXnyldsD1QKQENSbJXbBoxwjIfkGhX9ff/9k=';
            var separator_light_vertical = 'data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAZABkAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCACBABQDAREAAhEBAxEB/8QAFwABAQEBAAAAAAAAAAAAAAAAAwACCP/EABQBAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhADEAAAAeqSIgxCDEIMQgxCMGyDEIMQgxCDEIMQgxCDEIMQgxCIiIj/xAAXEAEBAQEAAAAAAAAAAAAAAAACECAw/9oACAEBAAEFAsGmmmmmiimmiiinh//EABQRAQAAAAAAAAAAAAAAAAAAAFD/2gAIAQMBAT8BT//EABQRAQAAAAAAAAAAAAAAAAAAAFD/2gAIAQIBAT8BT//EABgQAAMBAQAAAAAAAAAAAAAAAAEQcSAw/9oACAEBAAY/AsGs1ms1ms1ms1ms1ms1ms3h/8QAGRAAAgMBAAAAAAAAAAAAAAAAELEBIDCh/9oACAEBAAE/IaR7Wz2m2aLWOzRaLRew/9oADAMBAAIAAwAAABCCSSSSSSQSSSSSSSSSSCQSSQSSf//EABQRAQAAAAAAAAAAAAAAAAAAAFD/2gAIAQMBAT8QT//EABQRAQAAAAAAAAAAAAAAAAAAAFD/2gAIAQIBAT8QT//EACEQAAICAgEEAwAAAAAAAAAAAAEREDEhwWEgQVGRcaGx/9oACAEBAAE/EOhVUzUtz8d6Wt5kwcfcD+Sbs0Xcle5awIyt71iRVEzNJ/cpVcS1vMkDiXzA/km7tH3JXuRIyBGVvesSCqmak/ufh4lx43mSBxL5gfHgSbuSriSvcgANAB5x0//Z'
            
            var details = [];   
            
            var imn = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAACXCAYAAAAYn8l5AAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAADExJREFUeNrsnTt248YSQFs4zEmndiC8FYheAaEViF7BQNnLhs7saKDI4VDRe9lwVmBoBQOuwNQKTAaOLa1gjKIKmhaMP/pXjapz+nB+4gCNi/p1dfXF169fxVTlh///GuUfYWmArHp+1XM+Dvhr+HzCz+Nf//3tMNX5vZgKXAgSjCWOS4P//SPCBiObCnDewpXDBACtEaiVg5f4AKD5DJtXcOVArREoGHNCl37KR5qPnU+gkYcLNdSGIFBtoG1z0I4Ml3mgFvlHjFBdCn9lj9psx3DphwqiucQjLdUnGt2iNntiuPRA9U5MWwCyHRWT6TRcDBVtTeYkXOhTgT/1gTlqhywHLGG4uoEV41s5Z3Z6RZhxDlnGcNWnFACqFbMyWB4QMidMZeAIWKDW/2CwRstNPo75fG4mr7lQW0H0c8Vc+KfFAotggW+VMVjatVg0Gc2FkeCW0wtG5c5GRGkULsxbpaytpmEmjcGF/lXGKQar8oiAGam8CAyBFWM0yGDZFbAYGb7o9OFCsD7xc3VG5ghYTNos5jewY8fdabnVWc4TMFiTlk86NVjAYDFgugALGCwWXYAFisGKGSwGTLlDz1GhFwL1YZGqPJgSuDBv8gc/G28AW6ooow4UgBWKl8w7ix8CebAU14DtwYUXkArOvPsmkMnf2dZcW8GL0L7Kzdiiw8E+Fzvwk5Efhzr4g+DiCodJyQkd/N6lOkPN4o7BmoxcDvW/esOFmynYz9KXBnDV/1prhQvNIW9UVQcSVIf+nI/r3OxciJfmKq7Krm96YjYgOmQZJ5/zkeYwpRV/t3b4uuf4/Du/AJ0deo4OlUCV1GW+MRn9J4H7uO66s3vWEawFay09UEmSELkf4KBTmXRXn2vD0eEgnwre8rgNLNRaVKpJrrpWT7SaRULqWqVAR7+i3TdInRmI8BPe5AV+wksIu2yirrkhjMApBUqdcl9dzGLiOUgnhAcc7EPPaoCs6mUcUFGwJjZnl2jNksGay2OtBZplh1Hb0eaFoD/7N1GzHzZprzbN5ZvWAud661g77iXRuZy3aa+ZJ06mqoitag5C9K3KJvBAqfmtJmmEq9YserLRojNUWF4SlRzzLv7aEcE71CRGu0AMmpTqklrt3sdZgx+wJgwVRHtxT031caBjC6NoWncx8Hoj9AFvCM51ImoWtuvyXLGgmdcCJ/PnHKpogKN+UgD0IAHzmg94mX9ScB3GI8e6HmCzBltKMaWwHuGsZ+gGFKkJgLM43k6Wwg8rzGeI2isdewNgVvMHlRHUYnGFT/pvn4voTp49gjXYwUbnXQxJTYAbodq5J5hY/a48B1VwUXPkP8MSi4+hGLFiAXBHtm0+15rBckMwCrslZBrrHXqsNpwzWAzYALkqXIs6zUVFaz1OASyCgK2pwwXRXCQmJgjYZ0pwvTr0RKJEpY0yiDr5rmfzX6PGwJLWAkju4EJwdH0jkymDJTnOzw5f36tVmVX9oSntg9oSstMxOoNNZ//sy6GuIU0BL90S5weuserYY8izHQXWhelc0MZ5g3lwNQcG85WWzaKp0w7OpzlIZvhZfKs8+L3h5340pbUUHHcMW8a2Oo+oy6/xKNw83xuCreWrWTR8Pkwx4cUeuDlqsrQl7XAwANUCtQIUSL4Tw9MysHTzBZZyyuG5QkkcTkksZJ/LJFxhW5RhYyJRkwLA7xV+LZj5g45+oxg9urrIHclwmayGjHFyMsmRDxve8L3uUmTphA8dZga0n66Oya5qr6UtuFbF8SCYCL0XL1UAddprqxksuJa29bsTXieUxFzn4z8Y5V7jn93b0CKovVyMHCM5WjTtGEJpyRLrmDb4kKvewueh1Z1dfSzR3HITgNk0XEPxs/D3G/RdqxriPQsFJTkNDzLBT1eW7s5WKLB02CPA/OaAIzwP8LakAVLN19F0UDuY7GUfuMHUY6R0V/5/dKUnINCBQsN8LFCj3qIm3VuE66ysLr7/3y+xsFfWAf5UVOMDxag1tESJLdvmKq9rgB/3SXTYgqXZ7Ec4l6bLqK4BLtAYthJyoB2OqEY3Jh9AQ92aMhhw08fShUV2C7VhZ7h2wk5x4COCdSM91ERXFh4nV45I6/pfPCo0x3B/qStb0AwXgt7NRHXeSbs5FC+16TelkP0jvu2xyuw2msCub+2VULcwfOfY3sbUpCIJDN9c0U0vFPUbEMAZ/KI4L2QjH/Qs3Gs7ZRL0JWiuleEb7LqrRYlGtbhzfDvxHdkL05rLRh6GtZYlmU3gHkPxNuezaPCpiu35o30b7iMhFt7DVc5XYd7nS80/32Eyl2W8XAU8ByxsFsc79nJryVoTOnY5TGeBIMPlJljFAnVbQPFuZGT5IPjsyVdx2SxGCr/LVDdq1yPEJcOlcCJQa5no2rN32SQanAcScM3lkhwCWitxGKwQzbXRuj3XfS4wjQcFcOmWk0taq9TLFSp8bWyi3VOAa5Qfg0V0VQ8AYKhb+qrt8+mouVtLMHXt52rELLq8e/dG49asJni2KkwypDUUmfYmgUoHqPj4gC+LM12KAgVmR7fEOr60ZWsWPKBsDBhY1QEPfqFrYjAnt3L0uT1RyNBvLH13Adim5wOHRGyhTeYUXzxFcoBKVPBp3jsOmDYfqGN15gl9v8rjXNB0F85zuaToWoezT+DonHMlKoXVe/CBdFUabNAJboqoIISHPvVQKftcciVsOdCJ488so+BzFSZKi3lEYEHrPPa4lpU0jIOFvqDrTZHPPtdR0JAPuiJH3JwLD+xe8VefNM2v82kS2BIYEGumlmqeENCOsEV/rwAq2JwRqu5zgTvTXT8n6GwFzv25iB1sdF+0ADBgemI0mVcdgcrQ6U81XhOFAygeYBd4kaGnBNd7yK7r7CFRqHXZz5PqvEIc8jHF2o/Hk9YHKch5XmS4KJ2asYOHbdKk21w7xCWeVNA5I+A8V4H8G0IyOoNORaRCR0rnMR5efS68ia8E597r1uFEwXrbExVlT3D+Cw229hCskCBYb6xgYCrM1wzY7zXN46iCFRELslrhyog/kw8++GD4knwRNE/qFXIUH5RC7xPxZxMRB8vlwwu6yIP8m3LJTUocrgXx66euedMmuHaCxaasvIXLA9O4ZD7tmcTyKkXQRh+bRZYhWqsOri3Pk7X0A1Wp7LM/qwglj/mN7ona/9WIhxviiwWuwRFHpwVpTIHAzz9NtBFJZZVw3b7FnQfOZV8BOG5EqQY+B6f4JdQoPZX8u6pc1MUE4aq0dpW7fxw+U8amv3Yl9JY3UzWL+7q13aAvjQR8l7XNh6txE6+rsqv7iza4KGovq3CJ4ekQiovvp6Ytf7VwoYNGMS0RFSeV9tA2sVC3SJwMjBQpLlI33utrPVeDiv+T4E1DtLvuGOkBWKrPxIEzjTY9Is1M0FuoBq0VDoYLbx7opLiYWuReji2mUGdU/CDq94UW/VmpRuWtu+C7wLXABzQXLCwdtVabQy/7Xpy1Z5El7vKPWjWXpMFAe13yvE5eOh902qeF0obnlUX0aNvUGS4sX33guZ203PVpT9C3+dtGTG9ZiOWbE5/0+YFecCG1Cc8zm0MdmkvgGdRsHqcl90NKiYIRFLN5nIY8Du0qNAguzH2ted69l2cxoqnv4G7OqCbveP69ls2YPhydk6h10nISBQttP2tUblNFH3owj4/8LLySvYrujaPhQv+LHXyPHHhV/vRosyiZRyr9OlmaHXhl/c6UHc+CF3TLz4fBUg4XArZjwBgsLXAxYAyWVrgYMAZLK1wMGIOlFa4SYJymmBhYSlMRTUJ4+5SPAnms2ER7dSMnxeKNRIIz+bZlLwz27TeiuSQNBtvUwFTe8HM2LkYO5LIGlwRZImh3LabmX210HePsHFwIWIRajLereeBfOQUXm0n/zKBTcEmQrREyjibHywm1VWb7QgIXZgP3RIbipTsMy3CByuClK31ZndBcFb4Y7DC6YlZ6pRhi1edpeweXBFmMkLGpbDaBG91HMnsHl+Twb3AwZG+hSmykF7yBiyGjCRUpuEqQQWSZiGnlx8Cn2lGBiiRcFY4/aDJfc2RF280t1TO8ycJV0mYxDh8izAeEKu3SsJfhMgdaiGaTGmgFUJlr6QSGq9k/i3C45KPBml9WDOoaanJw1cAGkC3xMzQEHER4BxwA08FXmCYLV0tgUPSEF+LtMS1tAMonmT2Jbz3nAaInqo64KvlHgAEAyyuKTmCedK4AAAAASUVORK5CYII=";
            
            var circl = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAYAAAAe2bNZAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAZhJREFUeNrsmL1OwzAQx89lR30AhvYJSHcG8gBIeYO2C2vVjS3txla6ssAbkKoPQAcGtponIAM7hifgTjpXpg01TprYQ//SyZLl2L/c+esswFFn9zdtLBK0c7SIqy+5lGiKyze07OP6Vv23b+EAQQOOGMRFGdocoVaVYRCC/n5m/H1ZEcwYoeRfDVoWkAkW6wOA6FCuuU83z+BHD1gMoB5R6Ibb80l4AAFjsscmUMsTCPBKnJkVJ1sgBDGB5hSdXl18fS9fXn+FCUE6PFnb0KwoTD0MV26GKfUAAjxmuvEMe+Ud/KqrPTMA/xppmH4AMIkIJESbfSaCQBQcDBxhjjCOMCokGBkKjD6bPj0dkoVzJgvAMZmGWQQAszAvV3Q+dTyB5Hi56ppLe+zRK9Od7AC981QiY6yqFXolLtr0hg3vO4rH3N2BOYeJGwJSnDfltiSOrhXPNe49GkRazyZuRB7K61g5RSB7D0pu3EO7OyDII+dIsvT7DL/NpBVeI+g5ZGp7oxEuPfLlPeFswnZdlQwxNyfpPv0IMADvbnuDM89EaQAAAABJRU5ErkJggg==";
            
            var train = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKEAAAA/CAYAAACIPaiFAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA6BJREFUeNrsne1xGjEQQEWG/3YqMPnPjEkFOVdgOjB0gCuwO4AOuFRgpwJIBYGB/7E7wBU4u0ZkMOZA97kG3pvReMYcukG800p70lF7fX11K+bzeSR/Rg4seJEykTLW0mw2x6fywWtI+KmlfJQyECEnSAjWPEu5VylFyMWxfbgvfL8HwYWUoZQn6SjupTSQEKw4k3In5a+IGB+LjEh4uNysyXh+CmPCqZQoy3hE6mz5Gd9ZhfUqP6XeTpZG0S/Wf8lZ+C3njTKeVz+TlsiXixSTmIGfxCyOVcKrPCkDqbctfx62vPQ1T6PtqHcq9bbyNIzUrTPSyyolTJCyJ6W942LblLEn548Jxx/ZKloBV+0i5f+LqLsyNDWjvbkUDbddFTxgzDjUC8h3KEgIhQoZ+x72uw419hyuPfjoUMaLSHh4Mk78WPdbgIw6rtW0Tg8JoQwZnwJl1BDdFxHHfoyJhFCqjL92HPpDyh9NdiMhlCmjzqKv3DL1lcSdn7i0kBDKknHs01O3bpmySZq4fJpeEQmPV0ZNXjf2hOhVr9hAQihLxIUP0d09veLEcgaNhKchY+yWtwGne2bQjxZ5RSQ8HREne0RUrt0yrxghIZQWngNE1F5xVOWkJVTCvF301nRAAWmCpPc38oQV/97GEYvYDjj0zie4Sw/PoatodFDbcdlu6qso/YTXyqrX+as962B74LKtoMl73ioJ3cah31FU5j4X9phAKN2ylogxJoRQhn6xLxKCKTc+uX2OhGCJjpWfirz3jISQBU3j6My5g4RgLeKwiNt9SAh56eedsCAhFDVhybyfBQmhMBH9ODG1iPXA4/QuwIJ2PlrOXfY7RJszZxUx1QMNQiXsndLz8k6Ngu+UpRaRcAxlcJkmNKd5Fg3hmHC8a7jmttQR9KyhegqzAZLQzVU9v1flcc2XoNBMOIYieFufqNtO3cdFs3tDMxJCEVysduytrd5+2RQRCaGS3nBNxM3V25dJd1ZY1ApVc+v3RNMTghn9zd18SAgWvNvfjIRggS4Di5EQrLlehWUkBEtiJARrNL/YRkKwpoOEYE2EhGA+U0ZCsOYZCcGaARKCaS8oJUZCsET3Li2QEKzQnwLWVdgkq8GE6fpvUSMhVN4DuuXK6//UaROoqvdzy1+mjzdfeLeyGsACwjGY8xaO5/O5rvlv0RxQMZqimdRms5k+5LBPe4DRODFSCXV73hntAUbc6piwRzuAVU+o2z//CTAALF+Y6cXiYxsAAAAASUVORK5CYII=";
            
            var car = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ0AAABVCAYAAABTneYPAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABqNJREFUeNrsndtx6kgQhscq3u0MDvvuKsjgyBGsMjCOwHIEB2egE4EhgoUIDBEcKDsAyABH4J32trwYG6yeqy7/X6XSCxIazaee7pmembPX11dVZz0/P4/0iY6fqtma62N2eXk5UR3XWV2h07ANqZL08aNl73xLH5GGb9FV6JIaA7doIXCKy/Soy1jA0tULupU+DTrw/tf6SLXV28HSxQVu3BHgFJdzoct8AUsXt1n908EW54Ut3gqWLixwFxw4dFHnbPGGgC6sipYGDgCvjtBxX9y1gkrwRvDp4MfF0E1bO5KjQsd+3MqgWZ2y/7ez7WTVz0DXl6MdV646bfV9+/pEB31UI8OIvJXg9SL//0QIXGOiPP2MG32igyAuNISpPo+VbDjvQV+n2gZeNJ+O++P+7kq3AllQfRB498JLH9o2epFEAo5e/i/hZXkb+rF0GehjuxFedqvf2QTQ2fk60v64aZuaGC7LFVvvqrpuC3gxLN2MuwaqisYn87Y50xywpF0ELyh0/MIkURxVyKitA+LsLpiAN2vyeG0SEDjqNpB2AOdtH480BI8CsMYmCiSBgKO+KmkENu1Kli2D12dXoqoam6GSBACOXsoEfty34O3Y4rUevBCWroAfB/CCQQc/Lih4m6ZkqCQegTPx4+aYLfUBvLngssakRvm0dFI/7m2WlILewdNHpv5LbmgVeF6g47FCaVZF1kU/rgJ8o7aBl3gAjpqFW+Fld1324zyCl7YeOgs/rgBaXsB7rGMWcs8CsAt2dod8Nln24QV+nAw8yq8T9gjULievJwSNAMv4cDE3NYcfZwTegmBqKni9CqClbI0yYTT6rZVD94gxeBNOEfvVRPB6J5rOnGHzNS0QgYMdeGM2CNL0d2qtxjFbmN4R2HLHVu0rDen/0LxaaWLgS1PPAmUib/nDp2MRchWp99lgkZbmeuEXFxO8fWtO0eGmIcBd8LO7NA7rAxBX3qDbW5rrHMYDOtCyhJBBtDYQZ09PTxf8dQM4qKo1XNhASNCNlXxmFgQdQjip2hwTdBvV7YVrILc++owBXJyC7hXvCvIgio4LBnAH6KDQ1o/gK0r4ErwTyLPOOWZYlelWgA4KJYob3vL8AB0U2urNAB0U3OIBOii4AB0E6CBAB0GADgJ0EAToIEAHQYAOAnQQoIMgQAe1ULH3BqP8+jKr9CeqI5oo0bKc39BXnqcvxICOprRRFumnXXM4yY8memPv1zCglenkm4N6oBmCtIzI2AeAIdPVqZBZlZnke1s5DcCGF9GyspUWE9d1kTN8502DjprRVDpHknfYgdVzqzvpeoCuJ+OHCCReTIAj8UKAa3DiTFOTBSh5PmvWpOjVdk+ITMm2MIK+1pY/YiOxW3TfBOjWXwUMwsKSkzsBM9YaO7hH4cIA+IauqNl9OhupulgMkVusmQvoph4Lu3BxE7Z2W7ATtx5YttAtEza7Xnymw/4fS23AjrFWNaqHPGEwctQLJIhkTXVP1yd8o4nnZhZqiSx24lnSOskfAgkOp+eOH7Dv8HZ9VLmxhpHrgfpas2PRK4G3dPiAqUN4sYZe5HpgZQbAfRgcSA5DYn2kDpvavGb36arOXWzXtJcIYAzcV5Zuv6m9c1DYgX7QzLKgZOVuwY21xg52v5Zs9TBXR4Y/35f0P1Hh4yPteNX8N+qOGZp2n+hnoGhpIPiyurYvhcT1mJoOhfFGKY8Vf05Dbkd9v5PQffMQG0FhxVkm/FXSSIQky+Qvx32DTYgmqSX5R3CJGDyDLJPf+j+OukQ2w2CSoalyj/lMWEgJcPOuAceuEI0QSEZrrvX7nVVtajmf7o+SpTWdZMPG0pnuP7Hkh/q0BwFDmSmzHLqrkFsN1czakeV6EF5W7lb0aSl+y8zhby2pMXT8cPRQNntQuJojMef97jsrobtzzBhIfUQjF8cqy4R7mG2SLAcMmw1wL+hSeZNtl0hZDzbA3VdxcRJHhY2ZZJl30Zf7wgCQa/E74iOsy2Eu79CxPxDL0kyxUfGHushVnPR+CmTSqj9OHBWWKv4mAnAjoPZJaWDwyll+u6DQHYAXoqm9B3BH62HH4C0D/B3BPZSmO1lFr0eiqCGH4gNPX9XIdt5FhyJa296Fkz0GynDSlXPo9grsepIuOcljbL9uZAQK5W7Zji0Hb8YfvjfouMBlJ2NuaPm26v8OzA0QsqqLlI2AKXzUlBYuAjev0B0Uus8A0pfX5/OhFSQ/hCwZhf/e9pDvOHx99vnKuvhxwpWh9z9zXRf/CjAAIav0pfaauYsAAAAASUVORK5CYII=";
            
            var tram = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIoAAABVCAYAAACW73yeAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABPZJREFUeNrsnU1OGzEUxyeIJRIs21Vm0e4iMewrEU5QOAHhBM0NCCcgnKDJDegJmkjddyLYFalhVZYgdU/fi95IjmuP30w+mGT+f8kaMdjPXz/bzzNJ3Hh9fY3qpPv7+4QuIwr7gahHrVYrjaCZdmpY574CkiweJNqtwQwS06VthKYy6QB4bDEoBcF4kWWIw9wyQ8vOCHhsESgLgDGCD7LloBAcpwLFqWIp+QYwaggKQdKly3VOlLEBBpaPGu964sDS8mwEaElqbOJzFJlVeNlJAlvdOWcVM0zNQLGgSSxnNvSMBEtTHUEJgJMonN2xNetgyaoDKAtun1kTawsNcOoASgAcnnEOAU5NQREYOsroBwLNoTL+o0AzXXE1pgTloCptuq3vehiUyxXZ5qXrfA11YN+pMqDU8e0xBFCglfooBdf0TVl6zje8Do8VWXpmz5sad3d32k98QfXVBYPCkByjLaAcvewAEkihfTizkEq7gX28xmlsKtNkcccF7LH4yeg6n4byA7jDN8iXZb4N5zffqaKc7PROFfaiQFzb7vyuh3wU89EsF65HXq7qE+i0W+pFxoMtStcIxdXEsW6frPMtL5WhTZfv685X8jb9xTHl31aU84ri9RT2ory4VjqGpWv2hT2jnOLVOyTvs3oEDF9nnyY0fZQJIIEsYPqyysyBgtfpkEupDQoEeQVQIIACARQIoEAABQIoEECBIIACARQIoEAABQIoEECBAAoEARQIoEAABQIoEECBAAq07TK/1xPLF7CKqL2GMnbky07rUvxG+Zp5r0rtEn0c26DwVzkvKwjzec3yXaWOo5I/SoClBwIoEECBAAoEUCCAAgEUCAIoEECBAAoEUCCAAgEUCKBAEECBAAq0TNk/cQ5BmFEggAIBFAigQAAF2j5QXtAMkAaULpoBCmiYHZDdFmAOrAj2l4VC59z50rHsswRdZ9eVta9Nt2656lj2XD9XujgqdwajK52vXINWqzWYgeITAWT/k08JS0Kt40j333mDxpl4czDlnaMn6biSv4umewt56hg810+bznMG4wV3bMC+K11uuYo6s4dyMGFeIVwgTRz3ph7SQ3IBsUmnl2mAPl3AfqKIEyv7Qw2K6+jZkE/jojJ1zDBThyPdJNA6Afsdjf2KyNX4x57BlA20gwJ1HLnaJ28wy4x8XrQNQ6DcOu5d+jpT7n9WVshnv+9rSLo/8Pg/t1WkRAaDazYdSIe5IOE67jt8sJHD/sgx2DjtyAWL3HO11SPZygUl5KMcyKjYd3nCUqnM+ep6OpELERdYizPdGJWKZZQ5neQq+ifW4Pnq+Bd3cN8AIJbZuOlqa6pjp4C/kTmnPWNWS+RvV18G/ZpcUKQgDMD1Am11RoW4zbHPjfVlAftHodFQAVhGUcmfmxCgYjmL2DeYUw9gGqk2KEFnVs6+HZYsxE0eJIZPMylp/6LqkBjO6UtJSNo+SKR/nhe039FEVO16ZNorCgtvt7oK28+yExgXrGBwuqyQr/IsU/+kBCSpwn4qbVgElket/ULbY4HlTDII7ZROQs8K7IYUP+NCaT/ZFEgsx5breKXo0KEsN2kB+6n4OUMFgFfShmr7QR/Fsy5+oMsnCu8ovKfwh8IThR+U+cMS1nS2zXl8pLBH4Zdh/ynacFH99mSG4ToeyW2u44PU8e8S7Gf9k9n/abRhYfv/BBgAjkbzwCwITmQAAAAASUVORK5CYII=";
            
            var walk = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAABVCAYAAADkDNxtAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABQ9JREFUeNrcW91x2kAQPm78Dh2AKwC/JU/gCkwHkSuAVGBRQeQKLHcgKrB4sfMWqCDQganAuZ0sMzeyTtrduwMlN6NhBvRz3+23u9/uid7Hx4eKPX6+vQ3MR4LHuPLz2hzFl69fc9/n9GKDMUAmMFlzDFtO3ZljbkDtOwkGgZTm6BMvOZpjIgWkI1OLA0ThuYX0mToiwzImkNMYm4VIugZm7nHtsjNgzMrOhFaxrTPoimUGAe4x+Z/AdMZn9pe4RywwW8/rD5JcEwWMmci7+dh43KLoEs1gpMLrjtJro4Ex1oHsv5LkGLRspywDgFIGILDIvY96jgrGAnTb4kNrFJheZUDvHPWMpQxG5gPUwciKeqWUVhcF868mzYuMq0gisynCRRtBaIa+AI7+jZJDDKisk2AMEKhbcqbk/x4DkBcYrAifhJffGEDbTgQAAyT3AHIqqy9rGawAQQhOAzw/qHW0wNHLQEDEtb63ZQQ9MOq49mn8sS2Djv4rAhAYydksY4CAoy4i5jrIPaMQ+uyqxdEzQiL0HX30nTQKzSz/iA0kKNV0Q+gdE+/x7FnvwxhKW7JtlgF5MiRyHSpDmMQkwMJGoRllYgcosqAyxNwTIsp5W0cC5lTibhngz5JEuWBA7c4rYTQkmHFTPSQB46LM2iHbZyrsSIOAaVkVlyCUWObQ8NvUzKOUWKhqmQEHjIfzL1vCOQjZFy4ozVjlfSCrKEzICYZ3FQpUFcyooRmxDUUxCCColKn+YYNKfMHsHN/PhFY5LVDGVA8A6skA2teB0jUnc5x/6gPGUhyP3ARbB0pXNBnZX1CMKl8wSDkICNeo8ySgtjB/TeR/WfOdZN/y6Koq4XvUeSdQR8Z9QRQXmuL8KswepWtRXKBgPisGqKkmRrKzganQL8V53bck2k+VpisybZhBQQlURCMo9bdjmqOzJ47Ac9DSCeBD1syJlT5mhZLDHLDoNxgBN3jAPGaUXYAmikGeuKNOxqxsitRRnqC2deWCJky6bFlpTtJbop6LMnRF/FUz/SOhfZowIk5fRegxn8anvhkmQ8ghe2oUM9fAQvxgPPc2xsaTaEsDFGx1MpCFGR2dA5be77FoxhlFjSTnNCOGKnDTXATGejEut/Uc+hbnjYyH0MFAYpm5tbppTag+MO6VXxqMTa+FTTf0AQ7dpiE6maIAgLT43ebMzJ2DYLsA2sMqTc6cMnNPegmazRuceeJBt4VP8y+kZWqd2QAqmEI0OxsYwrvK0Fqt0mXJoNsYlcRZLEN5g3xSU9Rx/CGVvJwtAUPhdF6jrDntpL5P7qHuNs8JuuuAfuJS1tRxJw0GmgBkQFytvKWHwJE6eSzLFIrWHM9aqsNUuTujn3JXTTDxA4PRhdK1fCZmcE60YgtR3SJdqKtDOg9roMdYdNMtN6LQa8Xsq6UMZT3F4CMXmsjXB8L1G2z9SGqil9BVad1LDRMikKMS/hUL6UZtkg+pNNYePJ17ynaO1FlQdh2qG7QZsSmxCtCd5CrrjOwzDB6L/KTBfyCPUbuijX980Mwsf1QBX3azpA6VblmTENVWuKS8/JME3N6w6UZNpo1Vae/t9ZVKr0fcrosyYCeZqDacPQNNXJVdTCBMuvWVY8tSE5xPnE+YdNsz6DaTqubgftIAKCf2DfYuMMeWUFio846EUCqULjBLSUyPaJ13pNGuYV57Z9JEZZpi9od6PY39xx1ihEst/wAAedO8/ggwAB/mQhys4qRVAAAAAElFTkSuQmCC";
            
            var bus = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAH4AAAA/CAYAAAAmJ3xBAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABVxJREFUeNrsXetR6kAUXjL+x1uB3P/OECoAKiC3AqGDUIGhA6zgxg6gAmMFwmgBpIILFXD36IkTY/YVNoSN55tZUVlI9nx7Xpt9dI7HI5Ph7e3tmr9MeQl4GTJC3TjwsuFlBeX29nZXx0U6MuI56SF/iXjpEh+N4Rk44B0gOQvxnPSYv9yR3C+qA0xtWYBS4on02vGAphwArtTHMtFwA6D9S+vEc9LBn/8lbmrFuMx0YzwVYpG51zVq/94K8XjhneCicww6CGZ40iW+wEOssABbXkZVyb8q/L2U9LSN7QDjJ4CTaPwZJDNA6yvipM9LwutUIt8r9LKAqLoccEJB60fo25mE/GtBpxuJ3vNyv08pbbtI8jc65OfI9nnZ8HJEN/OP/56oiCc4Sj5kYkA6doJ+4f0hjsl8JZ7/s1dSmeAW+ZB+v0isdpQ3+5nGj0i0zpCvE4ctePlT6CRdTBOJeEfJTzC1lgFy/BVmBHmEmdZnxPskUqfIB0LXkio3nOAIiS/Vei8XGRLcAgTjqeT9e/YxHPxN69+Jh1yPZOik1u81MrFlQevT7DMe9gqCu/7+QVJlgm4cAsIZr99D3/8+ZEv+3W1ETD74Bk/zvll10vh2mPxQUmVY5s490vhWkB8rAr2ojHhCO6DSep9MfTu1HoK2rW7H8CiHbxVkU7KCsrF6Qvt9fZflxvmJ+PYh1jH3VySnRvCEU7LAJ0M6tsHXhJfdiVOogfh7wXvw3B4GcXZEfLPI4qtshdI7YZycbDUNdITEZK4jkMo/v5XEbmDul2TqLxNd7Az3aB32OMMmMNB6EUbwo/P6+nokOTuFAxK7FLkEzNlfJFahQ8S7jTV2gKSE/D0Tj9+PydS7jQm6gqRkPF62+GVExLcDw5IOICPeJ1PfTjxjeihagpUS8T8UZOqJeAIRTyDiCUQ8gYgnEPEEIp5AxBOIeAIRT/gRxMODghkvA15+s48dGh5rulaK1/oFkwzgFf9OHeYGZDWA9tTRproe0sxF227iY0OY/N+1KKBQtNebo9uzznCqdFl7YG58wk5cD1EH8XPVXqtI/pMNq1K2ErTkeiAoV7ZcX/A2RYr2APnwvP3mFFO/tmlydTbYxalCNsx+pFkvdIT0VEU6ym9v0HYh8UuLNx4b1F1ZEFKiUxF3i9o6QPzqTPJ78DR2VTBBUlPdMuwM6+8dIF77Hk/YuRoUIPLwS8IaI27C5eBzx+urXA+aYgQMnUC0dNpXROOBgSaLgrJsFUmGa0EEa7qhgy9JBXdnJqAnCMwCXd9dXO9eQnDRIsT5TKGjOoxIcFHwLxOB3+1pfoco0l7z7wg0MwBh2lO41pSJD19Y6ARUNoF70InWtw0wJqnKAcO8v5YBHJFW36DV0CFiqPndMm1cKnp+phmyALaJwxdkbYpFW40X5DeRWExWF/GyiPJORj7uovxXNzPAZUKiiBzcTiLaqw//nyjcUxPEy+TXV7QpUshPK9qvZOrxBoAg2YhYiiQmOR87ZfIRp0eINQxNdd6vrVCbehhDqAZttF1TDeZeJb98m7K4IGDqEc+xTpp7CvGy82uqAExUTzL0umMnjFQJMLdxolNF+fXQ2tg8HOJLfFSHqf88N8XiTQeK3NT2sSlpU6TnXJjNEcUDMzhswjvx5sGkzCzc9ExlnjDSnVkUUuPn72BGMrPUHqNDiTxLNz9m1R4XpuiT4jMKKhPSRRyllmvT4Zzt8SzdfILB20KzAQes65seaYaCGrBqY+/bSyK90CYIRp9NfTrGRcbtqRzcKQKXABvil6ROSbaDsoXrTNFP9jUsS6RrWZoEpnEhkx82KNwQQRf/BRgAXvVANSxopuYAAAAASUVORK5CYII=";
            
            var ferry = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAH4AAAA/CAYAAAAmJ3xBAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABkZJREFUeNrsXe9x6jgQ98vcd+jgcd+ZgasgTgUxFcSpIKSCkAoOKsBU8KCCMx04E74f6QAqyEl5a0Y4uyvJNgfGuzNMZhJLSL+f9q8k58fn52dwDtlsNtQXr/v9fhiInFRuBAIhXkSIFxHiRYR4ESFepOHyxxm/+474/U5oOb38OFceL3KFGr/ZbLrqx/iC5pn1+/2l0H1ijVfEp+rH7YXN9U9F/lYoP1Fwp0hPLpB0LYnQfSLiFenavD9c6Fxv1fhCofw0Gh9d+HzHQnk78/h7pfU9ob2dBRzR+pYSH0O6KcS3TDqafCG+nTIW4tspP5W5j4X4dooQ31JpdUGn7fvxsRDfTnloa0FHTuC0NMIX4lta0JETOOLjRYR4kasX65k75nKjyGXLWn2m1FlDF41/EwwbKUP1SauY+kQwbKRMlLbvhPiWmXlF+rRScAerZiVYNkqsRSnXqF4uIzRHZkpZszqJ3wumFy8f2rfXlseDuRetb4CJ5wK6Mhp/CnOvLchr8PvW7MgjbTTbrEu0udZ4Ze1zP9CrVr/ZbPRq6tQ00Ds10NTouwt554Bp86jaJIUx6Tbcla3nYoSr2miA7q+IdK1EQ5+7gb4l27pSuzeTdMOdcCnIR5F0EK7NnkhrJlem7VPfC6HnIp7yQ9zgt559lW3TuIBOke69kL2IhzTho4bBDok98Jhpc0u0CZk2A+KETXRFxMdlGpXZnasjyNNxwtIk0vGmbWoSCUekX2zjLbQJr8jUL4ou8yTBHQCnQfy3zmhUfXSfP/+HNl1L8Ni0gK7nmr5VJh7Iz64IwKbKs60eX7eprzPIEymfFU2rdFCWeKniNTCgq0w85IyyY3c+E59V7aTK685s1a+3Qr5c1wuRPgr5+TCor5poloB9g8fAEYsqAeaiqomvi/g5EmnqgSVYJQnSr3HJiS+C3xWqDOk3hH7vS5IywerckMG4pIxU1K3HtCxG3pDGRoBVx2OctV3+qHSuHl5t9mAMLHIpHap2Ew8wNYCxywaE6jeCwNMVzFeXqpfqdwj9ui7YFYx5Z+m3C/3eO5Aelk3dUOLf3997RrCwgxW6dSReA/2rODDQlBDM5cFCmNoK2j93ID3M2wFQIZj3XI7eWgkkpQ7kH234gNUIjb9rDNIcC8dNpNwcx4XxhGDic4zTAhamAjmRbliNHoWFjfgt4svWoL07B/KzfGAwGK1BT4x/jgwibZr/l/HsGPruEAtkkvs/IPEflxwYnk0Yfz6DvvP5bZlFdfh/OrD4Eya2WYNVyBdWijyL8qCenVowjm0VPU38p4u2OSwAV4040jZmW9Ukh9MIVNuYRbVSz0QeVudI65hFdaikeVidA8aAX2YsQH2EalwHxlQ6R2266EGnHhcKXQekZW68lCAKvh/rWhukTwP3t2U+AOEB+O41Fi8Ybmru2O8A5heAJr0iz0RAes+RdBPjvPQawxhHRdKNgNoH45gjPoSJrIiBuQRVU2JAK4jGsfN6X5s0xoRNciKDnCdCAxcBfmrnhVlUJjkJYSYXAX6yZwDzxBbVq2Falwjpe8BihWBxwFj3oT5dIsOYEJYxxwJT4ClYHz6qB0CwVUVGv0aAV5xolIMBVgPTXNMnRhC0faWCjD89qlGD7//bYnYjCKhSxr1o8MZGgBoSJH6dHILx6QW7M9wW5l6Osp2SGGPu5Vu2Q7hEXd4dWtM5xj/dFQMGmESGPDsqrlrGP6ETJjaCZpgJJCaM/uNCihwMHGZR95CAiyLn25EoBjcMY0oBUB9OLOpvuN0g5diMqAUvzZcFGSu3g3zJEul3R/jzF9Daw0SByAFCzpgoIceI2dcHNxJkz/8Fs05Ev0uI6lG/XCAdc4kxlhrD71wxTgmMqcANw/gpj32sBRwmks7Lj6i/wTQHqd7NmVLskPCR7GFCxlLtQbuoEuyIy30tkTS3xz+jFqpDWsZibCvmEJbKxDjliPdJHZzIcSxYBL7keBaFyGJLiUVVudLmebbBOcW25Pr07hwMOgzczrvnA3Kq+AHYC8fJPrpWo8D8PdZJuuH+wsDtNpFvedUX48xxzGMO4xtL45z8GQcgaLrXViGA/syAuYZgJ/HsVz9/x9Qn9pAZxJ79ZuCGuEscM9+auoHxqwWLshiPMCz+E2AAh+VnxXqJ6ugAAAAASUVORK5CYII=";
            
            var taxi = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIgAAABVCAYAAACSGqyjAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABmlJREFUeNrsnV1y2jAQxxUN78kN6r4zU3KCuieobxDnBIEThJ6g4QSBE5SeIM4JChPeG07QcIJUmy6tCwZ2Zckf8v5nPPBgjCX9vFqt5NXZ6+ur6pJWq9WF+cjM8eHIabN+v58qkdIdLHN6Ag7QlQFpIHh0E5DE8XkCiEgAEYkEEJEAIhJARAKISAARtUO9pt7YarWKPV36gnhedOge+v1+1hVAzpoSasfI5VD9CVCdt6Dulua4M7BMBRD/cKTm476ldQigpAaUhfggAkeRYF7nB5ZDLIhjOCLzsWhJl0JRcLPAdVuQaUBwgGAWOCifpDYLYioSHNKvgfp239EveRFA7EcsPwIfIYLzGrcdEl0DHBCHmHcghADOa4blFUCYfsc71Q21HpJKuxgLv2NZ0trE5viI3784KsYAr3vOLEcrYyWVAYJh6wfi6RtzJGVD2uY/x+bjFr6ba505Lg8MZ8cMa7hBn6RVkOiK4OD6HXHT5zsgxG6OyHydEH9yjt3NQADZV8YwyaM2PWXmXqHbvA4VEl2B9bhTp18z+Bs/MBV+17Z+GifsgoREe4YDZmZvOI5cW719hOQSfQ0qJElnAcEnZMpw4FofecSuMWZA8q3pk3zaExwXijfPMgxlujwHyZr4k/smQ+LLgnD8jkloi24QkgF2m62GRHuwHlDQK6rfgaOA4ITdZdx2SLRjOAZoPah+R6wCVg6SRwYkw5AtCMfvSEKYDqdAYg6AZEb8ydcmrSnRDq3HmOF3fOnSynAEJWVA0piFR9oRHNC13BJPfzSVNVYdVBsh6ZWAIlL/ZjapAZ+N6njeDYDE1B1Yz3siJKrOda49BhAX2LgxHjZrOoZd8DsIkEyh4TmQ1FV3PYKVAChShn9xSOvQXzLyCQlYa1gyUTUk+lAsA83gT/Vngc8HB//1LFjsQ6LoywVqWZ3W2wEjwTiGjyWB8ASkYkX2xLEIAMkvU48QfFtsD58jwr8rytBjvpL2aq3y0GSu5rbeABE4ghVEcDMExsrKnD09PaWq3e/GijwCA4AsHDmhonYJYlJzBGZ+aHQEgLxKXQksCMt017IIIKKibmi8BUVylIl2BS+aPeBi81pylI1wKHZK1FVpMLzjrKGgXhcm1aaE81LGCNDXvfrQDUR66wCEFNgxN0cNIL1whnCM6z4T7zNmlN3XvXqDRLoY0VEJICIBRCSAiDypjjgIePIU5+uFONqBVW2cKXDqdTnOJ9Xx5N4rnF9rkr86RjGchc1jgqcP59wy/p963Uz9Sz5zTOQ1thb3Kl2MSAARCSAiAUQkgIhEdY5itmsmn3EIGanu5Ev1pTXWZ4b1OVCOJ/eqAATyk8JClOeCYR+AMiYOJ0W5obXKrdnYqVOIswzxKB1D8RkoIyePDTyxv/MHjhjH2aYAK2VRfPkgYPrISWMxs+G1tP1JjahBOYtUWJUCwk5Ihy9UfRcGDoqdIhTbIGkaILMSb3oNhQO3dYOWZNYkQKyT76MjK1akwCktcvKraBNdhq4DjVx2L5iF8LCnrKY2WWs0XRtHBdnUXRmBysVDs7T4TarRkUkdFeRF2jIYQf7aTOdMkIsNd1xERmNpmz25SPzPiYc8bvPX6lw/NXbhjzhIUD8QHtw+NMw2WeaHxnrHmYGuZlKyMEmJgkTm47PwsKePWDe+22Rvp05d4PFuN8ixdTivmC8T5XUnLLitGwy5U978A59jsBvgPLhnHRI7LDD5lIk1gCviRFMxTzklT8l2BjMkRUT/bcSJpuLE3YJw7Y25buFiavamhoxsRL4m6y5D2Tpk5ymnbjTtY7JucmhTBRtAoPt4YPzE5XT/I+Y9D06MVfRv9QBdTlEADC1/qnjT/e8PRWqttkVlFiZvUfILhmze+fgUao53iwdv25W/Ja1T9guGZscyOdsCwjGJrhSs9cjV67ziUdxJX9Fqsg59gFHFBelCjvdUuZv2IP3fqYGE9WwuetOzigoSd2VvGUXfFLGsRpRJPF2yQKlnSKCirkMbtRCsc+IZkgl1uGzlgxT0nfBnN44LAfGOpEtwFPh5mXL/8vY1Jx26dkQ9DKk+qZLrH/OeNXjkXYUjZ0kihxYahsaX3Fz5TizIDvkpxjbeWYIx7dp2ZcQh8NByhLPEmMnU5r+dA7JjIpMTMY81juPBWZrLZkMn6zRS/2/qdH7EWizwYStlhX8LMADOLsahZpAMLAAAAABJRU5ErkJggg==";
            
            var flight = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHwAAABVCAYAAACLvYBAAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABRJJREFUeNrsndFt2zAQhhWjjwWaThAPEMDJewHLE8R56HOcCepOUGcDZYKoE8SdIPIEVZC8V56g8QTpnXFOCcWyKJOySN79AKEAkWWKH+94R1H00evra+Sqnp+fCzicaJ6+gJKcnp7OI1GljlwFDrBjODzs8dFHKDGAfxG879VztWIALCOrbaoBlAw6zLHg9Qg4abrn5wS6j8DBynM43Ah0PhaOSqAsBToT4BR8TQ0uIdDVKP3p6WlWZVkuRboADIO4ocElJHon4NgAnyoaaAwNVDgCvA+HP4aXYQ8dXXq2wxXm0NBnjrj2wiCAE/euATwiy89cgW4YwAl0Al43FekMdAsBHHvoPXKVS4+gYwddWII+52jhUY1bd9G9TyxdZwj3k3IErtvTnYBuKYDb6IoT9CYWXoYeBxDAsYPeU4KhRUPoD9BIkw6t3FYAxwq6OrW6TwBz1zF0WwEcG+imwDuHbjGAYwG9VwqElr5BtxzABQ+9/LTMJC/t0tJtBnBBQy8DN73BTqBTANfG9wYH/d0iRrjBqqdnTfQdICS+NQbNLxxv6VBZyMCxR19ZuPZPaKhJJHIe+BgO95au/wtKbrnOBZVDKQ/p+fnWdemW3LpoP+2aV8jIgLJ9O2EVcBx/v0nbO61Hyk7mTeBXAcfg5be0qRdaUXaV6CxHq3zVqOF7XSI3dAtltsviezWTGSK3XfqCymbSCYfhggLvxsDnjtzUZaQ3dYqu7ZrKSmeugK79qJNiQhlR1qFT55FmnZdUB906q5qCJcdU+nQ/Kwq27wH6fNsSrp1vj+KH4HDRIfDPG/cEdZnB4ceOcy83rwprpJa3cO6UzsVG+bsrasZGVdoEo+TBjk7XV+pcF/yONpM6e6TDo/KEEC3lThRm2Pkm9MpWrYV37dZXpbGoLiDJ9zmXvmNVk/dHmtd+aVhn9f/GuT4GbVDGZO1R9H+x5pmWhTsQvK0tkXpuVlMPHMvGynA0rHGl6AoLzZnFtTXRc4K7mnNv4NwZNXJWM5+BQ8REybEHJha+JdPafP+b59EBrnOTom7imzqv0FeMZD00ae0AUTNuifzRue7bozHleCK/9VULOPp+impHkd01ZKLD6mOj98MxSKAU5ZxyU5FnMtrFiXLYMZULaU7ndW1t264S/DiSx6suRvVfWtunjfLAWCnSAboTzjvg5g75wTbmow6w6QRnkuYdTAuC/WI8hlvoBDFNDvSpI6gTBSJzq8ZHpam1oK1lb3BMnkA9RvS3DA/bhVOoOK08r9pz9sjlzXUNA8iig46BDy3Kiza3Ln22LPzOQn0qVqUPgfb0pAPY12X3ScqCycMdtW6MBR4cge2cegFadyqwmQCnVTEnApuBS7e0U2PQsEOz8FRgMwFOq3KGApuBSz9gzo2TGrFOrisW3r4rF9gcLPxAOXcwsL0GTq48bzkNCwq27y59KrCZWPgBXmcOErbPFp4IbCbAwbqnLebcQcP2zqW3nHMHD9tHC08FNhMLbzHnZgPbG+At5tysYPvk0mcCm4mFt5Rzvy3Mj5jJh0WMqeXrsf45yp7j1o0590BgM3DptGQpt5iGya8LO27hicBmApz2LLsQ2AxcuuWcW2B7YOEzgc3Ewi3m3ALbEwtPBTYT4PSa0EBgM3DplnJuge2RhacCmwlwyrmHApuBS7ewZGm9/bTA1lfXT8tmBrDllw99snDDJUsC28MxfCqweQFv+jIBjteXAttM/wQYAA3Swvys2YEyAAAAAElFTkSuQmCC";
            
            var clock = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADgAAAA4CAYAAACohjseAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA/JJREFUeNrUmutto0AQxwnKd9xBXIClkApMByYVhKvgfBWcr4JwFRyuIE4HpIIDyQXYHcQV+HZyg7Xgfcw+ADMSsmQt7P52Z/8z+7g7n8/BFGy/38fsp8QnWywWn5T37qYAyMFF+NcJIXeTBxTA8fabQa4nC6iBa+yDPanMZb0CsgYl7Kd5Zux5lBQFF6u4p2QNPAi+l7Of74Sqa6hTBOkMyBqRQg/iEzl8ChpZwMM3lH0f/nuxhbQGZBVn7GfDngfPngmjC+KxaUbVBdIYEEcs7wFMZL+gLmiwLSQZkFUwQxdaDaw1RwwJpQHkOyufkgFRzXYDjZpqNKENf4nlfzDIXAuIcy13FJCx7EkJiHB/gulaLQV0hDtigD7gL2/g7nOMlY+OalvqNEEISMwgZPIOqlcZCBeIwdoCdsvqyTTCc7oCxEorQ0G5yLlCpJqcUdgBmAUVhvU+Q8ItgYQOT0SAO4NQUKOEV4pRmmOH8SuBWJSaYXlIHn4aeM3XtzqQJ4yFVSgI4isDF4kJ7thN4SKcg0Jj3wPAZ2ykziJUeHgPNGPLw8H/YeeF3MT/iWVnpuqB67yECLlC924gW50edlTzwTOctWEjE2LxDfdey/VDUSHVnBsCrgP5jVB02Yxi10Ju7j0QJnQ6dKRmkCAe74SiaykgKCHhA7lM+QawNWE+rjDECQFXhNHLR4Jr5hWl/isPC9E9KaP3GYxrhRUg5oY+Pj7EKOrmYiIC1ElxPeLc65puHzTCzOli96qsAq3soaExawi17IHrYEpb5riKuQDqwkPVA+CrySoFYhzERMw5KYDCQC/twZHdMuokIR++ASdtUwG0jsH3AzTORaQg9u5cVLx3QNjP9KzEc98umtyYuxqpfmiqSmOabEkkcOsWoM6/0xsavZQ4JVqAuvkRERPyWwCsRXOw9NFzA7mnbv5dsYQowUfNiy/dJHYEo2ypFDIVLWxeHnj0lppiR9EWpgngcoy5yJ1LWg1AyC0mt5SP4Db80Ct5yoZYrgv0FB+PEHI20OgBHGWnXbqlEna2BCijCKdAZd+QBsfVyg2x1uELNvoQ0I7NWmcAnudcToQD+zqqJgFiBSAkbwZtUh6dWaglZc5dFr+sXmX6JjsApd4wukh08P9eS2EJFqMGmNzgAA+a6zpWdYRdEmKPqFKALHU3AREKej8LzE93ydNDBTjD1MflHL3msvsDtzKJA7dbG0/Uua+7ZeED0qcZCxvlnoypqt0MHAmQA4UTnteR4JR3Qr0AcsKQW4iPy6htVHHOKyAHmgX9XKXkDbKqtWt8dboQi6CZxxFt0q7C14GPlyvNuBhubv2awtao1CXlFr2p/RNgABjBBdNkZebHAAAAAElFTkSuQmCC"
            
            var duration = {};
            duration.hour = parseInt(data.duration / 60);
            duration.minute = data.duration - (60 * duration.hour);
            
            var imageLogo = "";   
            imageLogo = imageLogo_y
            moment.locale('it')
            var content = [
                {
                    columns: [
                        {
                            image: imn,
                            fit: [38, 38]
                        },
                        [
                          { text: capitalize(data.from)+' > '+capitalize(data.to), bold: true, fontSize:20, color: '#707070', margin: [-210, -5, 0, 0 ]},
                          { text: moment(data.date).format("DD MMMM YYYY"),color: '#707070', margin: [-210, -5, 0, 0 ]}
                        ]
                    ]
                },
                {
                    columns: [
                        {
                            image: imgMap,
                            fit: [15, 15],
                            margin: [0, 20, 0, 0 ]
                        },
                        {
                            text: data.distance+" km", 
                           margin: [-30, 20, 0, 0 ], 
                           fontSize: 11,
                           color: '#AFAFAF'
                        },
                        {
                            image: imgH,
                            fit: [15, 15],
                            margin: [-15, 21, 0, 0 ]
                        },
                        {
                            text: duration.hour+" ore "+duration.minute+" minuti", 
                            margin: [-46, 20, 0, 0 ], 
                           fontSize: 11,
                           color: '#AFAFAF'
                         
                        },
                        {
                            image: imgBenz,
                            fit: [15, 15],
                            margin: [-6, 21, 0, 0 ]
                        },
                        {
                            text: "20 lt (64 €)", 
                             margin: [-37, 20, 0, 0 ], 
                           fontSize: 11,
                           color: '#AFAFAF'
                        },
                        {
                            image: co2,
                            fit: [15, 15],
                            margin: [-17, 22, 0, 0 ]
                        },
                        {
                            text: data.co2+" kg CO2", 
                             margin: [-49, 20, 0, 0 ], 
                           fontSize: 11,
                           color: '#AFAFAF'
                        },
                        {
                            image: imgEur,
                            fit: [15, 15],
                            margin: [-33, 21, 0, 0 ]
                        },
                        {
                            text: data.indicative_price+" €", 
                             margin: [-66, 20, 0, 0 ], 
                           fontSize: 11,
                           color: '#AFAFAF'
                        }
                    ]
                },
                { 
                    image: separator,
                    fit: [550, 20],
                    margin: [-10, 10, 0, 20 ]
                },
            ];
            
            
            var first = true;
            
            data.travel_saved_segments.forEach(function(details){
                var duration = {};
	            duration.hour = parseInt(details.duration / 60);
	            duration.minute = details.duration - (60 * duration.hour);
	            var dur = '';
	            
	            if(duration.hour > 0){
		            dur = duration.hour + "h ";
	            }
	            
	            dur += duration.minute + "min";
	            
	            if(details.company == ''){
		            details.company = '-';
	            }
	            
                if(!first)
                {
                    content.push(
                        { 
                            image: separator_light,
                            fit: [520, 20],
                            margin: [-10, 10, 0, 20 ]
                        }
                    );
                }
                
                var transfer = "";
                
                switch(details.kind){
	                case 'train':
	            		transfer = train;    
	                break;
	                case 'car':
	            		transfer = car;     
	                break;
	                case 'tram':
	            		transfer = tram;    
	                break;
	                 case 'bus':
	            		transfer = bus;    
	                break;
	                case 'ferry':
	              		transfer = ferry;  
	                break;
	                case 'taxi':
	                	transfer = taxi;
	                break;
	                case 'flight': 
	                	transfer = flight;
	                break;
	                default:
	                	transfer = imgH;
	                break;
                }
                            
                content.push(
                    {
                        columns: [
                            [
                                {   
                                    image: segment,
                                    fit: [400, 35],
                                    margin: [0, 0, 0, 0 ]
                                },
                                { 
                                    columns: [
                                        [
                                            { text: "", margin: [5, 0, 0, 0 ]}, //orario
                                            { text: capitalize(details.s_name), margin: [0, 0, 0, 0 ]}
                                        ],
                                        [
                                            { text: "", margin: [40, 0, 0, 0 ]}, //orario
                                            { text: capitalize(details.t_name), margin: [0, 0, 0, 0 ], alignment: 'right' ,width: '35%'} 
                                        ]
                                    ]
                                }
                                
                             ],
                            [
                                { 
                                    columns: [
                                        {
                                            image: transfer,
                                            fit: [20, 20],
                                            margin: [70, 0, 0, 12 ]
                                        },
                                        {text: details.company || " ", margin: [-20, -3, 0, 0 ], fontSize:10, color: '#a5a5a5', bold: true}
                                    ]

                                },
                                { 
                                    columns: [
                                        {
                                            image: clock,
                                            fit: [13, 13],
                                            margin: [75, 0, 0, 0 ]
                                        },
                                        {text: 'Durata complessiva: ' + dur, margin: [-25, 1, 0, 0 ], fontSize:9, color: '#a5a5a5', bold: true}
                                    ]

                                },
                                { text: details.indicative_price+' €', margin: [70, 0, 0, 0 ], fontSize:40}
                            ],
                            {
                                image: transfer,
                                fit: [20, 20],
                                margin: [-410, 35, 0, 0 ]
                            },
                            {
                                image: separator_light_vertical,
                                fit: [40, 100],
                                margin: [-500, -10, 0, 0 ]
                            }
                            

                        ]
                    }
                );
                
                first = false;
                
            })
            
            var dd = {
                content: content
            };
            
            utilities.elaboratePdf(dd, function(file){
                res.send(file.substring(file.lastIndexOf("/")+1));
            });
        }
        else
        {
            errors.sendError(res, errors.codes.invalidParam, 'travel saved not found');
        }
         
     })
    
}

function capitalize(s)
{
	if(s)
   	   return s[0].toUpperCase() + s.slice(1);
   	else
   		return '';
}

module.exports.travelDone = function(req, res) {
	    db.TravelSaved.findAll({
       		 where:{id_user: req.user.id, id: req.params.idtravel, travel_done: 0}, 
    	}).then(function(data){
    		if(data) {
	    		db.ActivityStream.emit(6, req.user.id, function(activity){
						db.TravelState.create({
							id_activity: activity.id,
							id_travel: req.params.idtravel,
							id_state: 3
						});
					});
	    		
    			db.TravelSaved.update(
	    			{ travel_done: 1 },
	    			{ where: {id_user: req.user.id, id: req.params.idtravel, travel_done: 0}}
    			).then(function(dataTwo){
    				db.TravelSavedSegment.update(
		    			{ segment_done: 1 },
		    			{ where: {id_travel_saved: req.params.idtravel, segment_done: 0}}	
    				)});	
    			ret = {isSaved: true};
    			res.send(ret);
    		}
    		else {
    			errors.sendError(res, errors.codes.notFound);
    		}
    	});
}

module.exports.segmentDone = function(req, res) {
		db.TravelSavedSegment.update(
			{ segment_done: 1 },
			{ where: {id_travel_saved: req.params.idtravel, id: req.params.idsegment, segment_done: 0}}	
		).then(function(data){
			db.TravelSavedSegment.findAndCountAll(
    		{
                where: { 
                    segment_done: 1,
                    id_travel_saved: req.params.idtravel
                },                   
            }).then(function(conteggioDone) {
            	db.TravelSavedSegment.findAndCountAll({
            	where: { 
                    id_travel_saved: req.params.idtravel
                },  
            	}).then(function(conteggioTot){
            		if(conteggioTot.count == conteggioDone.count) { //Se tutti i segmenti sono completi, allora il viaggio è completo
		    			db.TravelSaved.update(
			    			{ travel_done: 1 },
			    			{ where: {id_user: req.user.id, id: req.params.idtravel, travel_done: 0}}
		    			)
            		}
            	});
			});
			ret = {isSaved: true};
    		res.send(ret);
		});
}

module.exports.travelBuyed = function(req, res) {
	    db.TravelSaved.findAll({
        	where:{id_user: req.user.id, id: req.params.idtravel, travel_buyed: 0}, 
    	}).then(function(data){
    		if(data) {
	    		db.ActivityStream.emit(2, req.user.id, function(activity){
						db.TravelState.create({
							id_activity: activity.id,
							id_travel: req.params.idtravel,
							id_state: 2
						});
					});
	    		
	    		
    			db.TravelSaved.update(
	    			{ travel_buyed: 1 },
	    			{ where: {id_user: req.user.id, id: req.params.idtravel, travel_buyed: 0}}
    			).then(function(dataTwo){
    				db.TravelSavedSegment.update(
		    			{ segment_buyed: 1 },
		    			{ where: {id_travel_saved: req.params.idtravel, segment_buyed: 0}}	
    				)});	
    			ret = {isSaved: true};
    			res.send(ret);
    		}
    		else {
    			errors.sendError(res, errors.codes.notFound);
    		}
    	});
}

module.exports.segmentBuyed = function(req, res) {
		db.TravelSavedSegment.update(
			{ segment_buyed: 1 },
			{ where: {id_travel_saved: req.params.idtravel, id: req.params.idsegment, segment_buyed: 0}}	
		).then(function(data){
			db.TravelSavedSegment.findAndCountAll(
    		{
                where: { 
                    segment_buyed: 1,
                    id_travel_saved: req.params.idtravel
                },                   
            }).then(function(conteggioDone) {
            	db.TravelSavedSegment.findAndCountAll({
            	where: { 
                    id_travel_saved: req.params.idtravel
                },  
            	}).then(function(conteggioTot){
            		if(conteggioTot.count == conteggioDone.count) { //Se tutti i segmenti sono completi, allora il viaggio è completo
		    			db.TravelSaved.update(
			    			{ travel_buyed: 1 },
			    			{ where: {id_user: req.user.id, id: req.params.idtravel, travel_buyed: 0}}
		    			)
            		}
            	});
			});
			ret = {isSaved: true};
			res.send(ret);
		});
}

module.exports.downloadPDF = function(req, res){
    
    var file = global.settings.upload_dir + "/" + req.params.file;
    
    fs.stat(file, function(err, stat) {
        if(err == null) {
            
            res.download(file, 'viaggio.pdf', function(err){
                
                fs.unlink(file, function(){
                
                });
                
            });
            
        } else if(err.code == 'ENOENT') {
            errors.sendError(res, errors.codes.notFound);
        } else {
            errors.sendError(res, errors.codes.notFound);
        }
    });
    
};

module.exports.callbackTravState = function(req, res){
	db.TravelStateType.find( {include: [{ model: db.TravelState, where: {id_activity: req.params.idactivity}}]})
					.then(function(activity){
						if(activity != null){
							res.send({state: activity.description});
						}
						else 
							errors.sendError(res, errors.codes.invalidParam);
					});
}

module.exports.checkTravelParams = function(req, res, next){
	if(req.params.idactivity === undefined || req.params.idactivity == '' || isNaN(req.params.idactivity) || req.params.idactivity <= 0)
		errors.sendError(res, errors.codes.invalidParam);
	else
		next();
}

module.exports.checkDeleteParams = function(req,res,next) {
	if ((typeof req.params.idtravel == undefined) || (isNaN(req.params.idtravel)) || (req.params.idtravel =='')){
		errors.sendError(res, errors.codes.invalidParam);
	}
	else {
		next();
	}
}

module.exports.deleteTravel = function(req, res) {
	db.TravelSaved.update(
		{ in_wishlist: 0 },
		{ where: { id: req.params.idtravel }
	}).then(function(data){
		db.TravelSaved.findAll({where: {id_user: req.user.id, in_wishlist: 1}}).then(function(dataTwo){
			res.send({length: dataTwo.length});
		  });
	});
}

module.exports.getUserTravelInfo = function(req, res) {
	db.TravelSaved.findAll({
		where: { 
				 travel_done: 1,
				 id_user: req.user.id
			   }
	}).then(function(data){
		var km = 0;
		var co2 = 0;
		for(var i=0; i<data.length; i++) {
			km += data[i].indicative_price;
			co2 += data[i].co2;
		}
		km = parseInt(km);
		co2 = parseInt(co2);
		obj = 	{
					"km": km,
					"co2": co2
				};
		res.send(obj);
	});
}

module.exports.getTravelReminder = function(req, res) {
	 var now = moment().format("YYYY-MM-DD");
     db.TravelSaved.findAll({
         where:{
         	id_user: req.user.id,
         	in_wishlist: 1,
         	date: {
         		$gt: now
         	}
         },
         include: [db.TravelSavedSegment]
     }).then(function(data){
     	var reminder = {};
     	for(var i = 0; i<data.length; i++) {
     		var travelDate = moment(data[i].dataValues.date).format("YYYY-MM-DD");
     		var d = moment(travelDate).diff(moment(now), "days");
     		/*
     		 * Attivo le notifiche, 1 giorno prima e 5 giorni prima
     		 */
     		if(d == 1) {
	             global.notificator.notify(req.user.id, "Domani ha un viaggio da effettuare", 6, {
	                    from: data[i].from,
	                    to: data[i].to
	              });
     		}
			if(d == 5) {
	             global.notificator.notify(req.user.id, "ha un viaggio da effettuare", 6, {
	                    from: data[i].from,
	                    to: data[i].to,
	                    date: data[i].date
	              });
			}
     	}
     	res.send(data);
     });
}

module.exports.checkParamAll = function(req, res, next){
	if(req.query.offset < 0 || isNaN(req.query.offset) || req.query.offset === undefined){
		 errors.sendError(res, errors.codes.invalidParam);
	}
	else
		next();
}

module.exports.checkParamList = function(req, res){
	if(req.query.limit != undefined){
		if(req.query.limit == '' || req.query.limit <= 0 || isNaN(req.query.limit))
			 errors.sendError(res, errors.codes.invalidParam);
		else
			next();
	}
	else
		next();
}

module.exports.callbackSearchList = function(req, res){
	
	db.sequelize.query("SELECT * FROM user_travel_searched WHERE id_user = :iduser", { type: db.sequelize.QueryTypes.SELECT, replacements: {iduser: req.user.id}}).then(function(tot){
		var query = "SELECT id_travel, b.description as fromc,  a.description as toc, data  FROM user_travel_searched JOIN travel ON travel.id = user_travel_searched.id_travel JOIN locality b ON b.id = travel.id_start JOIN locality a ON a.id = travel.id_end JOIN activity_stream ac ON ac.id = user_travel_searched.id_activity WHERE id_user = :iduser GROUP BY a.description, b.description, ac.data ORDER BY user_travel_searched.id DESC";
	
		if(req.query.limit !== undefined){
			query += " LIMIT " + req.query.limit;
		}
		
		db.sequelize.query(query, { type: db.sequelize.QueryTypes.SELECT, replacements: {iduser: req.user.id}})
			.then(function(search){
				resp = {
					count: tot.length,
					search: search
				}
				
				res.send(resp);
			})
	});
}
