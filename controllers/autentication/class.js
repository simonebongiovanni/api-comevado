var db = global.db;
var utilities = global.utilities;
var errors = global.errors;
var aws = global.aws;
var request = require('request');


/**
 * Auth Controller
 */

exports.login = function(req, res) {
        
    if(typeof req.body.client_id != "undefined" && typeof req.body.client_secret != "undefined")
    {
        exports.loginExternal(req, res);
    }
    else if(typeof req.body.uname != "undefined" && typeof req.body.password != "undefined")
    {
        exports.loginInternal(req, res);
    }
    else if( typeof req.body.provider != "undefined" && typeof req.body.token != "undefined")
    {
        exports.loginSocial(req, res);
    }
    else
    {
        errors.sendError(res, errors.codes.invalidParam);
    }
     
}

exports.loginExternal = function(req, res){
    var client_id = req.body.client_id;
    var client_secret = req.body.client_secret;
        
    db.UserExternal.find({ where: {client_id: client_id, client_secret: client_secret}})
    .then(function(e_user) {
                
        if(e_user != null){
            
            e_user.refresh_token = utilities.randomString(128);
            e_user.session_token = utilities.randomString(128);
            e_user.timestamp = new Date();
            e_user.save().then(function(session){
                    res.send({
                        session_token: session.session_token,
                        refresh_token: session.refresh_token
                    });
                });
                        
        }
        else{
            errors.sendError(res, errors.codes.invalidCredentials); 
        }
        
    });
      
}

exports.firstLogged = function(req, res){

    db.User.find({where: {id: req.user.id}})
    		.then(function(user){
	    		user.first_login = false;
	    		
	    		user.save();
	    		
	    		res.send({first_login: user.first_login});
    		});
}

exports.loginInternal = function(req, res) {
        
    if(typeof req.body.uname && typeof req.body.password != "undefined") {
	    
        var username = req.body.uname;
        var password = req.body.password;
        
        db.User.find({ where: {
	        $or:{
	        	username: username,
	        	email: username	        
	         },
	         	is_active: true}})
        .then(function(user) {
            
            if(user != null && user.checkPassword(password)){

                user.refresh_token = utilities.randomString(128);
                user.session_token = utilities.randomString(128);
                user.timestamp = new Date();
                user.save().then(function(session){
                	
/*
	email – Which displays the visitor’s email address and it will be used as a unique identifier instead of cookies.
	name – Which displays the visitor’s full name
	company – Which displays the company name or account of your customer
	avatar – Which is a URL link to a visitor avatar
 */	
                	
					// Identifica l'utente loggato
					global.woopra.identify(user.email, {
					    visitorProperty: 'property'
					}).push();
					// Traccio
					global.woopra.track('InternalLogin', {
					    eventProperty: 'Comevado Internal Login'
					});
						db.userLoggedIn(session.id);
	                  //  global.notificator.notify(session.id, "si è appena collegato", 2);
	                
					var platform = req.header('x-platform');
					
					if(patform == 'web'){
						res.send({
	                        first_login: session.first_login,
	                        session_token: session.session_token,
	                        refresh_token: session.refresh_token
	                    });
					}
					
					if(patform == 'android'){
						res.send({
	                        first_login: session.first_login,
	                        session_token: session.android_session_token,
	                        refresh_token: session.refresh_token
	                    });
					}
					
					if(patform == 'ios'){
						res.send({
	                        first_login: session.first_login,
	                        session_token: session.ios_session_token,
	                        refresh_token: session.refresh_token
	                    });
					}
					 
                });

            }
            else{
                errors.sendError(res, errors.codes.invalidCredentials); 
            }

        });
    }
    else
    {
        errors.sendError(res, errors.codes.invalidParam); 
    }
    
    
}

exports.loginSocial = function(req, res){
    
    var token = req.body.token;
    var provider = req.body.provider;
        
    switch(provider)
    {
        case "facebook":
            utilities.https.get("graph.facebook.com", "/me?access_token="+token, function(status, response_data){     
               
                var response = JSON.parse(response_data);
                      
                if(typeof response.error != "undefined")
                {
                    errors.sendError(res, errors.codes.invalidCredentials, response.error);
                }
                else
                {
	                if(response.email)
						email = response.email;
					else 
						email = response.first_name + "_" + response.last_name + "@facebook.com";
	                                    
					db.User.find({ where: {email: email }}).then(function(user) {                
                        
                        if(user != null){
                            
                            user.refresh_token = utilities.randomString(128);
                            
                            if(req.body.source == 'web')
                            	 user.session_token = req.body.token;
                            
                            if(req.body.source == 'ios')
                            	 user.ios_session_token = req.body.token;
								 
						    if(req.body.source == 'android')
                            	 user.android_session_token = req.body.token;
                            	
                               
                            user.timestamp = new Date();
                            user.save().then(function(session){
	                            	
								// Identifica l'utente loggato
								global.woopra.identify(user.email, {
								    visitorProperty: 'property'
								}).push();
								// Traccio
								global.woopra.track('FacebookLogin', {
								    eventProperty: 'Comevado Facebook Login'
								});
								 
								db.userLoggedIn(session.id);
	                             //   global.notificator.notify(session.id, "si è appena collegato", 2);
                                if(req.body.source == 'web'){
									db.User.find({where:{session_token: session.session_token}}).then(function(us){
										 res.send({
		                                    first_login: us.first_login,
		                                    session_token: session.session_token,
		                                    refresh_token: session.refresh_token
		                                });
									});								
								}
								
								if(req.body.source == 'ios'){
									db.User.find({where:{ios_session_token: session.ios_session_token}}).then(function(us){
										 res.send({
		                                    first_login: us.first_login,
		                                    session_token: session.ios_session_token,
		                                    refresh_token: session.refresh_token
		                                });
									});
								}
								
								if(req.body.source == 'android'){
									db.User.find({where:{android_session_token: session.android_session_token}}).then(function(us){
										 res.send({
		                                    first_login: us.first_login,
		                                    session_token: session.android_session_token,
		                                    refresh_token: session.refresh_token
		                                });
									});
								}
                            });
                        }
                        else{
                            
                           var password = utilities.randomString(5);
                           
                           var obj = {};
                           
                           if(req.body.source == 'web'){
	                           var obj = {
	                                username: response.name.toLowerCase().replace(" ","_") + "_" + utilities.randomString(3),
	                                email: email,
	                                password: utilities.bcrypt.generate(password),
	                                refresh_token: utilities.randomString(128),
	                                session_token: utilities.randomString(128),
	                                is_active: true,
	                                first_login: 1,
	                                timestamp: new Date()
	                            }
                           }
                           
                            if(req.body.source == 'ios'){
	                           var obj = {
	                                username: response.name.toLowerCase().replace(" ","_") + "_" + utilities.randomString(3),
	                                email: email,
	                                password: utilities.bcrypt.generate(password),
	                                refresh_token: utilities.randomString(128),
	                                ios_session_token: utilities.randomString(128),
	                                is_active: true,
	                                first_login: 1,
	                                timestamp: new Date()
	                            }
                           }
                           
                           if(req.body.source == 'android'){
	                           var obj = {
	                                username: response.name.toLowerCase().replace(" ","_") + "_" + utilities.randomString(3),
	                                email: email,
	                                password: utilities.bcrypt.generate(password),
	                                refresh_token: utilities.randomString(128),
	                                android_session_token: utilities.randomString(128),
	                                is_active: true,
	                                first_login: 1,
	                                timestamp: new Date()
	                            }
                           }
                           
                            
                           db.User.create(obj)
                           .then(function(session) {  
                               
 
                                request({url: "https://graph.facebook.com/me/picture?access_token="+token, followRedirect: false}, 
                                    function (error, response_image, data) {
                                                                            
                                        utilities.downloadImage(session, response_image.headers.location, ".jpg" , function(file){
                                            console.log("download facebook profile image completed");

                                            session.setInfo(9, file, function(data){});
                                            session.setInfo(6, response.first_name, function(data){}); //name
                                            session.setInfo(7, response.last_name, function(data){}); //surname
                                            session.setInfo(3, response.gender, function(data){}); //gender  
                                            session.setInfo(10, response.id, function(data){}); //gender  
                                            
                                            db.usrAutocomplete.addWithName(session.id, response.first_name, response.last_name);
                                            db.usrAutocomplete.addWithSurname(session.id, response.last_name, response.first_name); 

                                            if(response.birthday){
                                                var tmp = response.birthday.split("/");
                                                var birthday  = tmp[2] + "-" + tmp[1] + "-" + tmp[0];

                                                session.setInfo(5, birthday, function(data){}); //gender  
                                            }

                                            global.controllers.permissions.setDefaultPermission(req, res, session.id);
                                            global.notificator.notify(session.id, "si è appena registrato al sito", 5);
                                            
											if(req.body.source == 'web'){
                                           		res.send({
	                                                first_login: session.first_login,
	                                                session_token: session.session_token,
	                                                refresh_token: session.refresh_token
	                                            });
                                            }
                                            
                                            if(req.body.source == 'ios'){
                                           		res.send({
	                                                first_login: session.first_login,
	                                                session_token: session.ios_session_token,
	                                                refresh_token: session.refresh_token
	                                            });
                                            }
                                            
                                             if(req.body.source == 'android'){
                                           		res.send({
	                                                first_login: session.first_login,
	                                                session_token: session.android_session_token,
	                                                refresh_token: session.refresh_token
	                                            });
                                            }
                                            
                                        });

                                });
                                
                            });
                        }
                    });
                    
                }
            })
            break;
        case "google":
            utilities.https.get("www.googleapis.com", "/oauth2/v1/userinfo?access_token="+token, function(status, response_data){    
                
                var response = JSON.parse(response_data);
                
                console.log(response);
                
                if(typeof response.error != "undefined")
                {
                    errors.sendError(res, errors.codes.invalidCredentials, response.error);
                }
                else
                {
                    db.User.find({ where: {email: response.email }})
                    .then(function(user) {

                        if(user != null){

                            user.refresh_token = utilities.randomString(128);
                            
                            if(req.body.source == 'web')
                            	 user.session_token = req.body.token;
                            
                            if(req.body.source == 'ios')
                            	 user.ios_session_token = req.body.token;
								 
						    if(req.body.source == 'android')
                            	 user.android_session_token = req.body.token;
							
                            user.timestamp = new Date();
                            user.save().then(function(session){
							// Identifica l'utente loggato
								global.woopra.identify(user.email, {
								    visitorProperty: 'property'
								}).push();
								// Traccio
								global.woopra.track('GoogleLogin', {
								    eventProperty: 'Comevado Google Login'
								});
								db.userLoggedIn(session.id);
                                //global.notificator.notify(session.id, "si è appena collegato", 2);
                                if(req.body.source == 'web'){
									db.User.find({where:{session_token: session.session_token}}).then(function(us){
										 res.send({
		                                    first_login: us.first_login,
		                                    session_token: session.session_token,
		                                    refresh_token: session.refresh_token
		                                });
									});								
								}
								
								if(req.body.source == 'ios'){
									db.User.find({where:{ios_session_token: session.ios_session_token}}).then(function(us){
										 res.send({
		                                    first_login: us.first_login,
		                                    session_token: session.ios_session_token,
		                                    refresh_token: session.refresh_token
		                                });
									});
								}
								
								if(req.body.source == 'android'){
									db.User.find({where:{android_session_token: session.android_session_token}}).then(function(us){
										 res.send({
		                                    first_login: us.first_login,
		                                    session_token: session.android_session_token,
		                                    refresh_token: session.refresh_token
		                                });
									});
								}
                            });

                        }
                        else{
                            var password = utilities.randomString(5);
                            
                            var obj = {};
                            
                            if(req.body.source == 'web'){      
								var obj = {
	                                username: response.email,
	                                email: response.email,
	                                password: utilities.bcrypt.generate(password),
	                                refresh_token: utilities.randomString(128),
	                                session_token: utilities.randomString(128),
	                                is_active: true,
	                                first_login: 1,
	                                timestamp: new Date()
	                            }
                            }
                            
                            if(req.body.source == 'ios'){
	                            var obj = {
	                                username: response.email,
	                                email: response.email,
	                                password: utilities.bcrypt.generate(password),
	                                refresh_token: utilities.randomString(128),
	                                ios_session_token: utilities.randomString(128),
	                                is_active: true,
	                                first_login: 1,
	                                timestamp: new Date()
	                            }
                            }
                            
                             if(req.body.source == 'android'){
	                            var obj = {
	                                username: response.email,
	                                email: response.email,
	                                password: utilities.bcrypt.generate(password),
	                                refresh_token: utilities.randomString(128),
	                                android_session_token: utilities.randomString(128),
	                                is_active: true,
	                                first_login: 1,
	                                timestamp: new Date()
	                            }
                            }
                            
                            db.User.create(obj)
                            .then(function(session) {
                                
                                console.log("starting download google profile image..");
                                
                                utilities.downloadImage(session, response.picture, ".png", function(file){
                                    console.log("download google profile image completed");

                                    session.setInfo(9, file, function(data){});

                                    session.setInfo(6, response.given_name); //name
                                    session.setInfo(7, response.family_name); //surname
                                    session.setInfo(3, response.gender); //gender
                                    
                                    db.usrAutocomplete.addWithName(session.id, response.given_name, response.family_name); 
                                    db.usrAutocomplete.addWithSurname(session.id, response.family_name, response.given_name); 
                                    
                                    global.controllers.permissions.setDefaultPermission(req, res, session.id);
									global.notificator.notify(session.id, "si è appena registrato al sito", 5);
                                    
                                    if(req.body.source == 'web'){
                                   		res.send({
                                            first_login: session.first_login,
                                            session_token: session.session_token,
                                            refresh_token: session.refresh_token
                                        });
                                    }
                                    
                                    if(req.body.source == 'ios'){
                                   		res.send({
                                            first_login: session.first_login,
                                            session_token: session.ios_session_token,
                                            refresh_token: session.refresh_token
                                        });
                                    }
                                    
                                     if(req.body.source == 'android'){
                                   		res.send({
                                            first_login: session.first_login,
                                            session_token: session.android_session_token,
                                            refresh_token: session.refresh_token
                                        });
                                    }

                                });
                            
                            });
                        }

                    });  
                    
                }
            })
            break;
        default:
            errors.sendError(res, errors.codes.invalidCredentials);
            break;
    }
    
}

exports.requestRefreshToken = function(req, res) {
    
    if(typeof req.e_user != "undefined" && typeof req.body.refresh_token != "undefined")
    {
        var refresh_token = req.body.refresh_token;
        
        req.e_user.refresh_token = utilities.randomString(128);
        req.e_user.session_token = utilities.randomString(128);
        req.e_user.timestamp = new Date();
        req.e_user.save().then(function(session){
            res.send({
                first_login: session.first_login,
                session_token: session.session_token,
                refresh_token: session.refresh_token
            });
        });
        
        
    }
    else if( typeof req.user != "undefined" && typeof req.body.refresh_token != "undefined")
    {
        var refresh_token = req.body.refresh_token;
        var username = req.body.username;
        
        req.user.refresh_token = utilities.randomString(128);
        req.user.session_token = utilities.randomString(128);
        req.user.timestamp = new Date();
        req.user.save().then(function(session){
            res.send({
                first_login: session.first_login,
                session_token: session.session_token,
                refresh_token: session.refresh_token
            });
        });
        
    }
    else
    {
        errors.sendError(res, errors.codes.invalidParam);
    }
      
}

exports.checkToken = function(token, platform, callback)
{
	if(platform !== undefined){
	
		if(platform == 'web'){
			db.User.find({ where: {session_token: token, is_active: true }})
	        .then(function(user) {
			
	            if(user != null){
	                
	                user.timestamp = new Date();
	                user.save().then(function(user){
	                    callback(user, false);
	                });
	                
	            }else{
	                
	                db.UserExternal.find({ where: {session_token: token}})
	                .then(function(e_user) {
	
	                    if(e_user != null){
	 
	                        e_user.timestamp = new Date();
	                        e_user.save().then(function(e_user){
	                            callback(e_user, true);
	                        });
	
	                    }
	                    else{
	                        callback(null);
	                    }
	
	                });
	                
	            }

        	});
		}
		
		if(platform == 'ios'){
			db.User.find({ where: {ios_session_token: token, is_active: true }})
	        .then(function(user) {
	
	            if(user != null){
	                
	                user.timestamp = new Date();
	                user.save().then(function(user){
	                    callback(user, false);
	                });
	                
	            }else{
	                
	                db.UserExternal.find({ where: {session_token: token}})
	                .then(function(e_user) {
	
	                    if(e_user != null){
	
	                        e_user.timestamp = new Date();
	                        e_user.save().then(function(e_user){
	                            callback(e_user, true);
	                        });
	
	                    }
	                    else{
	                        callback(null);
	                    }
	
	                });
	                
	            }

        	});

		}
		
		if(platform == 'android'){
			db.User.find({ where: {android_session_token: token, is_active: true }})
	        .then(function(user) {
	
	            if(user != null){
	                
	                user.timestamp = new Date();
	                user.save().then(function(user){
	                    callback(user, false);
	                });
	                
	            }else{
	                
	                db.UserExternal.find({ where: {session_token: token}})
	                .then(function(e_user) {
	
	                    if(e_user != null){
	
	                        e_user.timestamp = new Date();
	                        e_user.save().then(function(e_user){
	                            callback(e_user, true);
	                        });
	
	                    }
	                    else{
	                        callback(null);
	                    }
	
	                });
	                
	            }

        	});

		}
	}
	else{
		 db.User.find({ where: {session_token: token, is_active: true }})
	        .then(function(user) {
	
	            if(user != null){
	                
	                user.timestamp = new Date();
	                user.save().then(function(user){
	                    callback(user, false);
	                });
	                
	            }else{
	                
	                db.UserExternal.find({ where: {session_token: token}})
	                .then(function(e_user) {
	
	                    if(e_user != null){
	
	                        e_user.timestamp = new Date();
	                        e_user.save().then(function(e_user){
	                            callback(e_user, true);
	                        });
	
	                    }
	                    else{
	                        callback(null);
	                    }
	
	                });
	                
	            }

        	});
	}
}

exports.authFilter = function(req, res, next){
  
    var token = req.header('x-session-token');
    var platform = req.header('x-platform');
	
    if(token != null) {
        
        exports.checkToken(token, platform, function(user, external){
            
            if(user != null)
            {
                if(external)
                    req.e_user = user;
                else
                    req.user = user;
                next();
            }
            else
            {
                errors.sendError(res, errors.codes.invalidCredentials);
            }

        });

    }
    else {
        errors.sendError(res, errors.codes.invalidCredentials);
    }

}


exports.authOrNot = function(req, res, next){
     
    var token = req.header('x-session-token');
    var platform = req.header('x-platform');
    
    if(token != null) {
        
        exports.checkToken(token, platform, function(user, external){
            
            if(user != null)
            {
                if(external)
                    req.e_user = user;
                else
                    req.user = user;
                next();
            }
            else
            {
				errors.sendError(res, errors.codes.invalidCredentials);
            }

        });

    }
    else {
    	req.user = null;
        next();
    }

}


exports.logout = function(req, res){
	console.log(req.user.dataValues);
    db.userLoggedOut(req.user.id);
    req.user.session_token = null;
    req.user.refresh_token = null;
    
  
    
    req.user.save().then(function(user){
        res.send("ok");
    })
    
}

exports.activeUser = function(req, res){
    
    if(typeof req.body.activation_code != "undefined")
    {
        db.User.find({ where: {activation_code: req.body.activation_code, is_active: false }})
        .then(function(user) {

            if(user != null){                
                
                user.is_active = true;
                user.refresh_token = utilities.randomString(128);
                user.session_token = utilities.randomString(128);
                user.activation_code = utilities.randomString(128);
                user.timestamp = new Date();
                user.save().then(function(session){
                    res.send({
                        first_login: session.first_login,
                        session_token: session.session_token,
                        refresh_token: session.refresh_token
                    });
                });               
                
            }else{                
                errors.sendError(res, errors.codes.invalidParam, "Activation code not found");                
            }

        });
    }
    else
    {
        errors.sendError(res, errors.codes.invalidParam, "Activation code not found");
    }
    
    
}

exports.recoveryPassword = function(req, res){
	if(req.query.email !== undefined || req.query.email != "" ) {
		 db.User.find({
			 where: {
				 email: req.query.email
			 } 
		 }).then(function(result){
             if(result != null) {
				 global.mailer.templates.recoveryPassword(result.dataValues.email, result.dataValues.username, settings.frontend_url+"#/recovery/"+result.dataValues.activation_code);
				 res.send(result.dataValues);
			 }else {
				 errors.sendError(res, errors.codes.invalidParam, "Email not found");
			 }
		 });
	}
	else {
		 errors.sendError(res, errors.codes.invalidParam, "Recovery email not found"); 
	}
}

exports.checkRecovery = function(req, res){
	if(req.query.code !== undefined || req.query.code != "" ) {
		 db.User.find({
			 where: {
				 activation_code: req.query.code
			 }
		 }).then(function(user){
             
             if(user != null){
                  user.refresh_token = utilities.randomString(128);
				  user.session_token = utilities.randomString(128);
				  user.timestamp = new Date();
				  user.save().then(function(session){
                    db.userLoggedIn(session.id);
                //	  global.notificator.notify(session.id, "si è appena collegato", 2);
                    
                    res.send({
                        first_login: session.first_login,
                        session_token: session.session_token,
                        refresh_token: session.refresh_token
                    });
                });
              }
             else
                 errors.sendError(res, errors.codes.invalidParam, "Reset code not found");
		 });
	}
	else {
		 errors.sendError(res, errors.codes.invalidParam, "Wrong recovery code");  
	}
}

exports.resetPassword = function(req, res){
	if(req.query.code !== undefined || req.query.code != "" ) {
		 db.User.find({
			 where: {
				 activation_code: req.body.code
			 } 
		 }).then(function(result){
             
             if(result != null)
             {
                 result.password = utilities.bcrypt.generate(req.body.password);
                 result.activation_code = utilities.randomString(128);
                 result.save().then(function(user){
                    res.send("ok");
                 });
             }
             else
             {
                 errors.sendError(res, errors.codes.invalidParam, "Wrong recovery code");
             }
                   
		 });
	}
	else {
		 errors.sendError(res, errors.codes.invalidParam, "Wrong recovery code");  
	}
}

exports.checkParamToken = function(req, res, next){
	if(req.query.token = '' || req.query.token === undefined)
		 errors.sendError(res, errors.codes.invalidParam, "Wrong recovery code");  
	else{
		next();
	}
}

exports.callbackCheckToken = function(req, res){

	db.User.find({where: {session_token: req.query.token}})
		.then(function(user){
			console.log(user)
			if(user != null){
				res.send({token_exists: true});
			}
			else{
				res.send({token_exists: false});
			}
		});
}