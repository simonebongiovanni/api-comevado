var autenticationController = require("./class");
global.filters.auth = autenticationController.authFilter;
global.filters.authOrNot = autenticationController.authOrNot;
global.controllers.autentication = autenticationController;


/** @autentication */
module.exports = function(app)
{
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
    app.post("/login", autenticationController.login);  
    app.get("/refreshtoken", global.filters.auth, autenticationController.requestRefreshToken);
    app.get("/firstlogged", global.filters.auth, autenticationController.firstLogged);
    app.get("/logout", global.filters.auth, autenticationController.logout);
    
	app.post('/user/activate', autenticationController.activeUser);
	
	/*
	*	Send recovery link email
	*
	*	@param {String}  email - user mail
	*
	*/
	app.get('/user/recovery', autenticationController.recoveryPassword);
	
	/*
	*	Check recovery code
	*
	*	@param {String}  code - recovery user code
	*
	*/
	app.get('/user/checkrecovery', autenticationController.checkRecovery);
    
    /*
	*	Recovery password
	*
	*	@param {String}  code - recovery user code
	*	@param {String}  password - new password
	*
	*/
	app.post('/user/resetpassword', autenticationController.resetPassword);
	
	
	/**
	*
	*	Check token existent
	*
	*	@pa
	*/
	app.get('/user/checktocken', autenticationController.checkParamToken, autenticationController.callbackCheckToken)
}