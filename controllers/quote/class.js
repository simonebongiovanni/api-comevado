var db = global.db;
var settings = global.settings;

/**
 * Quota Controller
 */

exports.checkUserQuota = function(quota, callback){
        
    if(quota.user_type == 1) //internal user
    {
        if(quota.value <= settings.quote.guest)
            callback(true);
        else
            callback(false);
    }
    else if(quota.user_type == 2) //external user
    {
        db.ExternalUser.find({where: {id: quota.id_user}}).then(function(e_user){
            if(quota.value <= e_user.credits)
                callback(true);
            else
                callback(false);
        })
        
    }
    else
    {
        callback(false);
    }
    
}

exports.checkGuestQuota = function(quota, callback){
        
    if(quota.value <= settings.quote.internal)
        callback(true);
    else
        callback(false);
    
}

exports.incUserQuota = function(id_user, user_type, callback){
    db.ClientQuote.find({where: {id_user: id_user}}).then(function(quota){
        if(quota != null)
        {
            quota.value = quota.value + 1;
            quota.save().then(function(quota){
                callback(quota)
            })
        }
        else
        {
            db.ClientQuote.create({ 
                id_user: id_user,
                user_type: user_type,
                value: 1
            })
            .then(function(quota) {
                callback(quota)
            });
        }

    })
}

exports.incGuestQuota = function(ip, callback){
            
    var ip_array = ip.split(".");

    db.ClientQuote.find({where: {ip1: ip_array[0], ip2: ip_array[1], ip3: ip_array[2], ip4: ip_array[3]}}).then(function(quota){

        if(quota != null)
        {
            quota.value = quota.value + 1;
            quota.save().then(function(quota){
                callback(quota)
            })
        }
        else
        {
            db.ClientQuote.create({ 
                user_type: 0,
                ip1: ip_array[0], 
                ip2: ip_array[1], 
                ip3: ip_array[2],
                ip4: ip_array[3],
                value: 1
            })
            .then(function(quota) {
                callback(quota)
            });
        }

    })
} 


exports.quoteFilter = function(req, res, next){
    
    if(typeof req.user != "undefined")
    {
        quoteController.incUserQuota(req.user.ID, 1, function(quota){
            quoteController.checkUserQuota(quota, function(status){
                if(status)
                    next();
                else
                    errors.sendError(res, errors.codes.quotaExceeded);
            })
        });
        
    }
    else if(typeof req.external_user != "undefined")
    {
        quoteController.incUserQuota(req.external_user.id, 2, function(quota){
            quoteController.checkUserQuota(quota, function(status){
                if(status)
                    next();
                else
                    errors.sendError(res, errors.codes.quotaExceeded);
            })
        });
        
    }
    else
    {
        var ip = req.connection.remoteAddress;
        quoteController.incGuestQuota(ip, function(quota){
            quoteController.checkGuestQuota(quota, function(status){
                if(status)
                    next();
                else
                    errors.sendError(res, errors.codes.quotaExceeded);
            })
        });
        
    }  
}