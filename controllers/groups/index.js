var extend = require('./class.js');
var filters = global.filters;

module.exports = function(app) {
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {post} /group/listtype Retrive group type
	*
	*   @apiName GetGroupTypeList
	*	@apiGroup Group
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive group type list
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/group/listtype
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	  [{"id":1,"description":"lavoro"},{"id":2,"description":"vacanza"},{"id":3,"description":"altro"}]
	*
	*/
	app.post('/group/listtype', extend.getGroupType);
	
	/**
	*
	*   @api {get} /group/:idgroup/info Retrive group info
	*
	*   @apiName GetGroupInfo
	*	@apiGroup Group
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive group info
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/group/1/info
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idgroup The id group for getting info. It return user member list and status of registration group
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    {"id":1,"type":1,"description":"Grouppo viaggio buckinghamshire ostend","id_admin":1,"data_create":"2015-12-21T10:50:21.000Z","from":"buckinghamshire","to":"ostend","groups_member":[{"id":2,"is_confirmed":false,"is_refused":false}]}
	*
	*
	*/
	app.get('/group/:idgroup/info', filters.auth, extend.checkParamsGroup, extend.callbackParamsGroup)
	
		/**
	*
	*   @api {get} /group/create Create group
	*
	*   @apiName CreateGroup
	*	@apiGroup Group
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method create new group
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/group/create
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	
	*   @apiParam {String} from The from locality
	*	@apiParam {String} to The to locality
	*	@apiParam {number} id_travel The id of the travel
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    TO DO
	*
	*
	*/
	app.post('/group/create', filters.auth, extend.checkGroupInfo, extend.createGroup);
	
	/**
	*
	*   @api {get} /group/:idgroup/join/:iduser Join user to group
	*
	*   @apiName JoinUserGroup
	*	@apiGroup Group
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method join user to group
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/group/1/join/1
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idgroup The id group.
	*   @apiParam {number} iduser The id user.
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	TO DO
	*	
	*/
	app.get('/group/:idgroup/join/:iduser', filters.auth, extend.checkParamsGroup, extend.checkParamUserGroup, extend.sendRequestGroup);
	
	/**
	*
	*   @api {get} /group/:idgroup/acceptrequest Accept user group request
	*
	*   @apiName AcceptUserGroup
	*	@apiGroup Group
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method accept user request group
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/group/1/acceptrequest
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idgroup The id group.
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	TO DO
	*	
	*/
	app.get('/group/:idgroup/acceptrequest', filters.auth, extend.checkParamsGroup, extend.joinGroup);
	
	/**
	*
	*   @api {get} /group/:idgroup/refuserequest Refuse user group request
	*
	*   @apiName RefuseUserGroup
	*	@apiGroup Group
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method refuse user request group
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/group/1/refuserequest
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idgroup The id group.
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	TO DO
	*	
	*/
	app.get('/group/:idgroup/refuserequest', filters.auth, extend.checkParamsGroup, extend.callbackUnjoin);
	
	/**
	*
	*   @api {get} /group/find Find group from id
	*
	*   @apiName RetriveGroupFromID
	*	@apiGroup Group
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive group from id group
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/group/find
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {number} idgroup The id group.
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	{"group":{"id":1,"id_grouptype":1,"description":"Grouppo viaggio buckinghamshire ostend","is_public":1,"id_usercreated":1,"datacreation":"2015-12-21T10:50:21.000Z","is_delete":0,"from_city":"buckinghamshire","to_city":"ostend","id_travel":2,"id_group":1,"id_travel_group_type":1}}	
	*	
	*/
    app.post('/group/find',filters.auth, extend.checkFindParams, extend.callbackFind);
} 