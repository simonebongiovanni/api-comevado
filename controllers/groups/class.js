db = global.db;
errors = global.errors;
var moment = require('moment');

/*
 * getGroupType
 * This method will get the description and the id of the group
 * @param  {Object} req, res
 */
exports.getGroupType = function (req, res) {
	db.GroupType.findAll().then(function(data){
		res.send(data);
	});
};


exports.checkGroupInfo = function(req, res, next) {
	if(req.body.from === undefined || req.body.to === undefined || req.body.from == '' || req.body.to == '' || req.body.id_travel === undefined || req.body.id_travel == '' || isNaN(req.body.id_travel) || req.body.id_travel < 0) 
		errors.sendError(res, 400, errors.invalidParam);
	else 
		next();
};

exports.checkParamsGroup = function(req, res, next){	
	if(req.params.idgroup === undefined || req.params.idgroup == '' || isNaN(req.params.idgroup) || req.params.idgroup <= 0)
		errors.sendError(res, 400, errors.invalidParam);
	else
		next();
	
};

exports.checkParamUserGroup = function(req, res, next){
	if(req.params.iduser === undefined || req.params.iduser == '' || isNaN(req.params.iduser) || req.params.iduser <= 0)
		errors.sendError(res, 400, errors.invalidParam);
	else{
		db.User.find({where: {id: req.params.iduser}})
			   .then(function(user){
				   if(user != null)
				   		next();
				   	else 
				   		errors.sendError(res, 400, errors.invalidParam);
			   });
	}
		
}

/* 
 * This method will create a new group
 */
exports.createGroup = function (req, res){
	var name = 'Grouppo viaggio ' + req.body.from + ' ' + req.body.to;
	
	db.Groups.create({
		id_grouptype: 1,
		description: name,
		is_public: 1,
		id_usercreated: req.user.id,
		datacreation: db.sequelize.fn('NOW'),
		from_city: req.body.from,
		to_city: req.body.to 
	}).then(function(group){
		//enterGroup(idUser, group.id);
		db.ActivityStream.create({
			id_typeactivity: 15,
			id_author: req.user.id,
			data: db.sequelize.fn('NOW')
		}).then(function(activity){
			db.GroupsActivity.create({
				id_groups: group.id,
				id_activity: activity.id
			}).then(function(){
				db.TravelGroup.create({
					id_group: group.id,
					id_travel: parseInt(req.body.id_travel),
					id_travel_group_type: 1
				}).then(function(tg){
					res.send(group);
				});
			});
		});	
	});
}
	
/*
 * enterGroup
 * Function for Groups that add the user to the group
 * @param {INT} - userId - The id of the user
 * @param {INT} - groupId - the id of the group
 */
function enterGroup(userId, groupId) {
	db.GroupUser.create({
		id_user: userId,
		id_group: groupId
	});
}

exports.sendRequestGroup = function (req, res) {
	db.Groups.find({where : {id: req.params.idgroup}, 
					include: [{
						model: db.GroupUser
					}]})
			.then(function(group){
			
				if(group != null){
					db.GroupUser.find({where: {id_user: req.user.id, id_group: group.id}})
								.then(function(presence) {
								     if(presence == null)
									 {
										 db.GroupUser.create({
											id_user: req.params.iduser,
											id_group: req.params.idgroup,
											is_confirmed: 0,
											is_refused: 0,
											is_delete: 0
										}).then(function(groupUser){
												db.ActivityStream.create({ 
													id_typeactivity: 28,
													id_author: req.user.id,
													data: db.sequelize.fn('NOW')
												}).then(function(activity){
													db.GroupsActivity.create({
														id_groups: req.params.idgroup,
														id_activity: activity.id
													});
													
													global.notificator.notify(req.user.id, 'ti ha invitato al gruppo', 8, req.params.iduser, {
									                    from: group.from_city,
									                    to: group.to_city,
									                    id: group.id,
									                    activity: activity.id
									               });
												});
																				
												res.send({send_request_status: "OK"});
											});			
									}
									else {
										errors.sendError(res, 400, errors.invalidParam, 'user is alredy in group');
									}});
				}
				else{
					errors.sendError(res, 400, errors.invalidParam, 'group not exists');
				}
			});
}

/*
 * unjoinGroup
 * This method will delete a user from a group or, if is an administrator, will delete the group
 * @param {INTEGER} - req.body.groupID - The ID of the group
 */
exports.callbackUnjoin = function (req, res) {

    
    	db.GroupUser.update({is_confirmed: 0, is_refused: 1}, {where : {id_group: req.params.idgroup, id_user: req.user.id}})
			.then(function(gp){
				db.ActivityStream.create({
					id_typeactivity: 37,
					id_author: req.user.id,
					data: db.sequelize.fn('NOW')
				}).then(function(activity){
					db.GroupsActivity.create({
						id_groups: req.params.idgroup,
						id_activity: activity.id
					});
				});
				
				res.send({confirm_status: 'OK'});
			}); 
    
}

exports.callbackParamsGroup = function(req, res){
	db.Groups.find({where : {id: req.params.idgroup}, 
					include: [{
						model: db.GroupUser
					}]})
			.then(function(group){				
				if(group != null){
					var users = [];
					index = 0;
				
					paramGroupSeries(index, group, res, users);

				}else{
					errors.sendError(res, 400, errors.invalidParam);
				}
			});
};

function paramGroupSeries(index, group, res, users){
	
	
	if(index < group.groupUsers.length){
			db.UserInfo.find({where: {id_user: group.groupUsers[index].id_user}})
									.then(function(us){
										
										users.push({id: group.groupUsers[index].id_user, is_confirmed:  group.groupUsers[index].is_confirmed, is_deleted:  group.groupUsers[index].is_deleted, is_refused: group.groupUsers[index].is_refused});	
										index++;
										
										paramGroupSeries(index, group, res, users);
									});
	}else {
		
		
		var response = {
			id: group.id,
			type: group.id_grouptype,
			description: group.description,
			id_admin: parseInt(group.id_usercreated),
			data_create: group.datacreation,
			from: group.from_city,
			to: group.to_city,
			groups_member: users  
		};
		
		res.send(response); 
	}
}

exports.joinGroup = function(req, res){
	db.GroupUser.update({is_confirmed: 1}, {where : {id_group: req.params.idgroup, id_user: req.user.id, is_confirmed: 0, is_refused: 0}})
				.then(function(gp){
					db.ActivityStream.create({
						id_typeactivity: 36,
						id_author: req.user.id,
						data: db.sequelize.fn('NOW')
					}).then(function(activity){
						db.GroupsActivity.create({
							id_groups: req.params.idgroup,
							id_activity: activity.id
						});
					});
					
					res.send({confirm_status: 'OK'});
				}); 
}

exports.checkFindParams = function(req, res, next){
	if(req.body.id_travel === undefined || req.body.id_travel == '' || isNaN(req.body.id_travel) || req.body.id_travel < 0)
		errors.sendError(res, 400, errors.invalidParam);
	else
		next();
}

exports.callbackFind = function(req, res){
	dat = moment(req.body.data).format("YYYY-MM-DD");
	
	db.sequelize.query("SELECT * FROM groups JOIN travel_group ON groups.id = travel_group.id_group WHERE id_travel = :idtravel", { type: db.sequelize.QueryTypes.SELECT, replacements: {idtravel: req.body.id_travel}})
				.then(function(group){
					if(group.length > 0)
						res.send({group: group[0]});
					else{
						res.send({group: ''});
					}
				});
	
	
	/*db.sequelize.query("SELECT * FROM groups WHERE datacreation BETWEEN '" + dat + " 00:00:00' AND '" +  dat +" 23:59:59' AND from_city = :from AND to_city = :to AND is_delete = 0 AND id_usercreated = " + req.user.id + " ORDER BY datacreation DESC LIMIT 1", { type: db.sequelize.QueryTypes.SELECT, replacements: {from: req.body.from, to: req.body.to}}).then(function(group){
		if(group.length == 0)
			res.send({group: ''});
		else
			res.send({group: group[0]});
		
	});*/
}