var errors = global.errors;
var utilities = require('../../utilities.js');
var settings = global.settings.external_service.google;

/**
 * checkParam
 * This method will check the params if they are correct or not
 * @param  {Object} req
 */
exports.checkParam = function (req, res, next) {
    if ((req.body.lat.length == 0) || (req.body.lng.length == 0) || (req.body.localString.length == 0)) {
        errors.sendError(res, errors.codes.invalidParam);
    } else {
        if (isNaN(req.body.lat) || (isNaN(req.body.lng))) {
            errors.sendError(res, errors.codes.invalidParam);
        }
        next();
    }
};

/**
 * Autocomplete Controller.
 * Give the AutoComplete Query results
 * @param {string} req.body.localString - localString
 * @param {string} req.body.lat - lat
 * @param {string} req.body.lng - lng
 */
exports.getAll = function (req, res) {

    var str = req.body.localString.trim();

    if (str != "") {
        
        global.db_autocomplete.getAutocomplete(str, req.body.lat, req.body.lng, function (autocomplete) {
            data = autocomplete;
            res.send(data);
        });
        
    } else {
        errors.sendError(res, errors.codes.invalidParam);
    }

}
module.exports.checkP = function(req, res, next){
	if(req.query.location === undefined || req.query.location.length < 2 || req.query.location == '' || req.query.lang == '' || req.query.lang === undefined){
		 errors.sendError(res, errors.codes.invalidParam);
	}
	else{
		next();
	}
}

module.exports.getResult = function(req, res){
	var host = 'maps.googleapis.com';
	var path = '/maps/api/place/autocomplete/json?input=' + encodeURI(req.query.location) + '&types=geocode&language=' + req.query.lang + '&key=' + settings.client_key;
	var suggests = [];
	
	utilities.https.get(host, path, function(status, result){
		k = JSON.parse(result);
		k = k.predictions;
		
		for(i = 0; i < k.length; i++){
			console.log(k[i].description)
			suggests.push({asciiname_extended: k[i].description});
		}
		
		res.send({suggests: suggests});
	})
}