var extend = require('./class.js')

module.exports = function(app) {
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {post} /autocomplete Retrive locality for user not logged
	*
	*   @apiName RetriveLocalityNotLoggedUser
	*	@apiGroup Locality
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive locality for not logged user
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/autocomplete
	*
	*   @apiParam {String} localString The name of the place.
	*	@apiParam {Float} lat The latitude of user location
	*   @apiParam {Float} lng The longitude of user location
	*
	*/
	app.post('/autocomplete', extend.checkParam, extend.getAll);
	
	
	/**
	*
	*   @api {get} /autocomplete/logged Retrive locality for user logged
	*
	*   @apiName RetriveLocationForUserLogged
	*	@apiGroup Locality
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive locality for user logged
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/autocomplete/logged?location=Mil&lang=IT
	*	
	*
	*   @apiParam {String} location The location name to search
	*	@apiParam {String} lang The lang of the autocomplete
	*
	*/
	app.get('/autocomplete/logged', extend.checkP, extend.getResult);
}

