var google = require('./class.js');

module.exports = function(app) {
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	/**
	*
	*   @api {post} getDirection Retrive google path
	*
	*   @apiName GetGooglePath
	*	@apiGroup Routing
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive path beetween two location
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/getDirection
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {String} from The from location
	*   @apiParam {String} to The to location
	*	@apiParam {String} type The response type(XML/JSON)
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*   TO DO
	*
	*
	*/
	app.post('/getDirection', google.checkParam, google.getCarDirection);
	
	/**
	*
	*   @api {get} /location/coordinates Retrive location coordinates
	*
	*   @apiName GetLocationCoordinates
	*	@apiGroup Locality
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive google coordinate location
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/location/coordinates
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {String} location The name of location
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*    {"coordiantes":{"lat":45.4654219,"lng":9.1859243},"asciiname_extended":"Milano"}
	*
	*
	*/
	app.get('/location/coordinates', google.checkLoc, google.replaceWhiteSpace, google.cbLoc);

}