var settings = global.settings.external_service.google;
var googleUtility = require('../../utilities.js');

/**
 * checkParam
 * This method will check the params if they are correct or not
 * @param  {Object} req
 * The checked param would be:
 * @param {float} - req.body.from - From places
 * @param {float} - req.body.to - To places
 * @param {string} - req.body.type - Type of the Google API. It must be 'json' or 'xml'
 */
exports.checkParam = function(req, res, next) {
	if(!(typeof req.body.from === 'undefined' || typeof req.body.to === 'undefined' || typeof req.body.type === 'undefined')){
		res.status(400).send('Invalid Params');
	}
	else {
		if((req.body.from.length == 0) || (req.body.to.length == 0) || (req.body.type.length == 0)) {
			res.status(400).send('Invalid Params');
		}
		else {
				if((req.body.type.toLowerCase()!="json") && (req.body.type.toLowerCase()!="xml")) {
					res.status(400).send('Invalid Params');
				}
				else {
					next();
				}
			}	
	}
}		

/**
 * getCarDirection - Get Google CAR direction and his path
 * @param {object} - req
 * @param {object} - res
 * 
 * In req we must have:
 * from - String of the from place
 * to - String of the to place
 * type - String that identifies the kind of API return (JSON OR XML)
 */
exports.getCarDirection = function(req, res) {
	var from = '';
	var to = '';
	var type = '';
	var googleKey = '';
	var host = '';
	var path = '';

	
	from = req.body.from;
	to = req.body.to;
	type = req.body.type;
	googleKey = settings.client_key;
	host = 'maps.googleapis.com';
	path = '/maps/api/directions/'+type+'?origin='+from+'&destination='+to+'&key='+googleKey;
	
	googleUtility.https.get(host, path, function(flag, data){
		if((flag==true)&&(googleUtility.isJSON(data)==true)){
			data = JSON.parse(data);
			if(data.status!="ZERO_RESULTS") {
				n = data.routes[0].legs[0].steps.length;
				for(i=0; i<n; i++) {
					data.routes[0].legs[0].steps[i].polyline.points = googleUtility.decodePolyline(data.routes[0].legs[0].steps[i].polyline.points);
				}
				data = JSON.stringify(data);
				res.send(data);	
			}
			else {
				global.errors.sendError(res, global.errors.codes.externalError, data);
			}
		}
		else {
			global.errors.sendError(res, global.errors.codes.externalError, data);
		}
	});
	
};

module.exports.checkLoc = function(req, res, next){
	if(req.query.location === undefined || req.query.location == ''){
		res.status(400).send('Invalid Params');
	}
	else
		next();
}

module.exports.replaceWhiteSpace = function(req, res, next){
	req.query.location = encodeURI(req.query.location);
	next();
}

module.exports.cbLoc = function(req, res){	
	
	var host = "maps.googleapis.com";
	var path = "/maps/api/geocode/json?address=" + req.query.location + "&key=" + settings.client_key;
	
	googleUtility.https.get(host, path, function(status, data){
		k = JSON.parse(data);

		res.send({coordiantes: {lat: k.results[0].geometry.location.lat, lng: k.results[0].geometry.location.lng}, asciiname_extended: decodeURI(req.query.location)})
	});
}
