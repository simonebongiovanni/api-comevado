var utilities = require('../../utilities.js');
var db = global.db;
var errors = global.errors;
var code_erros = global.errors.codes;
var settings = global.settings;
var utilities = global.utilities;
var request = require('request');

/**

Get all

*/

module.exports.callbackGetAllUser = function(req, res){
    
    db.User.find().then(function(users){
        
        var data = [];        
        users.forEach(function(user){
            data.push({
                id: user.id,
                username: user.username,
                email: user.email
            });
        })
        res.send(data);
        
    }).catch(function(err){errors.sendError(res, code_erros.internalError)})
    
}


/**

User registration

*/

module.exports.callbackNew = function(req, res) {
	// da verificare come genereare il  token di autenticazione per cookie
	db.insertUser(req, function(id_inserted){
        
        db.User.find({where:{id: id_inserted}}).then(function(user){
            emailName = req.body.name + " " + req.body.surname
            
            if(req.body.name === undefined || req.body.name == '')
                db.usrAutocomplete.addWithName(id_inserted, req.body.name);
            if(req.body.surname === undefined || req.body.surname == '')
                db.usrAutocomplete.addWithSurname(id_inserted, req.body.surname);
           
          global.mailer.templates.wellcome(user.dataValues.email, emailName, user.dataValues.username, settings.frontend_url+"#/activation/"+user.dataValues.activation_code);
          
          db.usrAutocomplete.addWithName(user.id, req.body.name, req.body.surname);
          db.usrAutocomplete.addWithSurname(user.id, req.body.surname, req.body.name); 
          
          global.controllers.permissions.setDefaultPermission(req, res, id_inserted);
          global.notificator.notify(user.id, "si è appena registrato al sito", 5);
            
          res.send({status: 'OK'});
            
        })
    });
}

module.exports.findUser = function(req, res, next){
	
	
	db.User.find({where:{id: req.params.userid}}).then(function(user){        
        if(user != null)
           next();
        else
            errors.sendError(res, code_erros.invalidParam, "User not found");              
    });
}

module.exports.checkParamNewUser = function(req, res, next) {
	var error = false;
	
	if(req.body.name === undefined || req.body.name == '')
		error = true;
	
	if(req.body.surname === undefined || req.body.surname == '')
		error = true;
	
	if(req.body.username === undefined || req.body.username == '')
		error = true;
	
	if(req.body.email === undefined || req.body.email == '' || req.body.email.indexOf('@') == -1 || req.body.email.indexOf('.') == -1) 
		error = true;
		
	if(req.body.password === undefined || req.body.password == '')
		error = true;
		
	if(error)
    {
        errors.sendError(res, code_erros.invalidParam);
    }		
	else {
        
        db.User.find({where:{email: req.body.email}}).then(function(user){        
            if(user == null)
               next();
            else
                errors.sendError(res, code_erros.invalidParam, "email-duplicated");              
        });
        
		
	}
}

module.exports.checkActivationAccount = function(req, res, next) {
	if(req.body.activation_code === undefined || req.body.activation_code == '')
		errors.sendError(res, code_erros.invalidParam);
	else
		next();
}


/**
	User retrive info
*/
module.exports.callbackUpdateInfo = function(req, res) {
		
		var entry = [];
	var index = 0;
	
	for(el in req.body) {
		if(el != 'user')
			entry.push({key: el,value: req.body[el]});
        
        if(el == 'name')
            db.usrAutocomplete.updateWithName(req.user.id, req.body[el]);
        if(el == 'surname')
            db.usrAutocomplete.updateWithSurname(req.user.id, req.body[el]);
        
	}
	
	slideArray(entry, index, req, res);
}

function slideArray(entry, index, req, res) {
	if(index < entry.length) {
		console.log(index);
		
		db.UserInfo.find({
			where:{
				id_user: req.user.id	
			},
			include: [{model: db.UserInfoType, where: {field: entry[index].key}}]
			}).then(function(info){		
				console.log(info)
				
				if(info != null){
			
					info.value = entry[index].value;
					info.save();
					
					index++;
					slideArray(entry, index, req, res);
					}
				else {
		
					db.UserInfoType
						.findOne({where: {field: entry[index].key}})
						.then(function(infotype){
							db.UserInfo.create({
								id_user: req.user.id,
								value: entry[index].value,
								id_type: infotype.id
							});
							
							index++;
							slideArray(entry, index, req, res);
						});
				}	
			});
	}
	else
		res.send({update_status: 'OK'});
}

module.exports.checkParamUpdateInfo = function(req, res, next) {
	db.checkParamAddition(req, res, next);
}

module.exports.checkIfUserSet = function(req, res, next) {
	var error = false;

	if(req.params.userid === undefined || req.params.userid == '')
		error = true;
		
	if(error)
    {
        errors.sendError(res, code_erros.invalidParam);
    }		
	else {
		if(isNaN(req.params.userid)) {
			if(req.params.userid != 'me')
				errors.sendError(res, code_erros.invalidParam);
			else {
				req.params.userid = req.body.id
			}
		}
		else {
			if(req.params.userid <= 0)
				errors.sendError(res, code_erros.invalidParam);
			else 
				next();
		}
	}
}

module.exports.callbackLogin = function(req, res) {
	db.loginUser(req, res);
}

/**
	
	User detail
	
*/
module.exports.userDetails = function(req, res){
    
    if(req.params.userid == "me")
	   req.params.userid = req.user.id;
    
    db.User.find({where: {id: req.params.userid}}).then(function(user){
        
        if(user != null)
        {
            db.getUserInfo(req.params.userid, function(data){
                res.send(data);
            });
        }
        else
        {
            errors.sendError(res, code_erros.invalidParam, "user not found");
        }
        
    })
    
}

/**
	
	User address
	
*/
module.exports.checkParamNewAddress = function(req, res, next) {		
	if(req.body.addressname === undefined || req.body.addressname == '' || req.body.addressvalue === undefined || req.body.addressvalue == '')
		errors.sendError(res, code_erros.invalidParam);
	else
		next();
}
 
module.exports.callbackInsertnewAddress = function(req, res) {
	db.insertNewAddress(req, res);
}

module.exports.checkAddressId = function(req, res, next) {
	if(req.body.address_id === undefined || req.body.address_id == '')
		errors.sendError(res, code_erros.invalidParam);
	else
		next();
}

module.exports.checkUpdateParam = function(req, res, next) {
	if(req.body.address_id === undefined || req.body.address_id == '' || req.body.address_value === undefined || req.body.address_value == '')
		errors.sendError(res, code_erros.invalidParam);
	else
		next();
}

module.exports.callbackDeleteAddress = function(req, res) { 
	db.deleteAddress(req, res);
}

module.exports.callbackUpdateAddress = function(req, res) {
	db.updateAddress(req, res);
}

/*
	User car	
	
*/

module.exports.checkAllestimentId = function(req, res, next) {
	if(req.body.allestiment === undefined || req.body.allestiment == '' || req.body.allestiment < 0 || isNaN(req.body.allestiment))
		errors.sendError(res, code_erros.invalidParam);
	else
		next();
}

module.exports.callbackInsertCar = function(req, res) {
	db.addUserCar(req, res);
}

module.exports.callbackDeleteCar = function(req, res) {
	db.removeCar(req, res);
}

/*
	
	Friendship status	
	
*/

module.exports.checkFriendshipParam = function(req, res, next){
	if(req.params.frienda === undefined || isNaN(req.params.frienda) || req.params.frienda <= 0 || req.params.friendb === undefined || isNaN(req.params.friendb) || req.params.friendb <= 0)
		errors.sendError(res, code_erros.invalidParam);
	else
		next();
}

module.exports.callbackFriendshipStatus = function(req, res, next){
	db.UserRelation.find(
		{
			where: {
				$or:
					 [
					 	{id_user1: req.params.frienda, id_user2: req.params.friendb},
					 	{id_user2: req.params.frienda, id_user1: req.params.friendb}
					 ]
			},
			order: [['id', 'DESC']]
		}).then(function(relation){
		if(relation != null){
			res.send(relation);
		}
		else {
			res.send({});
		}
	})
}


module.exports.validateString = function(req, res, next) {
	var error = false;

	for(k in req.body){
		if(!utilities.checkString.checkInvalidChar(req.body[k])) {
			error = true;
		}			
	}
	
	if(utilities.checkString.checkSpace(req.body.email) || utilities.checkString.checkSpace(req.body.password))
		error = true;
	
	if(!utilities.checkString.checkSnail(req.body.email) || !utilities.checkString.checkPoint(req.body.email)) 
		error = true;
	
	if(!utilities.checkString.checkNumber(req.body.password) || req.body.password.length < 8)
		error = true;

	if(error) 
		errors.sendError(res, code_erros.invalidParam);
	else
		next();
}

module.exports.validateUserString = function(req, res, next){
	for(k in req.body){
		
		req.body[k] = utilities.checkString.replaceInvalidChar(req.body[k]);
	}
	
	next();
}

module.exports.validateAddress = function(req, res, next){
	error = false; 
	
	for(k in req.body){
		if(!utilities.checkString.checkInvalidChar(req.body[k]))
			error = true;
		
		req.body[k] = utilities.checkString.replaceInvalidChar(req.body[k]);
	}
	
	if(error)  
		errors.sendError(res, code_erros.invalidParam);
	else
		next();
};

module.exports.checkUserInviter = function(req, res, next){ 
	
	if(req.body.to === undefined || req.body.to == '')
		errors.sendError(res, code_erros.invalidParam);
	else
		next();
		
}

module.exports.callbackSendInvite = function(req, res){
	db.UserInfo.find({where: {
		id_user: req.user.id,
		id_type: 6
	}}).then(function(item){
		global.mailer.templates.inviteFriend(req.body.to, item.value, 'http://demo.comevado.com/#/register');
			res.send('ok');
	})
};

module.exports.checkFacebookCod = function(req, res, next){
	if(req.query.facebook_id === undefined || req.query.facebook_id =='')
		errors.sendError(res, code_erros.invalidParam);
	else
		next();
};

module.exports.callbackFacebookCod = function(req, res){

	db.UserInfo.find({where:{id_type: 10, value: req.query.facebook_id}}).then(function(user){
		if(user != null){
			db.User.find({where: {
				id: user.id_user
			}}).then(function(item){
				if(item != null){
					db.getUserInfo(item.id, function(user){
						db.UserRelation.find({where:{
							id_user1: req.user.id,
							id_user2: user.user_id,
							is_deleted: 0,
							is_confirmed: 0
						}}).then(function(rel){
							if(rel != null){
								user.friendship = 'pending';
								user.id_relation = rel.id;
							}
							else
								user.friendship = 'none';
								
							res.send({user: user});
						})
					});
				}
				else
					res.send({}); 
			});
		}
	});
};

module.exports.checkParamEmail = function(req, res, next){
	if(req.query.email === undefined || req.query.email == '' || !utilities.checkString.checkSnail(req.query.email) || !utilities.checkString.checkPoint(req.query.email)){
		errors.sendError(res, code_erros.invalidParam);
	}
	else
		next();
};

module.exports.callbackFindMail = function(req, res){
	db.User.find({where: {
		email: req.query.email
	}}).then(function(item){
		if(item != null){
			db.getUserInfo(item.id, function(user){
				db.UserRelation.find({where:{
					id_user1: req.user.id,
					id_user2: user.user_id,
					is_deleted: 0,
					is_confirmed: 0
				}}).then(function(rel){
					if(rel != null){
						user.friendship = 'pending';
						user.id_relation = rel.id;
					}
					else
						user.friendship = 'none';
						
					res.send({user: user});
				})
			});
		}
		else
			res.send({}); 
	});
};

module.exports.checkUsername = function(req, res, next){
	if(req.query.uname === undefined || req.query.uname == ''){
		errors.sendError(res, code_erros.invalidParam);
	}
	else 
		next();
};

module.exports.callbackUname = function(req, res){
	db.User.find({where :{ $or: [{username: req.query.uname} , {email: req.query.uname}]}}).then(function(user){
		if(user != null)
			res.send({user: user.id});
		else
			res.send({});
	});
}

module.exports.callbackNewPassword = function(req, res){
	crypt = utilities.bcrypt.generate(req.body.password);
	
	db.User.find({where: {id: req.user.id}}).then(function(user){
		if(user != null){
			user.password = crypt;
			
			db.ActivityStream.emit(40, req.user.id, function(activity){
				user.save();
				res.send(activity); 
			});
		}
	});
}

module.exports.checkParamsPs = function(req, res, next){
	
	if(!utilities.checkString.checkUpper(req.body.password) || !utilities.checkString.checkNumber(req.body.password) || req.body.password.length < 8)
		errors.sendError(res, code_erros.invalidParam);
	else
		next();

}

module.exports.checkParamsAssitence = function(req, res, next){
	if(req.body.data === undefined  || req.body.data == '' || req.body.data.recaptcha_response === undefined)
		errors.sendError(res, code_erros.invalidParam);
	else
		next();
}

module.exports.checkCaptcha = function(req ,res, next){
	
	var data_s = {
		secret: '',
		response: req.body.data.recaptcha_response
	}
	
	utilities.https.get('www.google.com', '/recaptcha/api/siteverify?secret=6LeAkRETAAAAAPBTIizgf3HjRPD2BE72lfrsx5DN&response=' + req.body.data.recaptcha_response ,function(status, data){
		data = JSON.parse(data);
		console.log(data.success);
		if(data.success == true){
			next();
		}
		else
			errors.sendError(res, code_erros.invalidParam);
	});
	
}

module.exports.callbackSendMailAssistence = function(req, res){
	req.body.data.from = req.body.data.from.trim();
	if(req.body.data.from.indexOf('@') !== -1 && req.body.data.from.indexOf(".") !== -1){
		var message = {
	       text: req.body.data.content.trim(),
	       from: req.body.data.from, 
	       to: 'simonebongiovanni@comevado.com',
	       subject: req.body.data.argument.trim()
	    };
		
		global.sendMessage(message);
		res.send('ok');	
	}
	else{
		errors.sendError(res, code_erros.invalidParam);
	}
}
 