var controllers = require('./class.js')
var filters = global.filters;
global.controllers.user = controllers;

module.exports = function(app) {
	
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {get} /user/list Get all user
	*
	*   @apiName GetAllUser
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive all user site list
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/activity/me?offset=0
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/
	app.get('/user/list', controllers.callbackGetAllUser);
    
    /**
	*
	*   @api {post} /user/new New user
	*
	*   @apiName CreateNewUser
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method create new user
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/new
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*   @apiParam {String} name The user name
	*   @apiParam {String} surname The user surname
	*   @apiParam {String} email The user email
	*   @apiParam {String} password The user password
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/
	app.post('/user/new',controllers.checkParamNewUser, controllers.validateString, controllers.callbackNew);
	
	/**
	*
	*   @api {post} /user/:userid/update Update user info
	*
	*   @apiName UpdateUserInfo
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method update user info
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/1/update
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiParam {number} userid me || user id  
	*   @apiParam {String} [name] change name field 
	*	@apiParam {String} [surname] change surname field
	*	@apiParam {String} [address] change address field
	*	@apiParam {String} [gender] change gender field
	*	@apiParam {String} [birthday] change age field
	*	@apiParam {String} [telephone] change telephone number
	*   @apiParam {String} [address_street] change street address
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/	
	app.post('/user/:userid/update', filters.auth,  controllers.checkParamUpdateInfo, controllers.validateUserString, controllers.callbackUpdateInfo);
    
    /**
	*
	*   @api {get} /user/:userid/details User details
	*
	*   @apiName GetUserDetails
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method get user details 
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/1/details
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiParam {number} userid me || user id  
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/	
    app.get('/user/:userid/details', filters.auth, controllers.userDetails);
    
    /**
	*
	*   @api {post} /user/:userid/newaddress Create new address
	*
	*   @apiName CreateNewAddress
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method create new address
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/1/newaddress
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiParam {number} userid me || user id  
	*	@apiParam {String} addressname  Identifier of address
	*	@apiParam {String} addressvalue Value of address
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/	
    app.post('/user/:userid/newaddress' , filters.auth,  controllers.checkParamNewAddress,controllers.validateAddress, controllers.callbackInsertnewAddress);
    
    /**
	*
	*   @api {post} /user/:userid/deleteaddress Delete adress
	*
	*   @apiName DeleteAdress
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method delete user address
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/1/deleteaddress
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiParam {number} userid me || user id  
	*	@apiParam {String} address_id  Id of address 
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/	
    app.post('/user/:userid/deleteaddress', filters.auth,  controllers.checkAddressId, controllers.callbackDeleteAddress);
    
    
    /**
	*
	*   @api {post} /user/:userid/updateaddress Update adress
	*
	*   @apiName UpdateAdress
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method update user address
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/1/updateaddress
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiParam {number} userid me || user id  
	*	@apiParam {String} address_id  id of address 
    *	@apiParam {String} address_value  value of the new address
   	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/	
    app.post('/user/:userid/updateaddress', filters.auth,  controllers.checkUpdateParam, controllers.callbackUpdateAddress);
    
     /**
	*
	*   @api {post} /user/:userid/addcar Add user car
	*
	*   @apiName AddCar
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method add user car
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/me/addcar
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiParam {Number} userid me || user id
	*	@apiParam {Number} allestiment id car allestiment 
   	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/	
    app.post('/user/:userid/addcar', filters.auth,  controllers.checkAllestimentId, controllers.callbackInsertCar);
    
    /**
	*
	*   @api {post} /user/:userid/removecar Remove user car
	*
	*   @apiName RemoveCar
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method remove user car
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/me/removecar
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiParam {number} userid me || user id  
	*	@apiParam {Number} allestiment id car allestiment
   	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/
    app.post('/user/:userid/removecar', filters.auth,  controllers.checkAllestimentId, controllers.callbackDeleteCar);
    
    /**
	*
	*   @api {post} /user/:frienda/friendstatus/:friendb Friendship status
	*
	*   @apiName FriendshipStatus
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method remove user car
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/2/friendstatus/1
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiParam {Number} usera id first member
	*	@apiParam {Number} userb id second member
   	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/
	app.post('/user/:frienda/friendstatus/:friendb', controllers.checkFriendshipParam, controllers.callbackFriendshipStatus);  
    
    /**
	*
	*   @api {post} /user/me/sendinvite Send email invite
	*
	*   @apiName SendEmailInvite
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method send email invite to user
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/me/sendinvite
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiParam {String} to user to invite
   	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/
    app.post('/user/me/sendinvite', filters.auth, controllers.checkUserInviter, controllers.callbackSendInvite);
    
     /**
	*
	*   @api {get} /user/retrivecod Get user by facebookID
	*
	*   @apiName GetUserByFBID
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive user from fb id
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/retrivecod?facebook_id=6fdsajfdksjafkjdsajkhfdsa
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiParam {String} facebook_id id facebook user
   	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/
    app.get('/user/retrivecod', filters.auth , controllers.checkFacebookCod, controllers.callbackFacebookCod);
    
    /**
	*
	*   @api {get} /user/getmail Get user by email
	*
	*   @apiName GetByEmail
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive user by email
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/getmail?email=info@comevado.com
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiParam {String} email The user email
   	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/
    app.get('/user/getmail', filters.auth, controllers.checkParamEmail, controllers.callbackFindMail);
   
   /**
	*
	*   @api {get} /user/getmail Get user by username
	*
	*   @apiName GetUserByUname
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive user by user name
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/getmail?uname=utente1kli
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiParam {String} uname The username of the user
   	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/
    app.get("/user/username", controllers.checkUsername, controllers.callbackUname);
     
    /**
	*
	*   @api {post} /user/:iduser/password/modify Modify password
	*
	*   @apiName ModifyPassword
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method modify user password
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/user/1/password/modify
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiParam {String} password The new password
   	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*
	*/
    app.post("/user/:iduser/password/modify", filters.auth, controllers.checkParamsPs, controllers.callbackNewPassword)
     
      /**
	*
	*   @api {post} /sendassitence Send assistence the developer
	*
	*   @apiName SendAssistence
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method send email assistence to developer
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/sendassitence
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiParam {String} recaptcha_response The captcha code
	*	@apiParam {Object} data The object email
	*	@apiParam {String} data.from The sender email
	*	@apiParam {String} data.argument The email argument
	*	@apiParam {String} data.content The email body
   	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*		
	*		TO DO
	*/
    app.post("/sendassitence", controllers.checkParamsAssitence, controllers.checkCaptcha, controllers.callbackSendMailAssistence)
}