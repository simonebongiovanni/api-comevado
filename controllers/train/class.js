var errors = global.errors;
var trenitalia = require('./trenitalia.js');

module.exports.trenitalia = trenitalia;

/**
*
*
*	Callback Trenitalia
*
*
*/
module.exports.callbackTrenitalia = function(req, res) {
	trenitalia.trenitalia(req, res);
}

/**
*
*
*	Check Trenitalia input param
*
*
*/
module.exports.checkParam = function(req, res, next) {
	status = 200;
	if(req.body.going === undefined || req.body.going == '' || (req.body.going > 1 || req.body.going < 0)) {
		status = 400;																				
	}
	else {
		if(req.body.going == 1)
			req.body.going = true;
		else
			req.body.going = false;
	}
	
	
	if(req.body.departureStation === undefined || req.body.departureStation == '') {
		status = 400;
	}
		
	if(req.body.arrivalStation === undefined || req.body.arrivalStation == '') {
		status = 400;
	}
		
	if(req.body.departureDate === undefined || req.body.departureDate == '') {
		status = 400;
	}
	else {
		obj = req.body.departureDate.split('-');
		if(obj.length != 3)
			status = 400;
		else {
			if(isNaN(obj[0]) || isNaN(obj[1]) || isNaN(obj[2]))
				status = 400;
			else {
				day = leftPad(obj[0],2);
				month = leftPad(obj[1],2);
				year = obj[2];
				req.body.departureDate = day + '-' + month + '-' + year;
			}	
		}
	}
		
	if(req.body.departureTime === undefined || req.body.departureTime == '')
		status = 400;
		
	if(req.body.adult === undefined || req.body.adult == '' || isNaN(req.body.adult) || (parseInt(req.body.adult) < 0))
		status = 400;
		
	if(req.body.children === undefined || req.body.children == '' || isNaN(req.body.children) || parseInt(req.body.children) < 0)
		status = 400;
		
	if(status != 200) {
		errors.sendError(res, 400);
	}
	else
		next();
} 		

function leftPad(number, targetLength) {
    var output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
}							
																										