
var cheerio = require('cheerio');
var utl = require('../../utilities.js');
var request = require('request');
var zlib = require('zlib');

var trenitalia = function (req, res) {
	var form = {
		tripType: 'on',
		isRoundTrip: false,//FIXME una volta gestito il ritorno si utilizza req.body.going
		departureStation: req.body.departureStation,
		departureTime: '00',
		arrivalStation: req.body.arrivalStation,
		departureDate: req.body.departureDate,
		adult: '1',
		children: '0'
	};
	
	var host = 'https://www.lefrecce.it';
	var path = '/B2CWeb/searchExternal.do?parameter=initBaseSearch&lang=it';
	var headers = {'Accept-Encoding':'gzip, deflate'};

	var callback = function(err, response, body) {
		
		if (!err) {
			if (response.headers['content-encoding'] == 'gzip') {
				zlib.gunzip(body, function (error, dezipped) {
					$ = cheerio.load(dezipped.toString('utf-8'));
					getJSON_trenitalia($, res);
				});
			}
		}
	}
	
	post(host, path, form, callback, headers);
}

exports.getTrenitalia = function (departureStation, arrivalStation, departureDate, callbackT) {
	var form = {
		tripType: 'on',
		isRoundTrip: false,//FIXME una volta gestito il ritorno si utilizza req.body.going
		departureStation: departureStation,
		departureTime: '00',
		arrivalStation: arrivalStation,
		departureDate: departureDate,
		adult: '1',
		children: '0'
	};
	
	var host = 'https://www.lefrecce.it';
	var path = '/B2CWeb/searchExternal.do?parameter=initBaseSearch&lang=it';
	var headers = {'Accept-Encoding':'gzip, deflate'};
	
	var callback = function(err, response, body) {
		
		if (!err) {
			if (response.headers['content-encoding'] == 'gzip') {
				zlib.gunzip(body, function (error, dezipped) {
					$ = cheerio.load(dezipped.toString('utf-8'));
					getJSON_trenitalia($, function(data){
						callbackT(data);
					});
				});
			}
		}
	}
	
	post(host, path, form, callback, headers);
}

function post(host, path, form, callback, headers) {
	url = 'https://www.lefrecce.it/B2CWeb/searchExternal.do?parameter=initBaseSearch&lang=it';
	
	var options = {
		url : this.url,
		headers : {
			'Accept-Encoding' : 'gzip, deflate'
		},
		form : form,
		encoding: null
	};

	req = request.defaults({
		jar: true,
		rejectUnauthorized: false,
		followAllRedirects: true
	});
	req.post(options, callback);
}

/**
*
*
*	Retrive trenitalia solution
*
*
*/
function getJSON_trenitalia($, callbackT) {
	var solutions = $("div[id^='travelSolution']");
	var routes = new Array();
	solutions.each(function (index, element) {
		routes.push({
			'departure' : $(element).find('#aa_'+index).find('.bottom').text().replace(/\W\s/g, '').trim(),
			'arrival' : $(element).find('#aa_'+index).next().find('.bottom').text().replace(/\W\s/g, '').trim(),
			'duration' : $(element).find('#aa_'+index).next().next().find('.descr').text().replace(/[^0-9:]/g, ''),
			'price' : $(element).find('.price').text().replace(/[^0-9,]/g, '').replace(',','.'),
			'departure_time' : $(element).find('#aa_'+index).find('.time').text().replace(/[^0-9:]/g, ''),
			'arrival_time' : $(element).find('#aa_'+index).next().find('.time').text().replace(/[^0-9:]/g, ''),
			'changes' : $(element).find('#aa_'+index).next().next().find('.descr').next().text().replace(/[^0-9]/g, ''),
			'code_trains' : get_code_trains_t($, element, index),
			'train_types' : get_train_types_t($, element, index),
			'departures_times_changes' : get_departures_times_changes_t($, element, index),
			'arrivals_times_changes' : get_arrivals_times_changes_t($, element, index),
			'durations_durations' : get_durations_changes_t($, element, index),
			'segments' : get_segments_t($, element, index),
			'offers_type' : get_offers_type_t($, element, index) 
		});
	});
	
	callbackT(routes);
}

function get_train_types_t($, element, index) {
	train_types = new Array();
	$(element).find('#aa_' + index).next().next().next().find('.row').find('.train').each(function(i, el) {
		$(el).find('.descr').find('strong').remove();
		train_types.push($(el).find('.descr').text().replace(/\W\s/g, '').trim());
	});

	return train_types; 
}

function get_durations_changes_t($, element, index) {
	durations = new Array();
	$(element).find('#travelSolution' + index + '_TABLE').find('tr').each(function(i, el) {
		durations.push($(el).find('td').first().next().next().text().replace(/[^0-9:]/g,'').trim());
	});
	return durations;
}

function get_departures_times_changes_t($, element, index) {
	departure_time = new Array();
	$(element).find('#travelSolution' + index + '_TABLE').find('tr').each(function(i, el) {
		departure_time.push($(el).find('td').first().find('strong').text().replace(/[^0-9:]/g,'').trim());
	});
	return departure_time;
}

function get_arrivals_times_changes_t($, element, index) {
	arrival_time = new Array();
	$(element).find('#travelSolution' + index + '_TABLE').find('tr').each(function(i, el) {
		arrival_time.push($(el).find('td').first().next().find('strong').text().replace(/[^0-9:]/g,'').trim());
	});
	return arrival_time;
}

function get_code_trains_t($, element, index) {
	code_trains = new Array();
	$(element).find('#aa_' + index).next().next().next().find('.row').find('.train').each(function(i, el) {
		code_trains.push($(el).find('.descr').find('strong').text().replace(/[^0-9]/g, '').trim());
	});
	return code_trains; 
}

function get_segments_t($, element, index) {
	segment = new Array();
	$(element).find('#travelSolution' + index + '_TABLE').find('tr').each(function(i, el) {
		$(el).find('td').first().find('strong').remove();
		$(el).find('td').first().next().find('strong').remove();
		segment.push({
			'departure': $(el).find('td').first().text().replace(/\W\s/g,'').trim(),
			'arrival': $(el).find('td').next().first().text().replace(/\W\s/g,'').trim(),
		});
	});
	return segment;
}

function get_offers_type_t( $, element, index) {
	offer_types = new Array();
	$(element).find('#aa_' + index).next().next().next().find('.row').find('.offer').each(function(i, el) {
		$(el).find('.descr').find('strong').remove();
		offer_types.push($(el).find('.descr').text().replace(/\W\s/g, '').trim());
	});

	return offer_types; 
}

module.exports.trenitalia = trenitalia;