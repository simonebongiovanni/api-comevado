var controllers = require('./class.js');

module.exports = function(app) {
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {post} /train/trenitalia Get train info
	*
	*   @apiName GetTrainInfo
	*	@apiGroup Train
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive train info
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/train/trenitalia
	*
	*   @apiParam {number} going The type of trip: 0 one way - 1 round trip
	*   @apiParam {String} departureStation The departure station
	*   @apiParam {String} arrivalStation The arrival station
	*   @apiParam {String} departureDate departure date FORMAT DATA gg-mm-aaaa
	*   @apiParam {String} departureTime The departure time
	*   @apiParam {Number} adult The number of adult
	*   @apiParam {Number} children The number of children
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*
	*		TO DO
	*/
	app.post('/train/trenitalia', controllers.checkParam, controllers.callbackTrenitalia);
	
}

global.controllers.train = controllers;