var errors = global.errors;
var utilities = global.utilities

module.exports.checkParam = function(req, res, next){
	if(req.query.access_token === undefined || req.query.access_token == '')
		errors.sendError(res, 400);
	else 
		next()
}

module.exports.callbackGetFriend = function(req, res){
	var host = "www.google.com";
	var path = "/m8/feeds/contacts/default/full?alt=json&access_token=" + req.query.access_token;
	var callback = function(status, data){
		var args = JSON.parse(data);
        console.log
		var entry = [];
        
        console.log(status);
		
		for(i = 0; i < args.feed.entry.length; i++)
        {
            if(typeof args.feed.entry[i].gd$email != "undefined")
            {
                entry.push({
                        name: args.feed.entry[i].gd$email[0].address
                    });
            }
            
        }
        
		res.send({contact: entry});
	}
	
	utilities.https.get(host, path, callback);
} 

module.exports.callbackRequestFacebook = function(req, res){
	var host= "graph.facebook.com";
	path ="/oauth/access_token?client_id=314040415414472&client_secret=5837a6b8fd9e6798408a6f424f59be13&grant_type=client_credentials";
	
	callback = function(status, data){
		token = data.split("=");
		
		res.send({app_token: token[1]});
	}
	
	utilities.https.get(host, path, callback);
}