var filters = global.filters;
var controllers = require("./class.js");

module.exports = function(app) {
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {get} /social/google/friendlist Google contact list
	*
	*   @apiName GetUserGoogleContactList
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method retrive User google contact list
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/social/google/friendlist?access_token=fjasjhfsdfjsdajfsd
	*
	*
	*   @apiParam {String} access_token The gplus access_token
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	
	*		TO DO
	*/
	app.get("/social/google/friendlist", controllers.checkParam, controllers.callbackGetFriend);  
	
	/**
	*
	*	Retrive facebook app token
	*
	*
	*/
	app.get("/social/facebook/apptoken", controllers.callbackRequestFacebook)
	
}