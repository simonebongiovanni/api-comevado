var controllers = require("./class.js");
var filters = global.filters;

module.exports = function(app) {
	
	/**
	 * @apiDefine InvalidParameters
	 *
	 * @apiError InvalidParameters Invalid Parameters
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 400 Invalid Parameters
	 *     {
	 *       "error": "InvalidParameters"
	 *     }
	 */
	
	
	/**
	*
	*   @api {post} /sendinvite Send user registration invite
	*
	*   @apiName SendUserInvite
	*	@apiGroup User
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method send user registration invite
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/group/listtype
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*	  TO DO
	*
	*/
	app.post("/sendinvite", filters.auth, controllers.sendinvite);

}