var db = global.db;
var errors = global.errors;
var code_erros = global.errors.codes;
var async = require("async");
var utilities = global.utilities;


module.exports.sendinvite = function(req, res, next){
	
    if(typeof req.body.recipient != "undefined")
    {
        req.user.getDetails(function(data){
            var name = "";
            
            if(typeof data.user_info.name != "undefined" && typeof data.user_info.surname != "undefined" )
                name = data.user_info.name +" "+ data.user_info.surname;
            else
                name = data.username;
            
            var msg = req.body.msg || " ";
            if(req.body.msg == 0)
                msg = "";
            
            global.mailer.templates.inviteFriend(req.body.recipient, name, msg, 'http://demo.comevado.com/#/register');
            
            res.send("ok");
        });
    }
    else
    {
         errors.sendError(res, errors.codes.invalidParam, "recipient not found");
    }
    
}