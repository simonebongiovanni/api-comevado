var fs = require("fs");
var path = require('path');

exports.render = function(template, data){
       
    var body = String(fs.readFileSync(path.join(__dirname, template+".html")));
    
    for (prop in data) {
        body = body.replace("%"+prop+"%", data[prop]);
    }
    
    return body;    
}
