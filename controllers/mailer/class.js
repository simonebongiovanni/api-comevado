
var settings = global.settings.email;
var email   = require("emailjs");
var templates = require("./templates");

var email_server  = email.server.connect({
   user: settings.user, 
   password: settings.password, 
   host: settings.host, 
   ssl: settings.ssl,
   port: settings.port    
});

var queue = [];
var isBusy = false;

var processQueue = function(){
    
    if(!isBusy)
    {
        isBusy = true;    
        var message = queue[queue.length - 1];
        console.log("sending email to: "+message.to);
        
        email_server.send(message, function(err, msg) { 
            if(err) 
            {
                console.log(err);                
            }                 
            else
            {
                queue.pop(); 
                console.log("mail successful sended!"); 
            }
            
            isBusy = false;
            
            if(queue.length != 0 && !isBusy)
                processQueue();

        }); 
        
    }
       
}

var addToQueue = function(message){
	console.log(message);
    queue.push(message);
    if(!isBusy)
        processQueue();
}

global.sendMessage = addToQueue;

var sendTempate = function(to, subject, template, data){
    
    var content = templates.render(template, data);
    
    var message = {
       text: "",
       from: "Comevado <"+settings.user+">", 
       to: to,
       subject: subject,
       attachment: [
          {
              data: content, 
              alternative: true
          }
       ]
    };
        
    addToQueue(message);
}

/**
 * send(to, subject, content)
 * @param {string} to - recipient's email address
 * @param {string} subject - mail subject
 * @param {string} content - mail content (text only)
 */

exports.send = function(to, subject, content){
    
    var message = {
       text: content,
       from: "Comevado <"+settings.user+">", 
       to: to,
       subject: subject
    };
    
    addToQueue(message);
}


/**
 * templates
 */

exports.templates = {};
exports.templates.wellcome = function(to, name, username, link){
    sendTempate(to, "Benvenuto " + name, "wellcome", {name: name, link: link, username: username});
}

exports.templates.recoveryPassword = function(to, name, link){
    sendTempate(to, "Recupera la password", "passreset", {name: name, link: link});
}

exports.templates.inviteFriend = function(to, name, msg, link){
    sendTempate(to, name + " ti ha invitato su Comevado", "invite", {name: name, link: link, msg: msg});
}
