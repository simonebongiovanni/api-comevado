global.mailer = require('./class.js');

module.exports = function(app) {
	
	/**
	*
	*   @api {get} /sendtestmail Send test mail
	*
	*   @apiName TestingMailFlow
	*	@apiGroup Test
	*   @apiVersion 1.0.0
	*
	*	@apiDescription This method send test mail to a specific address
	*
	*	@apiExample  Example usage:
	*      http://api.comevado.com/activity/47/info
	*	
	*
	*	@apiHeader {String} x-session-token user token session
	*   @apiHeaderExample {json} Header-Example:
	*     {
	*       "x-session-token": "dfadfd324das"
	*     }
	*
	*
	*	@apiSuccessExample {json} Success-Response:
	*     HTTP/1.1 200 OK
	*   	
	*		TO DO
	*   
	*/
	app.get("/sendtestmail", function(req, res){
        
        global.mailer.templates.inviteFriend("simone.bongiovanni.it@gmail.com", "Pippo", "Simone.bongiovanni", "pippo", "link");
        
        res.send("ok");
    })


}

    