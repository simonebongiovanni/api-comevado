
var filters = global.filters;


/**
 * Model Controller
 */

var testController = require("./class");


module.exports = function(app)
{
    app.get("/test", testController.test);
        
}

global.controllers.test = testController;