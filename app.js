
/**
 *  Modules
 **/

var express = require("express");
var bodyParser = require('body-parser');
var busboy = require('connect-busboy');

var app = express();

app.use(busboy());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
/**
 * CORS Setting
 **/

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, x-session-token, x-platform');
    if ('OPTIONS' == req.method) {
         res.status(200).send("ok");
     } else {
         next();
     }
});

global.settings = require("./settings.json");
global.errors = require("./errors");
global.utilities = require('./utilities');
global.filters = require("./filters");

global.db = require("./databases/main");
global.db_car = require("./databases/car");
global.db_autocomplete = require("./databases/autocomplete");
global.db_usercomplete = require("./databases/usercomplete"); 

global.controllers = {};

/*
 * Woopra
 */
var Woopra = require("woopra");
global.woopra = new Woopra('comevado.com');
global.woopra.config({
    ssl: true
});

var aws = require("./controllers/aws")(app);
var google = require("./controllers/google")(app);
//var travelController = require("./controllers/travel")(app);

var fake_activity = require("./controllers/fake_activity")(app);
var autentication = require("./controllers/autentication")(app);
var romeToRio = require("./controllers/rome2rio")(app);
var autostrade = require('./controllers/autostrade')(app);
var autocomplete = require("./controllers/autocomplete")(app);
var usercomplete = require("./controllers/usercomplete")(app);
var co2 = require("./controllers/co2")(app);
var fuel = require("./controllers/fuel")(app);
var train = require("./controllers/train")(app);
var user = require("./controllers/user")(app);
var groups = require('./controllers/groups')(app);
var permission = require("./controllers/permissions")(app);
var groups = require("./controllers/groups")(app);
var mailer = require("./controllers/mailer")(app);
var skyscanner = require("./controllers/skyscanner")(app);
var car = require("./controllers/car")(app);
var social = require("./controllers/social")(app);
var upload = require("./controllers/upload")(app);
var teststring = require("./controllers/teststring")(app);
//var relation = require("./controllers/relations")(app);
var relation = require("./controllers/relations")(app);
var travel = require("./controllers/travel")(app);
var comment = require('./controllers/comment')(app);
var activity = require("./controllers/activity")(app);
var invite = require("./controllers/invite")(app);

/**
 *  Docs
 **/

app.use('/docs', express.static('docs'));

/**
 *  Error Handler
 **/

app.use(global.errors.notFoundHandler);
app.use(global.errors.errorHandler);

/*
 * Event manager
 */

global.notificator = require("./notificator");

/**
 *  Server startup
 **/

var port = process.env.PORT || global.settings.server_port;

app.listen(port, function() {
   console.log("API server listening on port " + port);
});