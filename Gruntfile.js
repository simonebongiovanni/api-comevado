'use strict';
module.exports = function (grunt) {

    grunt.initConfig({
        awsebtdeploy: {
            single: {
              options: {
                region: 'eu-central-1',
                applicationName: 'ComevadoApi',
                environmentCNAME: 'comevado-single.elasticbeanstalk.com',
                sourceBundle: "archive.zip",
                accessKeyId: "AKIAJF5SIB5KF2RB7A7Q",
                secretAccessKey: "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",
                deployType : "inPlace",
                versionLabel : '<%= ((new Date()).valueOf().toString()) + (Math.floor((Math.random()*1000000)+1).toString()) %>',
                s3: { 
                    bucket: 'elasticbeanstalk-eu-central-1-618617042311',  
                    key: "archive.zip"
                }
              }
            },
            loadbalance: {
              options: {
                region: 'eu-central-1',
                applicationName: 'ComevadoApi',
                environmentCNAME: 'comevado-loadbalance.elasticbeanstalk.com',
                sourceBundle: "archive.zip",
                accessKeyId: "AKIAJF5SIB5KF2RB7A7Q",
                secretAccessKey: "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",
                deployType : "inPlace",  
                versionLabel : '<%= ((new Date()).valueOf().toString()) + (Math.floor((Math.random()*1000000)+1).toString()) %>',
                s3: { 
                    bucket: 'elasticbeanstalk-eu-central-1-618617042311', 
                    key: "archive.zip"
                }
              }
            }
          },
        compress: {
          main: {
            options: {
                archive: 'archive.zip'
            },
            expand: true,
            cwd: '',
            src: ['**/*', '!archive.zip', '!Gruntfile.js'],
            dest: ''
          }
        }
    });
    
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-awsebtdeploy');
    
    grunt.registerTask('archive', ['compress']);
    grunt.registerTask('upload-demo', ['awsebtdeploy:single']);
    grunt.registerTask('upload-master', ['awsebtdeploy:loadbalance']);
    grunt.registerTask('demo', ['compress', 'awsebtdeploy:single']);
    grunt.registerTask('master', ['compress', 'awsebtdeploy:loadbalance']);
}
