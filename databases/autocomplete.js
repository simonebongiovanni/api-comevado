var Sequelize = require('sequelize');
var settings = global.settings.databases.autocomplete;
/**
 * Creating sequelize OBJ
 * @param {string} database_name
 * @param {string} database_user
 * @param {string} database_password
 **/

var sequelize = new Sequelize(settings.schema, settings.username, settings.password, {
    dialect: settings.dialect,
    port: settings.port,
    host: settings.host,
    logging: function (str) {
        if (settings.log)
            console.log("querylog: " + str.replace("Executing (default):", ""));
    }
});

sequelize.authenticate().then(function (err) {
    if (!!err) {
        console.log('Database ' + settings.schema + ' Connection Error:', err)
    } else {
        console.log('Database ' + settings.schema + ' Connected')
    }
})

exports.sequelize = sequelize;

/**
 *  getAutocomplete
 *
 * @param {string} database_name
 * @param {string} database_user
 * @param {string} database_password
 * @param {function} callback
 **/

exports.getAutocomplete = function (local, lat, lng, callback) {
    var locality = "";
    var limit = 6;
    locality = local;
    locality = locality.toLowerCase();
    locality = locality + "%";
    locality = locality.replace(/ /g, "%");
    local = local.substr(0, 2);
    local = local.toLowerCase();
    table = 'cvautocomplete_' + local; //Da convertire in Javascript
    if (local.length >= 2) {
        sequelize.query("SELECT asciiname_extended, latitude, longitude, country_code, country_name, admin2_code, (peso-pesopaese) AS ordine FROM(SELECT asciiname_extended, latitude, longitude, country_code, country_name, admin2_code, IF (country_code = 'IT',10000,0) AS pesopaese, ROUND(((POW('latitude' - :lat, 2) + POW('longitude' - :lng, 2)) / (pop_fact))*1000000) as peso FROM " + table + " WHERE asciiname_extended LIKE :local) as T1 ORDER BY ordine ASC LIMIT :limit", {
            replacements: {
                lat: lat,
                lng: lng,
                local: locality,
                limit: limit
            },
            type: sequelize.QueryTypes.SELECT
        }).then(function (autocomplete) {
            callback(autocomplete);
        });
    }
};