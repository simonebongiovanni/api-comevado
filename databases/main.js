var Sequelize = require('sequelize');
var settings = global.settings.databases.main;
var errors = global.errors;
var utilities = global.utilities; 

var sequelize = new Sequelize(settings.schema, settings.username, settings.password, {
      dialect: settings.dialect,
      port: settings.port,
      host: settings.host,
      logging: function (str) {
          if(settings.log)
            console.log("querylog: "+str.replace("Executing (default):", "") );
      }
    });

sequelize.authenticate().then(function(err) {
    if (!!err) {
      console.log('Database '+settings.schema+' Connection Error:', err)
    } else {
      console.log('Database '+settings.schema+' Connected')
    }
  })

exports.sequelize = sequelize;

/*
 *   User
 */

exports.User = sequelize.define('users', {
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    is_active: Sequelize.BOOLEAN,
    activation_code: Sequelize.STRING,
    session_token: Sequelize.STRING,
    refresh_token: Sequelize.STRING,
    timestamp: Sequelize.DATE,
    dataregistration: Sequelize.DATE,
    first_login: Sequelize.BOOLEAN,
    android_session_token: Sequelize.STRING,
    ios_session_token: Sequelize.STRING
}, {
    timestamps: false,
    instanceMethods: {
        checkPassword: function(hash)
        {
            return utilities.bcrypt.check(hash, this.password);
        },
        setFirstLogin: function(callback){
            this.first_login = true;
            this.save().then(function(user){
                callback();
            })
        },
        getInfo: function(callback){            
            exports.UserInfo.findAll({where:{id_user: this.id}})
            .then(function(data){
                callback(data);
            })
        },
        setInfo: function(type, value, callback){

           exports.UserInfo.create({
                id_user: this.id,
                id_type: type,
                value: value
            }).then(function(data){
               if(callback)
                   callback(data);
            });
        },
        getDetails: function(callback){
            module.exports.getUserInfo(this.id, callback);
        }
    }
})


exports.usrAutocomplete = {
    addWithName: function(user_id, value, value1){
        sequelize.query("CALL userADDtoAutocomplete_SP(6, :user_id, :value, :value1);", null, {raw: true}, 
                        {user_id: user_id, value: value, value1: value1}).then(function() {
            
        })
    },
    addWithSurname: function(user_id, value, value1){
        sequelize.query("CALL userADDtoAutocomplete_SP(7, :user_id, :value, :value1);", null, {raw: true}, 
                        {user_id: user_id, value: value, value1: value1}).then(function() {
            
        })
    },
    updateWithName:  function(user_id, value, value1){
        sequelize.query("CALL userUPDtoAutocomplete_SP(6, :user_id, :value, :value1);", null, {raw: true}, 
                        {user_id: user_id, value: value, value1: value1}).then(function() {
            
        })
    },
    updateWithSurname:  function(user_id, value, value1){
        sequelize.query("CALL userUPDtoAutocomplete_SP(7, :user_id, :value, :value1);", null, {raw: true}, 
                        {user_id: user_id, value: value, value1: value1}).then(function() {
            
        })
    }
}



//db.usrAutocomplete.addWithName(89, "Mario");
//db.usrAutocomplete.addWithName(89, "Mario");

/*
 *   users_external
 */

exports.UserExternal = sequelize.define('users_external', {
    name: Sequelize.STRING,
    client_id: Sequelize.STRING,
    client_secret: Sequelize.STRING,
    refresh_token : Sequelize.STRING,
    session_token : Sequelize.STRING,
    credits : Sequelize.INTEGER,
    timestamp : Sequelize.DATE
}, {
    timestamps: false,  
    instanceMethods: {
        incQuota: function()
        {
            this.quota++;
            this.save(function(user){});
        }
    }
})

/*
 *   user_relations
 */

exports.UserRelation = sequelize.define('user_relations', {
    id_user1: Sequelize.INTEGER,
    id_user2: Sequelize.INTEGER,
    id_type_relation: Sequelize.INTEGER,
    is_confirmed: Sequelize.BOOLEAN,
    is_deleted: Sequelize.BOOLEAN,
    date : Sequelize.DATE
}, {
    timestamps: false,
    	tableName: 'user_relations',
});

/*
 *   user_relation_type
 */

exports.UserRelationType = sequelize.define('user_relation_type', {
    id: Sequelize.INTEGER,
    description: Sequelize.STRING
}, {
    timestamps: false
})

//exports.UserRelation.belongsTo(exports.User, {as: 'Initiator', foreignKey: 'id_user1'})

/*
 *   user_info
 */
exports.UserInfo = sequelize.define('user_info', {
    id_user: Sequelize.INTEGER,
    id_type: Sequelize.INTEGER,
    value: Sequelize.INTEGER
}, {
    timestamps: false,
    tableName: 'user_info'
});

exports.UserInfoType = sequelize.define('user_info_type',{
	field: Sequelize.STRING
},{
	timestamps: false,
	tableName: 'user_info_type'
});

exports.UserInfo.belongsTo(exports.UserInfoType, {foreignKey: 'id_type'});
exports.UserInfoType.hasMany(exports.UserInfo, {foreignKey:'id', }); 

/* import all user into autocomplete_user
exports.UserInfo.findAll({where:{id_type: 6}}).then(function(datas){
    datas.forEach(function(data){
        exports.usrAutocomplete.addWithName(data.id_user, data.value);
    })
})

exports.UserInfo.findAll({where:{id_type: 7}}).then(function(datas){
    datas.forEach(function(data){
        exports.usrAutocomplete.addWithSurname(data.id_user, data.value);
    })
})
*/

/*
 *   Quote
 */

exports.Quota = sequelize.define('users_quota', {
    id_user: Sequelize.INTEGER,    
    user_type: Sequelize.INTEGER,
    api_type: Sequelize.INTEGER,
    ip1: Sequelize.INTEGER, 
    ip2: Sequelize.INTEGER, 
    ip3: Sequelize.INTEGER, 
    ip4: Sequelize.INTEGER,
    value: Sequelize.INTEGER
}, {
    timestamps: false, 
    instanceMethods: {
        getIp: function()
        {
            return this.ip1.toString()+"."+this.ip2.toString()+"."+this.ip3.toString()+"."+this.ip4.toString();
        },
        getUser: function(callback)
        {
            if(this.user_type == null)
            {
                callback(null);  
            }                
            else if(this.user_type == 1)
            {
                 exports.User.find({where:{id: this.id_user}}).then(function(user){
                    callback(user);
                })
            }
            else if(this.user_type == 2)
            {
                 exports.ExternalUser.find({where:{id: this.id_user}}).then(function(user){
                    callback(user);
                })
            }
        }
    }
});

exports.Comment = sequelize.define('comment',{
	id_parent: Sequelize.BIGINT(20),
	id_typecomment: Sequelize.BIGINT(20),
	body: Sequelize.TEXT,
	id_author: Sequelize.BIGINT(20),
	is_approved: Sequelize.BOOLEAN,
	datainsert: Sequelize.DATE,
	starsnumber: Sequelize.BIGINT(20),
	is_preferences: Sequelize.BOOLEAN,
	is_like: Sequelize.BOOLEAN,
	is_dislike: Sequelize.BOOLEAN,
	id_activity: Sequelize.BIGINT(20)
},{
	timestamps: false,
    tableName: 'comment',
    classMethods:{
	    newComment: function(req, callback){
	    	db.ActivityStream.emit(11, req.user.id, function(activity){
				db.Comment.build(
				{	    id_parent: req.body.parent, 
						id_typecomment: req.body.type, 
						body: utilities.checkString.replaceInvalidChar(req.body.comment),
						id_author: req.user.id,
						is_approved: 1,
						starsnumber:  0,
						is_preferences:  0,
						is_like: 0,
						is_dislike: 0,
						id_activity: activity.id})
			     .save()
			     .then(function(data){
				    callback({activity:activity, comment:data});
			     });
			});
	    }
    }
});
	
function recursiveCall(i, arrayPrint, result, res, req) {
	if(i < result.length) {
		comment = result[i];
		var query = 'SELECT id_author as id, is_preferences, is_like, a.cont FROM comment JOIN (SELECT id_parent, count(*) as cont FROM comment WHERE id_parent = ' + comment.id + ' AND is_like = 1 GROUP BY id_parent) a ON a.id_parent = comment.id_parent WHERE comment.id_parent = ' + comment.id + ' AND is_like = 1 LIMIT 4 UNION  SELECT id_author as id,is_preferences, is_like, cont FROM comment JOIN (SELECT id_parent, count(*) as cont FROM comment WHERE id_parent = ' + comment.id + ' AND is_dislike = 1 GROUP BY id_parent) a ON a.id_parent = comment.id_parent WHERE comment.id_parent = ' + comment.id + ' AND is_dislike = 1 LIMIT 4 UNION SELECT id, is_preferences, is_like, cont FROM comment JOIN (SELECT id_parent, count(*) as cont FROM comment WHERE id_parent = ' + comment.id + ' AND is_preferences = 0) a ON a.id_parent = comment.id WHERE comment.id_parent = ' + comment.id + ' AND is_preferences  = 0';
		
		sequelize.query(query , { type: sequelize.QueryTypes.SELECT, replacements: {parent: req.body.parent, id: req.body.id, type: req.body.type, refer: req.body.refer, author: req.body.author, approved: req.body.approved}}).then(function(preference, a) {
			var like = [] 
			var dislike = [];
			var childcomment = [];
			var likec = 0;
			var dislikec = 0;
			for(k = 0; k < preference.length; k++) {
			
				if(preference[k].is_preferences == '1') {
					if(preference[k].is_like == '0') {
						dislike.push(preference[k].id);
						dislikec = preference[k].cont;
					}
					else {
						like.push(preference[k].id);
						likec = preference[k].cont;
					}
				} 
				else {
					childcomment.push(preference[k].id);
				}
			}
			
			comment.like = {
				like_cont: likec,
				user: like
			};
			
			comment.dislike = {
				dislike_cont: dislikec,
				user: dislike
			};
			
			comment.child_comment = {
				comment_id : childcomment
			};
			
			var rep = JSON.stringify(comment, function(key, value) {
		    if(value === null)
			    	return '';
			    else	
			        return value;
		    });
			
			arrayPrint.push(comment);
			i++;
			recursiveCall(i, arrayPrint, result, res, req);
			
		});

	}
	else {
		var rep = JSON.stringify(arrayPrint, function(key,value) {
			if(value === null) 
				value = '';
			
			
			return value;
		});
		res.status(200).send(JSON.parse(rep));
	}
}


/**
*
*	Insert wizard in database(wizard)
*
**/
module.exports.wizard = function(req, res) {
	var obj = buildWizard(req);
	var queryString = 'INSERT INTO wizard(id_comment,' + obj.field + ') VALUES (:comment,' + obj.value + ')';
	sequelize.query(queryString, {replacements: {comment: req.body.comment}} );
	res.status(200).send({response: 'OK'});
}

/**
*
*	Build wizard query
*
**/
function buildWizard(req) {
	var field = '';
	var value = '';
	
	for(var key in req.body) {
		if(field != '')
			field += ',';
		
		if(value != '')
			value += ',';
			
		switch(key) {
			case 'speed':
				field += 'rate';
				value += req.body[key];
			break;
			case 'punctuality':
				field += 'punctuality';
				value += req.body[key];
			break;
			case 'comfort':
				field += 'comfort';
				value += req.body[key];
			break;
			case 'price':
				field += 'price';
				value += req.body[key];
			break;
		}
	}
	
	return {
			field: field,
			value: value
	};
}

/**
*
*	Build filter for getting comment list
*
**/
function buildFilter(req) {
	var filter = '';
	var length = 0;
	
	for(var k in req.body)
		length++;
		
	if(length > 0) {
		for(k in req.body) {
	
		filter += ' AND ';
			    
			switch(k) {
				case 'parent':
					filter += " id_parent = :parent";
				break;
				case 'id':
				    filter += " comment.id = :id";
				break;
				case 'type':
					filter += " id_typecomment = :type";
				break;
				case 'refer':
					filter += " id_refer = :refer";
				break;
				case 'author':
					filter += " id_author = :author";
				break;
				case 'approved':
					filter += " is_approved = :approved";
				break;
				default:
				break;
			}
		}	
	}
	
	return filter;
}

/**
*
*	get fuel cost
*
**/
module.exports.getFuelCost = function(fuel, req, res) {
	var query = "SELECT DISTINCT price FROM fuel_cost JOIN fuel_type ON fuel_type.id = fuel_cost.id_fuel WHERE description = :fuel";
	
	sequelize.query(query, {replacements: {fuel: fuel}, type: sequelize.QueryTypes.SELECT}).then(function(data, a) {
		var price = data[0].price;
		res.status(200).send({price: price});
		
	});
}

/**
*
*	get co2 val
*
**/
module.exports.getCo2 = function(vehicle, req, res) {
	var query = "SELECT DISTINCT value FROM travel_vehicle JOIN co2_costant ON co2_costant.id_vehicle = travel_vehicle.id WHERE vehicle = :vehicle";
	
	sequelize.query(query, {replacements: {vehicle: vehicle}, type: sequelize.QueryTypes.SELECT}).then(function(data, a) {
		var value = data[0].value;
		res.status(200).send({co2: value});
	});
}



/**



	User registration



*/
module.exports.insertUser = function(req, callback) {
	var activation_code = utilities.randomString(40);

	var query = "INSERT INTO users(username,email,password,is_active, activation_code, dataregistration, first_login) VALUES(:username, :email, :password, 0,'" + activation_code + "', NOW(), 1)";
	
    var passwordEncrypted = utilities.bcrypt.generate(req.body.password);
    
	sequelize.query(query, {replacements: {username: req.body.email, email: req.body.email, password: passwordEncrypted}}).then(function(data, a) {
		var query1 = 'INSERT INTO user_info(id_user,id_type, value) VALUES(' + data[0].insertId + ',6,:name);';
		
		sequelize.query(query1, {replacements: {name: req.body.name}}).then(function(data, a) {
		});
		
		var query1 = 'INSERT INTO user_info(id_user,id_type, value) VALUES(' + data[0].insertId + ',7,:surname);';
		
		sequelize.query(query1, {replacements: {surname: req.body.surname}}).then(function(data, a) {
		});
		
		callback(data[0].insertId);
	});
}

module.exports.activateAccount = function(req, res) {
	var query = 'UPDATE users SET is_active = 1 WHERE is_active = 0 AND activation_code = :code';

	sequelize.query(query, {replacements: {code: req.body.activation_code}}).then(function(data, a) {
		if(data[0].changedRows == 1) {
		 	query = 'SELECT id FROM users WHERE activation_code = :code AND is_active = 1';
			sequelize.query(query, {replacements: {code: req.body.activation_code}, type: sequelize.QueryTypes.SELECT}).then(function(data1, a) {
 				res.send({activation_status: 'OK', user: data1[0].id });
 			});
 		}
		else
			res.send({activation_status: 'NONE'});
	});
}

module.exports.checkParamAddition = function(req, res, next) {
	var option = [];
	var i = 0;
	
	for(el in req.body) {
		if(el != 'user')
			option.push(el);
	}
	
	queryAddition(option, i, res, next);
}

function queryAddition(opt, i, res, next) {
	var query = 'SELECT count(*) as cont FROM user_info_type WHERE field = :field';
	
	if(i < opt.length) {
		sequelize.query(query, {replacements: {field: opt[i]}, type: sequelize.QueryTypes.SELECT}).then(function(data1, a) {
			if(data1[0].cont == 0) {
				errors.sendError(res, 400);
			}
			else {
				i++;
				queryAddition(opt, i, res, next);
			}
		});
	}
	else {
		next();
	}
}

module.exports.getUserInfo = function(user_id, callback) {
	var query = 'SELECT id, username, email, activation_code FROM users WHERE id = :users';
	var newdata = {};
	var address = {};
	var car = [];
    		
	sequelize.query(query, {replacements: {users: user_id}, type: sequelize.QueryTypes.SELECT}).then(function(data1) {
		var query1 = 'SELECT field, value FROM user_info JOIN user_info_type ON user_info.id_type = user_info_type.id WHERE id_user = :users';
		
		sequelize.query(query1, {replacements: {users: user_id}, type: sequelize.QueryTypes.SELECT}).then(function(data) {
			
			for(i = 0; i < data.length ; i++) {
				newdata[data[i].field] = data[i].value;
			}
			
			var query = 'SELECT addressname as address_name, addressvalue as address_value FROM user_custom_address WHERE id_user = :user';
			
			sequelize.query(query, {replacements: {user: user_id}, type: sequelize.QueryTypes.SELECT}).then(function(data2) {
				address = data2;
				
				var query = 'SELECT DISTINCT id_allestiment FROM user_vehicle WHERE id_user = :user';
				
				sequelize.query(query, {replacements: {user: user_id}, type: sequelize.QueryTypes.SELECT}).then(function(data3) {
					
					for(i = 0; i < data3.length; i++) {
						car.push(data3[i].id_allestiment);
					}
                    
					if(data1[0]){
						 if(data1[0].activation_code == null)
		                    social = true;
	                    else
	                    	social = false;
	                    
						var response = {
							user_id : data1[0].id,
							email : data1[0].email,
							username: data1[0].username,
							user_info: newdata || {},
							address: address || {},
							car_allestiment_id: car || {},
							social_user: social
						}

					}
					else{
						response = {};
					}
					
                    callback(response);
				});
			})
			
		});
	});
}


/*
 	Get Car Info
 */

exports.getVehicle = function(userID, callback) {
	var query = 'SELECT DISTINCT id_allestiment FROM user_vehicle WHERE id_user = :user';
	sequelize.query(query, {replacements: {user: userID}, type: sequelize.QueryTypes.SELECT}).then(function(data) {
		var car = [];
		for(i = 0; i < data.length; i++) {
			car.push(data[i].id_allestiment);
		}
		callback(car);
	});
}

/*
	
		
	User address
	
	
*/
module.exports.insertNewAddress = function(req, res) {
	var query = 'INSERT INTO user_custom_address(id_user, addressname,addressvalue) VALUES(:user,:key, :value)';
	
	sequelize.query(query, {replacements: {user: req.user.id, key: req.body.addressname, value: req.body.addressvalue}}).then(function(data) 	{
		if(data[0].affectedRows > 0 )
			res.send({id_address:data[0].insertId,response: 'Address insert correctly'});
	});
}

module.exports.deleteAddress = function(req, res) {
	var query = 'DELETE FROM user_custom_address WHERE id_user = :user AND id = :address'; 
	
	sequelize.query(query, {replacements: {user: req.user.id, address: req.body.address_id}}).then(function(data) {
		if(data[0].affectedRows > 0)
			res.send({status: 'Address delete correctly'});
		else
			res.send({status: 'No address delete'});
	});
}

module.exports.updateAddress = function(req, res) {
	var query = 'UPDATE user_custom_address SET addressvalue = :value WHERE id_user = :user AND id = :address';
	
	sequelize.query(query, {replacements: {user: req.user.id, address: req.body.address_id, value: req.body.address_value}}).then(function(data) {
		if(data[0].affectedRows > 0)
			res.send({status: 'Address update correctly'});
		else
			res.send({status: 'No address update'});
	});
}



/*
	
		
	User car	
	
	
*/
module.exports.addUserCar = function(req, res) {
	var query1 = 'SELECT id FROM user_vehicle WHERE id_user = :user';
	
	sequelize.query(query1, {replacements: {user: req.user.id}, type: sequelize.QueryTypes.SELECT}).then(function(data) {
		
		if(data.length == 0) {
			
			var query = 'INSERT INTO user_vehicle(id_user, id_allestiment) VALUES (:user, :vehicle)';
		
			sequelize.query(query, {replacements: {user: req.user.id, vehicle : req.body.allestiment}}).then(function(data) {
				
				if(data[0].affectedRows > 0) {
					res.send({status: 'car add successfully!' , id_car: data[0].insertId});
				}
				
			});
			
		}
		else {
			var query = 'UPDATE user_vehicle SET id_allestiment = :vehicle WHERE id = ' + data[0].id; 
			
			sequelize.query(query, {replacements: {vehicle : req.body.allestiment}}).then(function(data) {
				
				if(data[0].affectedRows > 0) {
					res.send({status: 'car update successfully!' , id_car: data[0].insertId});
				}
				
			});
		}
		
	});
	
}

module.exports.removeCar = function(req, res) {
	var query = 'DELETE FROM user_vehicle WHERE id_user = :user AND id_allestiment = :vehicle';
	
	sequelize.query(query, {replacements: {user: req.user.id, vehicle : req.body.allestiment}}).then(function(data) {
		if(data[0].affectedRows > 0) {
			res.send({status: 'car remove successfully!'});
		}
	});
}


/*
 * Groups
 */
exports.Groups = sequelize.define('groups', {
	id_grouptype: Sequelize.INTEGER,
	description: Sequelize.STRING,
	is_public: Sequelize.INTEGER,
	id_usercreated: Sequelize.BIGINT(20),
	datacreation: Sequelize.DATE,
	is_delete: Sequelize.INTEGER,
	from_city: Sequelize.STRING,
	to_city: Sequelize.STRING
}, {
    timestamps: false, 
    tableName: 'groups'
});

/*
 * Group User
 */
exports.GroupUser = sequelize.define('groupUser', {
	id_user: Sequelize.INTEGER,
	id_group: Sequelize.INTEGER,
	is_confirmed: Sequelize.BOOLEAN,
	is_refused: Sequelize.BOOLEAN,
	is_delete: Sequelize.BOOLEAN
}, {
    timestamps: false, 
    tableName: 'groups_user'	
});

exports.GroupUser.belongsTo(exports.Groups, {foreignKey: 'id'});
exports.Groups.hasMany(exports.GroupUser, {foreignKey:'id_group'}); 

/*
 * Groups Activity
 */
exports.GroupsActivity = sequelize.define('groupsActivity', {
		id_groups: Sequelize.INTEGER,
		id_activity: Sequelize.INTEGER
}, {
    timestamps: false, 
    tableName: 'groups_activity'
});

/*
 * Activity Stream
 */
exports.ActivityStream = sequelize.define('activityStream', {
    id_typeactivity: Sequelize.INTEGER,
    id_author: Sequelize.INTEGER,
    data: Sequelize.DATE
}, {
    timestamps: false, 
    tableName: 'activity_stream',
    classMethods: {
        emit: function(id_type, id_user, callback)
        {
            exports.ActivityStream.create({id_typeactivity: id_type, id_author: id_user}).then(function(activity){
                callback(activity);
            })
        }
    }
});

exports.ActivityStream.hasOne(exports.Comment, {foreignKey: 'id'});
exports.Comment.belongsTo(exports.ActivityStream, {foreignKey: 'id_activity'});

exports.ActivityStream.hasOne(exports.GroupsActivity, {foreignKey: 'id_activity'});
exports.GroupsActivity.belongsTo(exports.ActivityStream, {foreignKey: 'id'});

/*
 * Group Type
 */
exports.GroupType = sequelize.define('groupType', {
id: Sequelize.INTEGER,
description: Sequelize.STRING
}, {
    timestamps: false, 
    tableName: 'group_type'
});

/*
 * Travel Group
 */
exports.TravelGroup = sequelize.define('travelGroup', {

id_travel: Sequelize.INTEGER,
id_group: Sequelize.INTEGER,
id_travel_group_type: Sequelize.INTEGER
}, {
    timestamps: false, 
    tableName: 'travel_group'
});

/*
 * CO2 Vehicle
 */
exports.Co2 = sequelize.define('co2', {
id: Sequelize.INTEGER,
id_vehicle: Sequelize.INTEGER,
value: Sequelize.FLOAT
}, {
    timestamps: false, 
    tableName: 'co2_costant'
});

/*
 * Vehicle
 */
exports.Vehicle = sequelize.define('vehicle', {
id: Sequelize.INTEGER,
vehicle: Sequelize.STRING
}, {
    timestamps: false, 
    tableName: 'travel_vehicle'
});

/*
	
	Locality
		
*/

exports.locality = sequelize.define('locality',{
	description: Sequelize.STRING,
	lat: Sequelize.FLOAT,
	lng: Sequelize.FLOAT
},{
	 timestamps: false, 
    tableName: 'locality',
    classMethods: {
      addCity: function(name, lat, lng, callback){
	     exports.locality.build({description: name, lat: lat, lng: lng})
	     .save()
	     .then(function(data){
		     callback(data);
	     });
      }
    }
});

/*
	
	Travel
		
*/
exports.travel = sequelize.define('travel',{
	id_start: Sequelize.INTEGER,
	id_end: Sequelize.INTEGER
},{
	timestamps: false, 
	tableName: 'travel',
	classMethods: {
		insertTravel: function(start, end, callback){
			 exports.travel.build({id_start: start, id_end: end})
		     .save()
		     .then(function(data){
			     callback(data);
		     });
		}
	}
});


exports.TravelSaved = sequelize.define('travel_saved',{
    id_user: Sequelize.INTEGER,
    from: Sequelize.STRING,
    to: Sequelize.STRING,
    distance: Sequelize.DECIMAL,
    duration: Sequelize.INTEGER,
    total_transfer_duration: Sequelize.INTEGER,
    indicative_price: Sequelize.DECIMAL,
    co2: Sequelize.INTEGER,
    date: Sequelize.DATE,
    path: Sequelize.STRING,
    solution_type: Sequelize.STRING,
    travel_done: Sequelize.INTEGER,
    travel_buyed: Sequelize.INTEGER,
    in_wishlist: Sequelize.INTEGER,
    datasaved: Sequelize.NOW()
    
},{
	timestamps: false, 
	tableName: 'travel_saved'
});


exports.TravelSavedSegment = sequelize.define('travel_saved_segment',{
    id_travel_saved: Sequelize.INTEGER,
    kind: Sequelize.STRING,
    vehicle: Sequelize.STRING,
    distance: Sequelize.DECIMAL,
    duration: Sequelize.INTEGER,
    s_name: Sequelize.STRING,
    s_pos: Sequelize.STRING,
    t_name: Sequelize.STRING,
    t_pos: Sequelize.STRING,
    path: Sequelize.STRING,
    indicative_price: Sequelize.DECIMAL,
    company: Sequelize.STRING,
    segment_done: Sequelize.INTEGER,
    segment_buyed: Sequelize.INTEGER,
    iconPath: Sequelize.STRING,
    iconOffset: Sequelize.STRING,
    link: Sequelize.STRING,
    astart_code:Sequelize.STRING,
    aend_code: Sequelize.STRING
    
},{
	timestamps: false, 
	tableName: 'travel_saved_segment'
});


exports.TravelSaved.hasMany(exports.TravelSavedSegment, {
    foreignKey: 'id_travel_saved'
});


//exports.travel.belongsTo(exports.locality, {foreignKey: ''});

/*
	
	user_travel_searched
		
*/
exports.usertravelsearch = sequelize.define('user_travel_searched',{
	id_user: Sequelize.BIGINT(20), 
	id_travel: Sequelize.BIGINT(20),
	id_activity: Sequelize.BIGINT(20)
},{
	timestamps: false,
	tableName: 'user_travel_searched',
	classMethods: {
		addSearch: function(id_user, id_travel, id_activity, callback){
			
			 exports.usertravelsearch.build({id_user: id_user, id_travel: id_travel, id_activity:id_activity})
		     .save()
		     .then(function(data){
			     callback(data);
		     }); 
		},
		
		searchSameTravel: function(data, travel, callback) {
			sequelize
				.query("SELECT * FROM activity_stream JOIN user_travel_searched ON activity_stream.id = user_travel_searched.id_activity WHERE id_travel = " + travel + " AND data BETWEEN '" +data.getFullYear() + "-" +  (data.getMonth() + 1) + "-" + data.getDate() + " 00:00:00" + "' AND '" + data.getFullYear() + "-" +  (data.getMonth() + 1) + "-" + data.getDate() + " 23:59:59'", {replacement: {travel: travel}, type: sequelize.QueryTypes.SELECT})
				.then(function(rs){
					callback(rs);
			});
		}
	}
});

/*
	
	travel_state	
	
*/
exports.TravelStateType = sequelize.define('travel_state_type',{
	description: Sequelize.STRING
},{
	timestamps: false ,
	tableName: 'travel_state_type'
});

exports.activitystreamtype = sequelize.define('activity_stream_type',{
	description: Sequelize.STRING,
	id_parenttype: Sequelize.BIGINT(20),
	is_visible_indiary: Sequelize.BOOLEAN
},{
	timestamps: false,
	tableName: 'activity_stream_type'
});

exports.activityParentType = sequelize.define('activity_parent_type',{
	description: Sequelize.STRING
}, {
	timestamps: false,
	tableName: 'activity_parent_type'
});

exports.ActivityStream.belongsTo(exports.activitystreamtype, {foreignKey: 'id_typeactivity'});
exports.activitystreamtype.hasMany(exports.ActivityStream, {foreignKey: 'id'});

exports.activitystreamtype.belongsTo(exports.activityParentType, {foreignKey: 'id_parenttype'});
exports.activityParentType.hasMany(exports.activitystreamtype, {foreignKey: 'id'});

exports.getActivityGroup = function(req, off, callback){
	if(req.params.id_user == "me")
		var user = req.user.id;
	else
		var user = req.params.id_user;	
		
	sequelize.query("CALL create_notification_bacheca(5," + off + ", " + user + "," + req.user.id + ")").then(function(data){
		
		callback(data);
	});
}

exports.getFriendActivityGroup = function(req, off, callback){
	if(req.params.id_user == "me")
		var user = req.user.id;
	else
		var user = req.params.id_user;	

	console.log("CALL create_friend_notification_bacheca(5," + off + ", " + user + "," + req.user.id + ")")
	sequelize.query("CALL create_friend_notification_bacheca(5," + off + ", " + user + "," + req.user.id + ")").then(function(data){
		callback(data);
	});
}

exports.travel.belongsTo(exports.locality, {foreignKey: 'id_start', as: 'LocalityStart'});
exports.locality.hasMany(exports.travel, {foreignKey:'id', }); 

exports.travel.belongsTo(exports.locality, {foreignKey: 'id_end', as: 'LocalityEnd'});
exports.locality.hasMany(exports.travel, {foreignKey:'id'}); 

exports.usertravelsearch.belongsTo(exports.travel, {foreignKey: 'id_travel'});
exports.travel.hasMany(exports.usertravelsearch, {foreignKey: 'id'}); 

exports.usertravelsearch.belongsTo(exports.ActivityStream,{foreignKey: 'id_activity'});
exports.ActivityStream.hasMany(exports.usertravelsearch,{foreignKey: 'id'});

exports.UserRelationStatus = sequelize.define('user_relation_status', {
	id_relation: Sequelize.BIGINT(20),
	id_activity: Sequelize.BIGINT(20)
}, {
	timestamps: false,
	tableName: 'user_relation_status'
});

exports.NotificationType = sequelize.define('notification_types', {
    description: Sequelize.STRING,
    guest: Sequelize.BOOLEAN
}, {
    timestamps: false
});

exports.TravelState = sequelize.define('travel_state', {
	id_activity: Sequelize.BIGINT(20),
	id_travel: Sequelize.BIGINT(20),
	id_state: Sequelize.BIGINT(20)
},{
	timestamps: false,
	tableName: 'travel_state' 
});

exports.TravelState.belongsTo(exports.TravelStateType, {foreignKey: 'id', as: 'stateTravel'});
exports.TravelStateType.hasMany(exports.TravelState, {foreignKey:'id_state', });

exports.TravelState.belongsTo(exports.ActivityStream, {foreignKey: 'id'});
exports.ActivityStream.hasMany(exports.TravelState, {foreignKey:'id_activity'});

exports.UserPermissionSending = sequelize.define('user_permission_sending',{
	id_user: Sequelize.BIGINT(20),
	id_useractivitystream: Sequelize.BIGINT(20),
	id_parenttype: Sequelize.BIGINT(20),
	at_all: Sequelize.BOOLEAN
},{
	timestamps: false,
	tableName: 'user_permission_sending' 
});

exports.UserPermissionReceiving = sequelize.define('user_permission_receiving',{
	id_user: Sequelize.BIGINT(20),
	id_useractivitystream: Sequelize.BIGINT(20),
	id_parenttype: Sequelize.BIGINT(20),
	from_all: Sequelize.BOOLEAN
},{
	timestamps: false,
	tableName: 'user_permission_receiving' 
});

exports.UserPermissionReceiving.belongsTo(exports.activityParentType, {foreignKey: 'id_parenttype'});
exports.activityParentType.hasMany(exports.UserPermissionReceiving, {foreignKey: 'id'});

exports.UserPermissionSending.belongsTo(exports.activityParentType, {foreignKey: 'id_parenttype'});
exports.activityParentType.hasMany(exports.UserPermissionSending, {foreignKey: 'id'});

/*
 * Procedures
 */

exports.userLoggedIn = function(user_id)
{
    sequelize.query("CALL loggedin(:user_id);", {replacements: {user_id: user_id}}).then(function() {
        
    });
};

exports.userLoggedOut = function(user_id)
{
    sequelize.query("call loggedout(:user_id);", {replacements: {user_id: user_id}}).then(function() {
        
    })
};

exports.UserInfo.belongsTo(exports.User, {foreignKey: 'id_user', as: 'iduser'});
exports.User.hasMany(exports.UserInfo, {foreignKey:'id', }); 

exports.Caselli = sequelize.define('caselli', {
	name: Sequelize.STRING,
	lat: Sequelize.FLOAT,
	lng: Sequelize.FLOAT,
	id_casello_site: Sequelize.INTEGER,
	id_real_pop: Sequelize.INTEGER
},{
	timestamps: false,
	tableName: 'caselli' 
});

exports.ActivityHide = sequelize.define('activity_hide',{
	id_user: Sequelize.BIGINT(20),
	id_activity: Sequelize.BIGINT(20)
},{
	timestamps: false,
	tableName: 'activity_hide'
});

exports.ActivityHide.belongsTo(exports.ActivityStream, {foreignKey: 'id_activity'});
exports.ActivityStream.hasMany(exports.ActivityHide, {foreignKey: 'id'});

exports.FakeUser = sequelize.define('fake_user',{
	name: Sequelize.STRING,
	surname: Sequelize.STRING
	},{
		timestamps: false,
		tableName: 'fake_user'
});

exports.FakeCity = sequelize.define('fake_city',{
	value: Sequelize.STRING
	},{
		timestamps: false,
		tableName: 'fake_city'
});

exports.FakeHistory = sequelize.define('fake_history',{
	id_user: Sequelize.BIGINT(20),
	id_from: Sequelize.BIGINT(20),
	id_to: Sequelize.BIGINT(20),
	id_type: Sequelize.BIGINT(20),
	time: Sequelize.DATE(),
	content: Sequelize.STRING,
	data: Sequelize.STRING
	},{
		timestamps: false,
		tableName: 'fake_history'
});
