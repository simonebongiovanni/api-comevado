/**
*
*   This file manage database connection and definition
*
**/
var Sequelize = require('sequelize');
var Entities = require('html-entities').AllHtmlEntities;
var settings = global.settings.databases.car;
entities = new Entities();

var sequelize = new Sequelize(settings.schema, settings.username, settings.password, {
      dialect: settings.dialect,
      port: settings.port,
      host: settings.host,
      logging: function (str) {
          if(settings.log)
            console.log("querylog: "+str.replace("Executing (default):", "") );
      }
    });


sequelize.authenticate().then(function(err) {
    if (!!err) {
      console.log('Database '+settings.schema+' Connection Error:', err)
    } else {
      console.log('Database '+settings.schema+' Connected')
    }
  })

/**
*
*    Make the query to get allestiment
*    @param {Object} res - the object for manipulating result
*    @param {Object} query - the object containing filter query
*    @param {String} type - the type of the request
*
**/
module.exports.allestiment = function(res, req, type) {
	var v = buildQuery(req, type);

	sequelize.query('SELECT car_allestiment.id, car_allestiment.Descrizione, car_allestiment.Trazione, car_allestiment.Trasmission, car_allestiment.cm3, car_start_production.Descrizione as Inizio_produzione, car_end_production.Descrizione as Fine_produzione FROM car_allestiment ' + v , { type: sequelize.QueryTypes.SELECT, replacements: {maker: req.body.maker, model: req.body.model, fuel: req.body.fuel, power: req.body.power, generic: req.body.generic, allestiment: req.body.allestiment}}).then(function(car){
			res.send(car);
	});
}


/**
*
*    Make the query to get fuel type
*    @param {Object} res - the object for manipulating result
*    @param {Object} query - the object containing filter query
*    @param {String} type - the type of the request
*
**/
module.exports.fuel = function(res, req, type) {
	var v = buildQuery(req, type);

	sequelize.query('SELECT DISTINCT car_fuel.id,car_fuel.Descrizione FROM car_fuel ' + v , { type: sequelize.QueryTypes.SELECT, replacements: {maker: req.body.maker, model: req.body.model, fuel: req.body.fuel, power: req.body.power, generic: req.body.generic, allestiment: req.body.allestiment}}).then(function(car){
			res.send(car);
		});
}

/**
*
*    Make the query to get model generic
*    @param {Object} res - the object for manipulating result
*    @param {Object} query - the object containing filter query
*    @param {String} type - the type of the request
*
**/
module.exports.generic = function(res, req, type) {
	var v = buildQuery(req, type);

	sequelize.query('SELECT DISTINCT car_model_generic.id, car_model_generic.Descrizione FROM car_model_generic ' + v , { type: sequelize.QueryTypes.SELECT, replacements: {maker: req.body.maker, model: req.body.model, fuel: req.body.fuel, power: req.body.power, generic: req.body.generic, allestiment: req.body.allestiment}}).then(function(car){
			res.send(car);
		});
}

/**
*
*    Make the query to get car maker
*    @param {Object} res - the object for manipulating result
*    @param {Object} query - the object containing filter query
*    @param {String} type - the type of the request
*
**/
module.exports.maker = function(res, req, type) {
	var v = buildQuery(req, type);
	
	sequelize.query('SELECT DISTINCT car_maker.id, car_maker.Descrizione FROM car_maker ' + v , { type: sequelize.QueryTypes.SELECT, replacements: {maker: req.body.maker, model: req.body.model, fuel: req.body.fuel, power: req.body.power, generic: req.body.generic, allestiment: req.body.allestiment}}).then(function(car){
			var cars = [];
			
			car.forEach(function(c){
				c.Descrizione = entities.decode(c.Descrizione);
				cars.push(c);
			});
		
			res.send(cars);
	});
}

/**
*
*    Make the query to get car model
*    @param {Object} res - the object for manipulating result
*    @param {Object} query - the object containing filter query
*    @param {String} type - the type of the request
*
**/
module.exports.model = function(res, req, type) {
	var v = buildQuery(req, type);
	
	sequelize.query('SELECT DISTINCT car_model.id, car_model.Descrizione FROM car_model ' + v , { type: sequelize.QueryTypes.SELECT, replacements: {maker: req.body.maker, model: req.body.model, fuel: req.body.fuel, power: req.body.power, generic: req.body.generic, allestiment: req.body.allestiment}}).then(function(car){
			var cars = [];
			
			car.forEach(function(c){
				c.Descrizione = entities.decode(c.Descrizione);
				cars.push(c);
			});
		
			res.send(cars);
	});
}

/**
*
*    Make the query to get car power
*    @param {Object} res - the object for manipulating result
*    @param {Object} query - the object containing filter query
*    @param {String} type - the type of the request
*
**/
module.exports.power = function(res, req, type) {
	var v = buildQuery(req, type);
	
	sequelize.query('SELECT DISTINCT car_power.id,car_power.Descrizione FROM car_power ' + v , { type: sequelize.QueryTypes.SELECT, replacements: {maker: req.body.maker, model: req.body.model, fuel: req.body.fuel, power: req.body.power, generic: req.body.generic, allestiment: req.body.allestiment}}).then(function(car){
		res.send(car);
	});
}


/*
	Get Car Fuel by Allestiment 
*/
exports.getFuelByAllestiment = function(allestimentID, callback) {
	var query = 'SELECT DISTINCT machine.fuel_cost.price FROM machine.fuel_cost, machine.car_fuel, machine.car_relation WHERE machine.car_relation.id = :allestiment AND machine.car_relation.IDCarFuel = machine.car_fuel.id AND machine.car_fuel.id = machine.fuel_cost.id_fuel';
	sequelize.query(query, {replacements: {allestiment: allestimentID}, type: sequelize.QueryTypes.SELECT}).then(function(data) {
		callback(data);
	});
}


/*
	Get Car Mixed by Allestiment 
*/
exports.getMixedByAllestiment = function(allestimentID, callback) {
	var query = 'SELECT DISTINCT mixed FROM car_consuption WHERE id = :allestiment';
	sequelize.query(query, {replacements: {allestiment: allestimentID}, type: sequelize.QueryTypes.SELECT}).then(function(data) {
		callback(data);
	});
}

/**
*
*	Build the query string
*   @param {Object} query - contain the object filter
*   @param {String} type - the type of the request
*
**/
function buildQuery (req, type) {
	var joinQuery = '';
	var clause = '';
	switch(type) {
		case 'maker':
		  joinQuery = 'JOIN car_relation ON car_relation.IDCarMaker = car_maker.id ';
		break;
		case 'model':
		  joinQuery = 'JOIN car_relation ON car_relation.IDCarModel = car_model.id ';
		break;
		case 'fuel':
		  joinQuery = 'JOIN car_relation ON car_relation.IDCarFuel = car_fuel.id ';
		break;
		case 'power':
		  joinQuery = 'JOIN car_relation ON car_relation.IDCarPower = car_power.id ';
		break;
		case 'allestiment':
		  joinQuery = 'JOIN car_relation ON car_relation.IDCarAllestiment = car_allestiment.id JOIN car_start_production ON car_start_production.id = car_relation.IDStartProduction JOIN car_end_production ON car_end_production.id = car_relation.IDEndProduction ';
		break;
		case 'generic':
		  joinQuery = 'JOIN car_relation ON car_relation.IDCarModelGeneric = car_model_generic.id ';
		break;
		default:
		   joinQuery = '';
		break;
		
	}
	
	for(k in req.body) {
		if(clause == '') {
			clause = ' WHERE ';
		}
		else {
			clause += ' AND ';
		}
	
		clausole = function (index, cl, val) {
			switch(index) {
				case 'maker':
				  cl += "IDCarMaker = :maker";
				break;
				case 'model':
				  cl += "IDCarModel = :model";
				break;
				case 'fuel':
				  cl += "IDCarFuel = :fuel";
				break;
				case 'power':
				  cl += "IDCarPower = :power";
				break;
				case 'generic':
				  cl += "IDCarModelGeneric = :generic";
				break;
				case 'allestiment':
				  cl += "IDCarAllestiment = :allestiment";
				break;
			}
			return cl;
		}
		clause = clausole(k, clause, req.body[k]);
	}
	
	filter = joinQuery + clause;
	
	return filter;
}

module.exports.getTypeFuel = function(idVehicle, callback){
	sequelize.query('SELECT IDCarFuel FROM car_relation WHERE  id = :vehicle', { type: sequelize.QueryTypes.SELECT, replacements: {vehicle: idVehicle}}).then(function(idfueltype){
		callback(idfueltype);
	});
}

exports = sequelize;