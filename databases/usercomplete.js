 var Sequelize = require('sequelize');
var settings = global.settings.databases.usercomplete;
/**
 * Creating sequelize OBJ
 * @param {string} database_name
 * @param {string} database_user
 * @param {string} database_password
**/

var sequelize = new Sequelize(settings.schema, settings.username, settings.password, {
      dialect: settings.dialect,
      port: settings.port,
      host: settings.host,
      logging: function (str) {
          if(settings.log)
            console.log("querylog: "+str.replace("Executing (default):", "") );
      }
    });

sequelize.authenticate().then(function(err) {
    if (!!err) {
      console.log('Database '+settings.schema+' Connection Error:', err)
    } else {
      console.log('Database '+settings.schema+' Connected')
    }
  })

exports.sequelize = sequelize;

/**
 *  getUsercomplete
 * 
 * @param {string} name
 * @param {function} callback
**/

exports.getUsercomplete = function(req, callback){
	
	if(req.body.name.length >= 2) {
	    table = req.body.name.substr(0, 2);
		table = table.toLowerCase();
		table = 'usr_autocomplete.cvuserautocomplete_'+table;
		name = req.body.name.toLowerCase();
		name = name+"%";
	
		sequelize.query("SELECT main.users.id, MAX(CASE WHEN id_type = 9 THEN value END) AS profile_image, MAX(CASE WHEN id_type = 6 THEN value END) AS name, MAX(CASE WHEN id_type = 7 THEN value END) AS surname, (SELECT COUNT(*) FROM main.user_relations WHERE (id_user1 = main.users.id AND id_user2 = :iduser1 AND is_deleted = 0) || (id_user1 = :iduser1 AND id_user2 = main.users.id AND is_deleted = 0)) as is_friend FROM main.users JOIN main.user_info ON main.user_info.id_user = main.users.id WHERE main.users.id <> :iduser1 AND main.users.id in ( SELECT id_user as id FROM " + table + " WHERE CONCAT(LOWER(name),LOWER(name1)) LIKE :name) GROUP BY main.users.id LIMIT 10", { replacements:{ iduser1: req.user.id, name: name }, type: sequelize.QueryTypes.SELECT}).then(function(usercomplete){

			countCommonFriend(0, usercomplete, req, function(usercomplete){
				scoreResult(0, usercomplete, req, function(usercomplete){
					callback(usercomplete);
				});
			});
	    });
	}		
};

var countCommonFriend = function(index, usercomplete, req, callback){
	if(index < usercomplete.length){
		sequelize.query('SELECT COUNT(*) as cont FROM (SELECT id_user2 AS user FROM main.user_relations WHERE id_user1 = :user2 AND is_confirmed = 1 AND is_deleted = 0 UNION SELECT id_user1 AS user FROM main.user_relations WHERE id_user2 = :user2 AND is_confirmed = 1 AND is_deleted = 0 )a INNER JOIN (SELECT id_user2 AS user FROM main.user_relations WHERE main.user_relations.id_user1 = :iduser1 AND is_confirmed = 1 AND is_deleted = 0 UNION SELECT id_user1 AS user FROM main.user_relations WHERE id_user2 = :iduser1 AND is_confirmed = 1 AND is_deleted = 0)c ON c.user = a.user', { replacements:{ iduser1: req.user.id, user2: usercomplete[index].id }, type: sequelize.QueryTypes.SELECT}).then(function(common){			
			usercomplete[index].common_friend = common[0].cont;
			
			index++;
			
			countCommonFriend(index, usercomplete, req, callback);
		});
	}
	else{
		callback(usercomplete);
	}
}

var scoreResult = function(index, usercomplete, req, callback){
	if(index < usercomplete.length){
		if(usercomplete[index].is_friend == 0){
			index++;
			scoreResult(index, usercomplete, req, callback);
		}
		else{
			sequelize.query("SELECT is_confirmed FROM main.user_relations WHERE (id_user1 = :iduser1 && id_user2 = :user) || (id_user2 = :iduser1 && id_user1 = :user) && is_deleted = 0", { replacements:{ iduser1: req.user.id, user: usercomplete[index].id }, type: sequelize.QueryTypes.SELECT}).then(function(user){
				
				if(user[0].is_confirmed)
					usercomplete[index].is_confirmed = true;
				else
				    usercomplete[index].is_confirmed = false;   
				    
				index++;
				scoreResult(index, usercomplete, req, callback);
			});
		}
	}
	else{
		callback(usercomplete);
	}
}

/**
	(

*/