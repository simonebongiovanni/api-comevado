var main_server_ip = "54.93.203.42";

/**
 * Route Filters
 */

exports.onlyMainServer = function(req, res, next){
    
    if (req.connection.remoteAddress == main_server_ip) {
        next();        
    } else {
        errors.sendError(res, errors.codes.invalidCredentials);
    }
}




